#!/usr/bin/env python3

"""
To test the program:
    % python3 TestNetflix.py >& TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py >& TestNetflix.out
"""


import io
import unittest
from Netflix import *


class TestNetflix (unittest.TestCase):

    def should_raise_error(self, errtype, func):
        flag = False
        try:
            func()
        except errtype:
            flag = True
        self.assertTrue(flag)

    def test_rmse1(self):
        """
        RMSE return 0 when there is no input.
        """
        self.assertEqual(rmse([], []), 0.0)

    def test_rmse2(self):
        """
        RMSE of equal iterators should be 0.
        """
        a = [7, 2, 5, 10]
        b = [7, 2, 5, 10]
        self.assertEqual(a, b)
        self.assertEqual(rmse(a, b), 0.0)

    def test_rmse3(self):
        """
        General RMSE test.
        """
        a = [7, 2, 5, 10]
        b = [8, 1, 6, 9]
        self.assertNotEqual(a, b)
        self.assertEqual(rmse(a, b), 1.0)

    def test_calc_rating1(self):
        """
        Verify that assertions catch bad movie ids.
        """
        self.should_raise_error(AssertionError, lambda: calc_rating(-1, 345, None, None))

    def test_calc_rating2(self):
        """
        Verify that assertions catch bad user ids.
        """
        self.should_raise_error(AssertionError, lambda: calc_rating(412, -1, None, None))

    def test_calc_rating3(self):
        """
        Verify that the function fails when the movie id is not in the cache.
        """
        movieavgs = {'12': 1.0}
        useravgs = {'345': 1.0}
        self.should_raise_error(KeyError, lambda: calc_rating('412', '345', movieavgs, useravgs))

    def test_calc_rating4(self):
        """
        Verify that the function fails when the user id is not in the cache.
        """
        movieavgs = {'412': 1.0}
        useravgs = {'45': 1.0}
        self.should_raise_error(KeyError, lambda: calc_rating('412', '345', movieavgs, useravgs))

    def test_find_rating1(self):
        """
        Verify that assertions catch bad movie ids.
        """
        self.should_raise_error(AssertionError, lambda: find_rating(-1, 345, None))

    def test_find_rating2(self):
        """
        Verify that assertions catch bad user ids.
        """
        self.should_raise_error(AssertionError, lambda: find_rating(412, -1, None))

    def test_find_rating3(self):
        """
        Verify that the function fails when the movie id is not in the cache.
        """
        cache = {'12': {'345': 1}}
        self.should_raise_error(KeyError, lambda: find_rating('412', '345', cache))

    def test_find_rating4(self):
        """
        Verify that the function fails when the user id is not in the cache.
        """
        cache = {'412': {'45': 1}}
        self.should_raise_error(KeyError, lambda: find_rating('412', '345', cache))

    def test_find_rating5(self):
        """
        Verify that the cache lookup works!
        """
        cache = {'412': {'345': 1.0}}
        self.assertEqual(find_rating('412', '345', cache), 1.0)

    def test_get_json_cache1(self):
        """
        Verify that the cache fails if the file does not exist.
        """
        self.should_raise_error(IOError, lambda: get_json_cache("I-DO-NOT-EXIST"))

    def test_netflix_solve1(self):
        """
        Test that having no input produces an RMSE of 0.
        """
        r = io.StringIO("")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertEqual(w.getvalue(), "RMSE: 0.0\n")

    def test_netflix_solve2(self):
        """
        Test that movies without a user list produces RMSE of 0.
        """
        r = io.StringIO("2043:\n10851:\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertEqual(w.getvalue(), "2043:\n10851:\nRMSE: 0.0\n")

print("TestNetflix.py")
unittest.main()
print("Done.")
