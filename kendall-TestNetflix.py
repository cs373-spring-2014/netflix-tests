import io
import unittest
#from Netflix import evaluate,predict
import Netflix


class TestNetflix(unittest.TestCase):
    #For evalute
    """Unit test to test the evaluate function for a simple test case."""
    def test_evaluate_simple(self):
        testInput = io.StringIO("1:\n30878\n2647871\n1283744")
        testOutput=io.StringIO()
        Netflix.evaluate(testInput,testOutput)
        output = testOutput.getvalue()
        self.assertTrue(len(output.split("\n"))==5)
        rmse = float(output.split("\n")[4].split(" ")[1])
        self.assertTrue(rmse >=0)






    """Unit test that tests for movies in the input that have no ratings"""
    def test_evaluate_empty_movie(self):
        testInput = io.StringIO("2:\n1:\n30878\n2647871\n1283744")
        testOutput = io.StringIO()
        Netflix.evaluate(testInput, testOutput)
        output = testOutput.getvalue()
        self.assertTrue(len(output.split("\n"))==6)
        rmse = float(output.split("\n")[5].split(" ")[1])
        self.assertTrue(rmse >=0)

    """Unit test that enforces the fact that the order of user ids within a movie should not matter"""
    def test_evaluate_order(self):
        testInput = io.StringIO("1:\n2647871\n1283744\n30878")
        testOutput=io.StringIO()
        Netflix.evaluate(testInput,testOutput)
        output = testOutput.getvalue()
        self.assertTrue(len(output.split("\n"))==5)
        rmse = float(output.split("\n")[4].split(" ")[1])
        self.assertTrue(rmse >=0)




    #For predict
    """First unit test to check for sanity of predictions. Since predictions will be changing as I improve the prediction algorithm, we can't check for exact values, only sanity"""
    def test_predict_1(self):
        output = Netflix.predict("30878","1")
        self.assertTrue(type(output) == float)
        self.assertTrue(output <=5)
        self.assertTrue(output >=1)


    """ The second unit test for predict. We'll try to predict two users on different ends of the probe file."""
    def test_predict_2(self):
        output1 = Netflix.predict("30878","1")
        output2 = Netflix.predict("842801","6014")
        self.assertTrue(type(output1) == float)
        self.assertTrue(output1<=5)
        self.assertTrue(output1>=1)
        self.assertTrue(type(output2)==float)
        self.assertTrue(output2<=5)
        self.assertTrue(output2>=1)


    """Third unit test for predict. Predict the same user/movie combo twice and make sure they're equal"""
    def test_predict_3(self):
        output1 = Netflix.predict("30878","1")
        output2 = Netflix.predict("30878","1")
        self.assertTrue(output1==output2)
        self.assertTrue(type(output1) == float)
        self.assertTrue(output1<=5)
        self.assertTrue(output1>=1)
        self.assertTrue(type(output2)==float)
        self.assertTrue(output2<=5)
        self.assertTrue(output2>=1)



unittest.main()
