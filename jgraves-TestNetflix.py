"""

	TestNetflix.py

"""
# ----
# imports
# ----

import io
import unittest

from Netflix import netflix_read, netflix_solve, netflix_predict, netflix_print, netflix_load_caches

class TestNetflix (unittest.TestCase) :
	# ----
	# read
	# ----
	def test_read_1 (self) :
		r = io.StringIO("1234:")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 1)
		assert(m_c[0] == "1234")
		assert(m_c[1] == '')
	def test_read_2 (self) :
		r = io.StringIO("1234")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 0)
		assert(m_c[0] == '')
		assert(m_c[1] == "1234")
	def test_read_3 (self) :
		r = io.StringIO("")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == -1)
		assert(m_c[1] == '')
		assert(m_c[1] == '')
	def test_read_4 (self) :
		r = io.StringIO("\n")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == -1)
		assert(m_c[1] == '')
		assert(m_c[1] == '')
	def test_read_5 (self) :
		r = io.StringIO("1234:\n")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 1)
		assert(m_c[0] == "1234")
		assert(m_c[1] == '')
	def test_read_6 (self) :
		r = io.StringIO("1234\n")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 0)
		assert(m_c[0] == '')
		assert(m_c[1] == "1234")
	def test_read_7 (self) :
		r = io.StringIO("1234:4321\n")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 1)
		assert(m_c[0] == '12344321')
		assert(m_c[1] == "")
	def test_read_8 (self) :
		r = io.StringIO("1234:\n4321\n")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 1)
		assert(m_c[0] == '1234')
		assert(m_c[1] == "")
	def test_read_9 (self) :
		r = io.StringIO("1234\n4321\n")
		m_c = ['','']
		i = netflix_read(r, m_c)
		assert(i == 0)
		assert(m_c[0] == '')
		assert(m_c[1] == "1234")
	
	# ----
	# print
	# ----
	def test_print_1 (self) :
		w = io.StringIO()
		s = "1234:"
		netflix_print(w, s)
		assert(w.getvalue() == "1234:\n")

	def test_print_2 (self) :
		w = io.StringIO()
		s = 1234
		netflix_print(w, s)
		assert(w.getvalue() == "1234\n")
	
	def test_print_2 (self) :
		w = io.StringIO()
		s = ""
		netflix_print(w, s)
		assert(w.getvalue() == "\n")	
		
	# ----
	# load_caches
	# ----
	def test_load_caches_1 (self) :
		try :
			netflix_load_caches()
		except :
			assert(False)
	
	# ----
	# predict
	# ----
	def test_netflix_predict_1 (self) :
		#netflix_load_caches()
		m_c = ["3349", "199403"]
		p = netflix_predict(m_c)
		assert(round(p,1) == 3.9)
	def test_netflix_predict_2 (self) :
		#netflix_load_caches()
		m_c = ["335", "1401865"]
		p = netflix_predict(m_c)
		assert(round(p,1) == 2.7)
	def test_netflix_predict_3 (self) :
		#netflix_load_caches()
		m_c = ["3350", "1717064"]
		p = netflix_predict(m_c)
		assert(round(p,1) == 3.6)
	
unittest.main()
