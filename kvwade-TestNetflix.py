#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""


import io
import unittest

from Netflix import netflix_read, netflix_rmse, netflix_read_actual_rating, netflix_eval, netflix_print, netflix_solve

class TestNetflix (unittest.TestCase) :

	def test_read1(self) :
		r = io.StringIO("1:\n2\n3\n4\n")
		for (i, j) in netflix_read(r) :
			self.assertTrue(i == "1")
			self.assertTrue(j[0] == "2")
			self.assertTrue(j[1] == "3")
			self.assertTrue(j[2] == "4")

	def test_read2(self) :
		r = io.StringIO("1:\n2\n3:\n4\n")
		g = netflix_read(r)
		(i, j) = g.__next__()
		self.assertTrue(i == "1")
		self.assertTrue(j[0] == "2")
		(i, j) = g.__next__()
		self.assertTrue(i == "3")
		self.assertTrue(j[0] == "4")

	def test_read3(self) :
		r = io.StringIO("1:\n2:\n3:\n4\n")
		g = netflix_read(r)
		(i, j) = g.__next__()
		self.assertTrue(i == "1")
		self.assertTrue(j == [])
		(i, j) = g.__next__()
		self.assertTrue(i == "2")
		self.assertTrue(j == [])
		(i, j) = g.__next__()
		self.assertTrue(i == "3")
		self.assertTrue(j[0] == "4")

	def test_read4(self):
		r = io.StringIO("1:\n2:\n")
		g = netflix_read(r)
		(i, j) = g.__next__()
		self.assertTrue(i == "1")
		self.assertTrue(j == [])
		(i, j) = g.__next__()
		self.assertTrue(i == "2")
		self.assertTrue(j == [])

	def test_read5(self):
		r = io.StringIO("1:\n2")
		g = netflix_read(r)
		(i, j) = g.__next__()
		self.assertTrue(i == "1")
		self.assertTrue(j[0] == "2")

	def test_rmse1(self) :
		a = [1]*50
		b = [1]*50
		ans = netflix_rmse(a, b)
		self.assertTrue(ans == 0)

	def test_rmse2(self) :
		a = [5]*500
		b = [1]*500
		ans = netflix_rmse(a, b)
		self.assertTrue(ans == 4)

	def test_rmse3(self) :
		a = [1, 2, 3]
		b = [3, 4, 5]
		ans = netflix_rmse(a, b)
		self.assertTrue(ans == 2)

	def test_read_actual_rating1(self) :
		g = netflix_read_actual_rating("1")
		n = g.__next__()
		self.assertTrue(n == "4")

	def test_read_actual_rating2(self) :
		g = netflix_read_actual_rating("10000")
		n = g.__next__()
		self.assertTrue(n == "5")
		n = g.__next__()
		self.assertTrue(n == "4")
		n = g.__next__()
		self.assertTrue(n == "4")

	def test_read_actual_rating3(self) :
		g = netflix_read_actual_rating("10003")
		n = g.__next__()
		self.assertTrue(n == "3")
		n = g.__next__()
		self.assertTrue(n=="5")

	def test_read_actual_rating4(self) :
		g = netflix_read_actual_rating("9999")
		n = g.__next__()
		self.assertTrue(n == "2")

	def test_eval1(self) :
		r = netflix_eval("1", "30878")
		ans = 3.749542961608775 * .45 + 3.6336173508907823 * .55
		self.assertTrue(r > ans - .0001 and r < ans + 0.0001)
		self.assertTrue(r == ans)

	def test_eval2(self) :
		r = netflix_eval("1", "2647871")
		ans = 3.749542961608775 * .45 + 3.2335329341317367 * .55
		self.assertTrue(r > ans - .0001 and r < ans + 0.0001)

	def test_print1(self) :
		w = io.StringIO()
		m = "1"
		r = ["2", "3", "4"]
		netflix_print(w, m, r)
		self.assertTrue(w.getvalue() == "1:\n2\n3\n4\n")

	def test_print2(self) :
		w = io.StringIO()
		m = "1"
		r = ["2"]
		netflix_print(w, m, r)
		self.assertTrue(w.getvalue() == "1:\n2\n")
		
	def test_print3(self) :
		w = io.StringIO()
		m = "80"
		r = ["2", "4", "3"]
		netflix_print(w, m, r)
		self.assertTrue(w.getvalue() == "80:\n2\n4\n3\n")

	def test_solve(self) :
		r = io.StringIO("1:\n30878\n")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue("RMSE:" in w.getvalue())
		self.assertTrue("1:" in w.getvalue())

print("TestNetflix.py")
unittest.main()
print("Done.")
