__author__ = 'wnation'

#!/usr/bin/env python3

# -------------------------------

# -------------------------------

"""
To test the program:
	% python TestNetflix.py >& TestNetflix.out
	% chmod ugo+x TestNetflix.py
	% TestNetflix.py >& TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import weighted_dict, rmse, netflix_read, netflix_print, netflix_solve

# -----------
# TestCollatz
# -----------

class TestNetflix (unittest.TestCase) :

	# ----
	# rmse
	# ----

	def test_rmse_1 (self) :
		v = rmse((1,2,3), (1,2,3))
		self.assertTrue(str(v) == "0.0")

	def test_rmse_2 (self) :
		v = rmse((1,2,3), (2,3,4))
		self.assertTrue(str(v) == "1.0")

	def test_rmse_3 (self) :
		v = rmse((2,3,4), (4,3,2))
		self.assertTrue(str(v) == "1.632993161855452")

	def test_rmse_4 (self) :
		self.assertRaises(AssertionError,rmse,(1,1,1), (1,1))


	# ----
	# netflix_read
	# ----

	def test_read_1 (self) :
		r = io.StringIO("1:\n1\n2\n3\n2:\n1\n2\n3\n")
		m = {}
		m = netflix_read(r)
		self.assertTrue(m[1] == [1,2,3])
		self.assertTrue(m[2] == [1,2,3])

	def test_read_2 (self) :
		r = io.StringIO("1:\n1\n10\n1000\n2:\n10\n")
		m = {}
		m = netflix_read(r)
		self.assertTrue(m[1] ==  [1,10,1000])
		self.assertTrue(m[2] == [10])

	def test_read_3 (self) :
		r = io.StringIO("1:\n1\n10\n1000\n2:\n0")
		m = {}
		m = netflix_read(r)
		self.assertTrue(m[1] ==  [1,10,1000])
		self.assertTrue(m[2] == [0])
		
	def test_read_4 (self) :
		r = io.StringIO("1:\n1\n2:\n")
		m = {}
		m = netflix_read(r)
		self.assertTrue(m[1] ==  [1])
		self.assertTrue(m[2] == [])

	

	# -----
	# netflix_solve
	# -----
	def test_netflix_solve_1 (self):
		r = io.StringIO("1005:\n188792\n239493\n532649")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue(w.getvalue() == 
			"1005:\n3.108410\n2.977337\n2.723416\nRMSE: 0.6630175045447323\n")

	def test_netflix_solve_2 (self):
		r = io.StringIO("10042:\n86062")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue(w.getvalue() == "10042:\n4.672252\nRMSE: 0.3277478223987025\n")
	
	def test_netflix_solve_3 (self):
		r = io.StringIO("")
		w = io.StringIO()
		self.assertRaises(AssertionError,netflix_solve,r, w)
		
	# -----
	# weighted_dict
	# -----
	def test_weighted_dict_1 (self):
		dictionary = {1:[2,3,4], 2:[2,3,4]}
		m_avg = {1:3.0, 2: 4.0}
		c_avg = {2: 3.0, 3: 4.0, 4: 5.0}
		weight = {}
		weight = weighted_dict(dictionary, c_avg, m_avg)
		self.assertTrue(weight[1] == [2.3, 3.3, 4.3])
		self.assertTrue(weight[2] == [3.3, 4.3, 5.0])
		
	def test_weighted_dict_2 (self):
		dictionary = {1:[2,3,4], 2:[2,3,5], 3:[6]}
		m_avg = {1:1.1, 2: 4.99, 3:3.5}
		c_avg = {2: 4.9, 3: 2.33, 4: 1.1, 5:3.35, 6:3.5}
		weight = {}
		weight = weighted_dict(dictionary, c_avg, m_avg)
		self.assertTrue(weight[1] == [2.3000000000000003, 1.0, 1.0])
		self.assertTrue(weight[2] == [5.0, 3.62, 4.640000000000001])
		self.assertTrue(weight[3] == [3.3])
	
	# -----
	# netflix_print
	# -----
	def test_netflix_print_1 (self):
		dictionary = {1:[2,3,4], 2:[2,3,4]}
		m_avg = {1:3.0, 2: 4.0}
		c_avg = {2: 3.0, 3: 4.0, 4: 5.0}
		weight = {}
		weight = weighted_dict(dictionary, c_avg, m_avg)
		w = io.StringIO()
		netflix_print(weight, w)
		self.assertTrue(w.getvalue() == "1:\n2.300000\n3.300000\n4.300000\n2:\n"
			+ "3.300000\n4.300000\n5.000000\n")
			
	def test_netflix_print_2 (self):
		dictionary = {1:[2,3,4], 2:[2,3,4]}
		m_avg = {1:1.0, 2: 1.0}
		c_avg = {2: 5.0, 3: 5.0, 4: 5.0}
		weight = {}
		weight = weighted_dict(dictionary, c_avg, m_avg)
		w = io.StringIO()
		netflix_print(weight, w)
		self.assertTrue(w.getvalue() == "1:\n2.300000\n2.300000\n2.300000\n2:\n2.300000\n"
			+ "2.300000\n2.300000\n")
		


# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
