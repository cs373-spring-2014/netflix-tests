#!/usr/bin/env python3
#!/usr/bin/Python

"""""
To test the program:
	%python3 TestNetflix.py > TestNetflix.out
	%chmod ugo+x TestNetflix.py
	%TestNetflix.py > TestNetflix.out
"""""

# -------
# imports
# -------

import io
import unittest
import sys

from math import sqrt
from Netflix import netflix_rmse, netflix_estimateUserAverage, netflix_buildCache, netflix_solve, netflix_getActualAverage, netflix_print


# -----------
# TestNetflix
# -----------


class TestNetflix (unittest.TestCase) : 

	def test_rmse_1(self):
		a = [1,2,1]
		p = [2,1,2]
		self.assertTrue(netflix_rmse(a, p) == 1.0)

	def test_rmse_2(self):
		p = a = [1,2,1]
		self.assertTrue(netflix_rmse(a, p) == 0.0)

	def test_rmse_3(self):
		a = [10, 20, 10]
		p = [20, 10, 20]
		self.assertTrue(netflix_rmse(a, p) == 10.0)
		
	def test_rmse_4(self):
		a = [700, 500, 700]
		p = [400, 800, 1000]
		self.assertTrue(netflix_rmse(a, p) == 300.0)
	
	def test_rmse_5(self):
		a = [10, 20, 30]
		p = [8, 15, 37]
		self.assertTrue(netflix_rmse(a, p) == sqrt(26.0))

	def test_getActualAverage_1(self):
		a = []
		p = [1.0,4.0]

		r = io.StringIO("47656:\n1\n3")
		firstMovie = [47656]

		netflix_getActualAverage(r, firstMovie, a, p)
		self.assertTrue(len(a) == len(p))		
		self.assertTrue(a == [1.0,3.0])

	def test_getActualAverage_2(self):
		a = []
		p = [1.0456,4.06473]

		r = io.StringIO("25576:\n3\n5")
		firstMovie = [25576]

		netflix_getActualAverage(r, firstMovie, a, p)
		self.assertTrue(len(a) == len(p))		
		self.assertTrue(a == [3.0,5.0])

	def test_getActualAverage_3(self):
		a = []
		p = [1.0,4.0,4.0,3.0]

		r = io.StringIO("47656:\n1\n3\n32:\n5\n2")
		firstMovie = [47656]

		netflix_getActualAverage(r, firstMovie, a, p)
		self.assertTrue(len(a) == len(p))		
		self.assertTrue(a == [1.0,3.0,5.0,2.0])

	def test_estimateUserAverage_1(self):
		movAvgRating = {47656:3.5}
		userAvgRating = {165:4}
		totalMovieRatings = {47656:1}
		userOverallAverage = sum(userAvgRating.values()) / len(userAvgRating)

		firstMovie = [0]

		r = io.StringIO("47656:\n165")
		p = []

		result = netflix_estimateUserAverage(movAvgRating, userAvgRating, totalMovieRatings, userOverallAverage, firstMovie, r, p)
		self.assertTrue(len(p) == 1)
		self.assertTrue(result == ("47656:\n" + str(p[0]) +"\n"))
		self.assertTrue(firstMovie[0] == 47656)
    

	def test_estimateUserAverage_2(self):
		movAvgRating = {78563:3.5}
		userAvgRating = {165:4, 3:2, 65:5, 74674:1}
		totalMovieRatings = {78563:4}
		userOverallAverage = sum(userAvgRating.values()) / len(userAvgRating)

		firstMovie = [0]

		r = io.StringIO("78563:\n165\n3\n65\n74674")
		p = []

		result = netflix_estimateUserAverage(movAvgRating, userAvgRating, totalMovieRatings, userOverallAverage, firstMovie, r, p)
		self.assertTrue(len(p) == 4)
		self.assertTrue(result == ("78563:\n" + str(p[0]) +"\n" + str(p[1]) + "\n" + str(p[2]) + "\n" + str(p[3]) + "\n"))
		self.assertTrue(firstMovie[0] == 78563)

	def test_buildCache_1(self):
		c = netflix_buildCache("/u/thunt/cs373-netflix-tests/irvin-movie_avg_rating.txt")
		self.assertTrue(len(c) == 17770)

	def test_buildCache_2(self):
		c1 = netflix_buildCache("/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt")
		c2 = netflix_buildCache("/u/thunt/cs373-netflix-tests/irvin-movie_avg_rating.txt")
		self.assertTrue(len(c1) == 17770)
		self.assertTrue(len(c1) == len(c2))

	def test_buildCache_3(self):
		c = netflix_buildCache("/u/thunt/cs373-netflix-tests/irvin-decade_avg_rating.txt")
		self.assertTrue(len(c) == 12)
		self.assertTrue(c[1890] == 3.6776315789473686)
		self.assertTrue(c[1900] == 2.7889908256880735)
		self.assertTrue(c[1910] == 3.386692607003891)
		self.assertTrue(c[1920] == 3.698777067205206)
		self.assertTrue(c[1930] == 3.890220752093558)
		self.assertTrue(c[1940] == 3.8420872646420086)
		self.assertTrue(c[1950] == 3.8526199400004963)
		self.assertTrue(c[1960] == 3.814806662467759)
		self.assertTrue(c[1970] == 3.77365151408982)
		self.assertTrue(c[1980] == 3.6843293771390138)
		self.assertTrue(c[1990] == 3.617264036843734)
		self.assertTrue(c[2000] == 3.522762354036767)
		
	def test_netflix_print_1(self):
		s = "65:\n3\n2"
		w = io.StringIO()
		netflix_print(w, s)
		self.assertTrue(w.getvalue() == "65:\n3\n2")

	def test_netflix_print_2(self):
		s = "47656:\n1\n5\n3\n2\n323:\n4\n3\n5"
		w = io.StringIO()
		netflix_print(w, s)
		self.assertTrue(w.getvalue() == "47656:\n1\n5\n3\n2\n323:\n4\n3\n5")

	def test_netflix_print_3(self):
		s = "47656:\n1\n5\n3\n2\n"
		s += "323:\n4\n3\n5\n"
		s += "65:\n3\n2"
		w = io.StringIO()
		netflix_print(w, s)
		self.assertTrue(w.getvalue() == "47656:\n1\n5\n3\n2\n323:\n4\n3\n5\n65:\n3\n2")
		
	def test_netflix_print_4(self):
		s = ""
		w = io.StringIO()
		netflix_print(w, s)
		self.assertTrue(w.getvalue() == "")

	def test_netflix_solve_1(self):
		r = io.StringIO("16762:\n2239778\n1222994")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue(w.getvalue() == "16762:\n2.4952496237559245\n2.0531500566563574\nRMSE: 2.1129596279170615\n")


print ("TestNetflix.py")
unittest.main()
print ("Done")
