#!/usr/bin/env python3

import io
import unittest

from Netflix import *

class TestNetflix(unittest.TestCase) :

    #
    # read_training_data
    #
    def test_read_training_data_1(self):
        r = io.StringIO('666:\n11421,1,2009-12-14\n141,4,2012-12-14\n1,9,2007-10-01')
        movies = read_training_data(r)
        self.assertTrue(movies == (666, {11421: (1, '2009-12-14'),
                141: (4, '2012-12-14'),
                 1: (9, '2007-10-01')}))

    def test_read_training_data_empty(self):
        r = io.StringIO("")
        try:
            movies = read_training_data(r)
        except InvalidTrainingFile:
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_read_training_data_2(self):
        r = io.StringIO('666:')
        movies = read_training_data(r)
        self.assertTrue(movies == (666, {}))

    #
    # read_probe
    #
    def test_read_probe_1(self):
        r = io.StringIO('345:\n3\n9\n12')
        movies = list(read_probe(r))
        self.assertTrue(movies[0] == [345, 3, 9, 12])

    def test_read_probe_2(self):
        r = io.StringIO('345:\n3\n9\n12\n35:\n3\n1:\n1\n2\n12')
        movies = list(read_probe(r))
        self.assertTrue(movies == [[345, 3, 9, 12], [35, 3], [1, 1, 2, 12]])

    def test_read_probe_3(self):
        r = io.StringIO('')
        movies = list(read_probe(r))
        self.assertTrue(movies == [[]])

    #
    # rmse
    #
    def test_rmse_gen_1(self):
        score = rmse_gen([3, 9, 12], [3, 9, 1])
        self.assertTrue(str(score) == '6.3508529610858835')

    def test_rmse_gen_2(self):
        score = rmse_gen([12, 3], [3, 9, 12, 3, 1, 2, 12])
        self.assertTrue(str(score) == '7.648529270389178')

    def test_rmse_gen_3(self):
        score = rmse_gen([], [3, 9, 12, 3, 1, 2, 12])
        self.assertTrue(score == 0)

    def test_rmse_gen_4(self):
        score = rmse_gen([3], [4])
        self.assertTrue(score == 1)

    def test_rmse_gen_same(self):
        score = rmse_gen([3, 9, 12, 3, 1, 2, 12], [3, 9, 12, 3, 1, 2, 12])
        self.assertTrue(score == 0)

    def test_rmse_gen_empty(self):
        score = rmse_gen([], [])
        self.assertTrue(score == 0)

    #
    # rmse_compare
    #
    def test_rmse_compare_1(self):
        score = rmse_compare([[345, 3, 9, 12]], [[345, 3, 9, 1]])
        self.assertTrue(str(score) == '6.3508529610858835')

    def test_rmse_compare_2(self):
        score = rmse_compare([[345, 3, 9, 12], [35, 3], [1, 1, 2, 12]],
                             [[345, 3, 9, 12], [35, 3], [1, 1, 2, 12]])
        self.assertTrue(score == 0)

    def test_rmse_compare_3(self):
        score = rmse_compare([[35]], [[12]])
        self.assertTrue(score == 0)


unittest.main()