#!/usr/bin/env python3
# Eric Nguyen 
# Daniel Robertson

"""
To test the program:
    % python RunNetflix.py &> TestNetflix.out
    % chmod ugo+x RunNetflix.py
    % RunNetflix.py &> TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_calc_squared_diffs, netflix_rmse, netflix_read_actual_ratings, netflix_read, netflix_eval, netflix_cache, netflix_solve

# -----------
# TestCollatz
# -----------

class TestNetflix(unittest.TestCase) :

    # ----
    # calc_squared_diffs
    # ----
    
    def test_calc_squared_diffs_1(self):
      m = 1
      actualRatingsDict = {1: {2015174: 3, 237415: 3, 1170974: 3, 2529844: 3, 1745342: 3, 889245: 3}}
      custIDs = [2015174, 237415, 1170974, 2529844, 1745342, 889245]
      predictions = [3, 3, 3, 3, 3, 3]
      ret = netflix_calc_squared_diffs(m, actualRatingsDict, custIDs, predictions)
      self.assertTrue(len(ret) == len(custIDs))
      self.assertTrue(len(ret) == len(predictions))
      self.assertTrue(round(sum(ret), 0) == 0)
      for squared_diff in ret:
        self.assertTrue(round(squared_diff, 0) == 0)

    def test_calc_squared_diffs_2(self):
      m = 1
      actualRatingsDict = {1: {2015174: 3, 237415: 3, 1170974: 3, 2529844: 3, 1745342: 3, 889245: 3}}
      custIDs = [2015174, 237415, 1170974, 2529844, 1745342, 889245]
      predictions = [2, 2, 2, 2, 2, 2]
      ret = netflix_calc_squared_diffs(m, actualRatingsDict, custIDs, predictions)
      self.assertTrue(len(ret) == len(custIDs))
      self.assertTrue(len(ret) == len(predictions))
      self.assertTrue(round(sum(ret), 1) == 6)
      for squared_diff in ret:
        self.assertTrue(squared_diff == 1)
      
    def test_calc_squared_diffs_3(self):
      m = 1
      actualRatingsDict = {1: {2015174: 3, 237415: 3, 1170974: 3, 2529844: 3, 1745342: 3, 889245: 3}}
      custIDs = [2015174, 237415, 1170974, 2529844, 1745342, 889245]
      predictions = [2.9, 3.6, 1.6, 2.6, 2.7, 1.5]
      ret = netflix_calc_squared_diffs(m, actualRatingsDict, custIDs, predictions)
      self.assertTrue(len(ret) == len(custIDs))
      self.assertTrue(len(ret) == len(predictions))
      self.assertTrue(round(sum(ret), 2) == 4.83)
      self.assertTrue(round(ret[0], 2) == 0.01)
      self.assertTrue(round(ret[1], 2) == 0.36)
      self.assertTrue(round(ret[2], 2) == 1.96)
      self.assertTrue(round(ret[3], 2) == 0.16)
      self.assertTrue(round(ret[4], 2) == 0.09)
      self.assertTrue(round(ret[5], 2) == 2.25)
      
    def test_calc_squared_diffs_4(self):
      m = 1
      actualRatingsDict = {1: {2015174: 5, 237415: 3, 1170974: 2, 2529844: 1, 1745342: 5, 889245: 3}}
      custIDs = [2015174, 237415, 1170974, 2529844, 1745342, 889245]
      predictions = [2.9, 3.6, 1.6, 2.6, 2.7, 1.5]
      ret = netflix_calc_squared_diffs(m, actualRatingsDict, custIDs, predictions)
      self.assertTrue(len(ret) == len(custIDs))
      self.assertTrue(len(ret) == len(predictions))
      self.assertTrue(round(sum(ret), 2) == 15.03)
      self.assertTrue(round(ret[0], 2) == 4.41)
      self.assertTrue(round(ret[1], 2) == 0.36)
      self.assertTrue(round(ret[2], 2) == 0.16)
      self.assertTrue(round(ret[3], 2) == 2.56)
      self.assertTrue(round(ret[4], 2) == 5.29)
      self.assertTrue(round(ret[5], 2) == 2.25)
      
    def test_calc_squared_diffs_5(self):
      m = 1
      actualRatingsDict = {1: {2015174: 5, 237415: 3, 1170974: 2, 2529844: 1, 1745342: 5, 889245: 3}}
      custIDs = [2015174, 237415, 1170974, 2529844, 1745342, 889245]
      predictions = [1.0, 2.0, 3.7, 2.6, 1.4, 4.7]
      ret = netflix_calc_squared_diffs(m, actualRatingsDict, custIDs, predictions)
      self.assertTrue(len(ret) == len(custIDs))
      self.assertTrue(len(ret) == len(predictions))
      self.assertTrue(round(sum(ret), 2) == 38.3)
      self.assertTrue(round(ret[0], 2) == 16)
      self.assertTrue(round(ret[1], 2) == 1)
      self.assertTrue(round(ret[2], 2) == 2.89)
      self.assertTrue(round(ret[3], 2) == 2.56)
      self.assertTrue(round(ret[4], 2) == 12.96)
      self.assertTrue(round(ret[5], 2) == 2.89)


    # ----
    # rmse
    # ----
    
    def test_rmse_1(self):
      l = []
      ret = netflix_rmse(l)
      assert(round(ret, 0) == 0)
      
    def test_rmse_2(self):
      l = [0]
      ret = netflix_rmse(l)
      assert(round(ret, 0) == 0)
      
    def test_rmse_3(self):
      l = [2.5, 4.1, 3.9, 1.5, 1.6, 3.8, 3.3, 3.1, 3.2, 2.2]
      ret = netflix_rmse(l)
      assert(round(ret, 2) == 1.71)
      
    def test_rmse_4(self):
      l = [2.1, 2.9, 1.4, 2.5, 4.5, 3.3, 2.2, 3.2, 2.4, 1.8, 3.7, 3.9, 2.8, 4.0, \
           4.2, 2.6, 2.1, 3.6, 1.1, 2.3, 4.9, 4.7, 1.3, 4.9, 4.6]
      ret = netflix_rmse(l)
      assert(round(ret, 2) == 1.75)
      
    def test_rmse_5(self):
      l = [4.6, 2.3, 4.6, 4.5, 1.7, 4.8, 1.8, 2.4, 2.9, 4.1, 4.6, 2.9, 2.6, 4.3, \
           3.4, 3.6, 2.8, 4.6, 3.7, 1.3, 4.4, 2.9, 4.9, 2.2, 2.5, 3.4, 1.2, 2.1, \
           4.0, 2.1, 2.2, 3.9, 1.4, 2.7, 3.9, 1.1, 3.8, 4.8, 2.3, 4.8, 4.2, 1.2, \
           2.4, 2.9, 1.5, 4.3, 2.7, 2.1, 1.1, 3.1, 4.9, 1.6, 4.7, 1.1, 4.0, 3.1, \
           4.1, 2.6, 4.2, 4.3, 3.0, 3.2, 2.7, 2.7, 1.0, 2.8, 5.0, 1.2, 2.3, 1.1, \
           4.6, 4.5, 1.3, 4.2, 4.3, 3.5, 1.3, 3.4, 3.7, 4.7, 1.0, 1.5, 4.8, 3.6, \
           4.5, 4.6, 4.5, 4.7, 3.6, 1.8, 1.7, 2.7, 3.5, 1.4, 1.8, 3.1, 4.9, 2.2, \
           1.7, 4.3]
      ret = netflix_rmse(l)
      assert(round(ret, 2) == 1.76)
    
    
    # ----
    # read_actual_ratings
    # ----
    
    # Since we use absolute paths as inputs for netflix_read_actual_ratings(), we can't 
    # test the function, since we are prohibited from creating additional files.
    
    
    # ----
    # read
    # ----
    
    def test_read_1 (self) :
        r = io.StringIO("\n")
        ret = netflix_read(r)
        self.assertTrue(len(ret) == 0)
    
    def test_read_2 (self) :
        r = io.StringIO("1:\n30878\n")
        ret = netflix_read(r)
        self.assertTrue(len(ret) == 1)
        self.assertTrue(len(ret[0][1]) == 1)
        self.assertTrue(ret[0][1][0] == 30878)
        
    def test_read_3 (self) :
        r = io.StringIO("1:\n30878\n2:\n1959936\n")
        ret = netflix_read(r)
        self.assertTrue(len(ret) == 2)
        self.assertTrue(len(ret[0][1]) == 1)
        self.assertTrue(ret[0][1][0] == 30878)
        self.assertTrue(len(ret[1][1]) == 1)
        self.assertTrue(ret[1][1][0] == 1959936)
        
    def test_read_4 (self) :
        r = io.StringIO("1032:\n1785214\n2269122\n5583:\n1967828\n1583391\n976453\n1679704\n839409\n\
                         573975\n2385745\n191785\n353820\n1550265\n2054960\n730191\n761609\n1693513\n\
                         907156\n1387019\n1253628\n1790351\n1746351\n1943107\n347799\n1169732\n\
                         1300985\n1008661\n896738\n2121294\n173128\n1481737\n2398879\n2470704\n\
                         3342:\n1050707\n517710\n1737096\n26679\n1575680\n49632\n1593150\n2220142\n\
                         605068\n1998010\n2224673\n737619\n854679\n")
        ret = netflix_read(r)
        self.assertTrue(len(ret) == 3)
        self.assertTrue(ret[0][0] == 1032)
        self.assertTrue(len(ret[0][1]) == 2)
        self.assertTrue(ret[1][0] == 5583)
        self.assertTrue(len(ret[1][1]) == 30)
        self.assertTrue(ret[2][0] == 3342)
        self.assertTrue(len(ret[2][1]) == 13)
       
        
    # ----
    # eval
    # ----
    
    def test_eval_1 (self) :
      m = 1
      l = []
      movieAvgCache = {}
      custAvgCache = {}
      movieDecadeAvgCache = {}
      numRatingsCache = {}
      ret = netflix_eval(m, l, movieAvgCache, custAvgCache, movieDecadeAvgCache, numRatingsCache)
      self.assertTrue(ret[0] == 1)
      self.assertTrue(len(ret[1]) == 0)
    
    def test_eval_2 (self) :
      m = 10323
      l = [2538479]
      movieAvgCache = {10323: 2.311320754716981}
      custAvgCache = {2538479: 4.078947368421052}
      movieDecadeAvgCache = {10323: 3.68432937714}
      numRatingsCache = {10323: 1}
      ret = netflix_eval(m, l, movieAvgCache, custAvgCache, movieDecadeAvgCache, numRatingsCache)
      self.assertTrue(ret[0] == 10323)
      self.assertTrue(len(ret[1]) == 1)
      self.assertTrue(ret[1][0] == 2.8)
    
    def test_eval_3 (self) :
      m = 5072
      l = [2520822, 2130386, 1523793, 892252, 59194]
      movieAvgCache = {10323: 2.311320754716981, 5072: 4.3792744315223056}
      custAvgCache = {2538479: 4.078947368421052, 2520822: 3.4482758620689653, 2130386: 4.045454545454546, \
                      1523793: 3.87279843444227, 892252: 3.5426170468187275, 59194: 4.232}
      movieDecadeAvgCache = {10323: 3.68432937714, 5072: 3.52276235404}
      numRatingsCache = {10323: 1, 5072: 41}
      ret = netflix_eval(m, l, movieAvgCache, custAvgCache, movieDecadeAvgCache, numRatingsCache)
      self.assertTrue(ret[0] == 5072)
      self.assertTrue(len(ret[1]) == 5)
      self.assertTrue(ret[1][0] == 4.2)
      self.assertTrue(ret[1][1] == 4.7)
      self.assertTrue(ret[1][2] == 4.6)
      self.assertTrue(ret[1][3] == 4.3)
      self.assertTrue(ret[1][4] == 4.9)
    
    
    # ----
    # cache
    # ----

    def test_cache_1(self) :
    	CUST_AVG_CACHE_FILE = "/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt"
    	cache = netflix_cache(CUST_AVG_CACHE_FILE)
    	self.assertTrue(cache[1048577] == 4.576923076923077)

    def test_cache_2(self) :
    	CUST_AVG_CACHE_FILE = "/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt"
    	cache = netflix_cache(CUST_AVG_CACHE_FILE)
    	self.assertTrue(cache[1048579] == 3.6231422505307855)

    def test_cache_3(self) :
    	CUST_AVG_CACHE_FILE = "/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt"
    	cache = netflix_cache(CUST_AVG_CACHE_FILE)
    	self.assertTrue(cache[10] == 3.3923076923076922)

    def test_cache_4(self) :
    	MOVIE_AVG_CACHE_FILE = "/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt"
    	cache = netflix_cache(MOVIE_AVG_CACHE_FILE)
    	self.assertTrue(cache[1] == 3.749542961608775)

    def test_cache_5(self) :
    	MOVIE_AVG_CACHE_FILE = "/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt"
    	cache = netflix_cache(MOVIE_AVG_CACHE_FILE)
    	self.assertTrue(cache[12] == 3.4175824175824174)


    # ----
    # solve
    # ----

    def test_solve_1 (self) :
        r = io.StringIO("1:\n30878\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.8\nRMSE: 0.2\n")

    def test_solve_2 (self) :
        r = io.StringIO("1:\n30878\n2647871\n1283744\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.8\n3.4\n3.7\nRMSE: 0.5447\n")

    def test_solve_3 (self) :
        r = io.StringIO("1:\n30878\n2647871\n1283744\n2488120\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.8\n3.4\n3.7\n4.7\nRMSE: 0.495\n")

    def test_solve_4 (self) :
        r = io.StringIO("10:\n1952305\n1531863\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10:\n3.0\n2.8\nRMSE: 0.1414\n")

    def test_solve_5 (self) :
        r = io.StringIO("2297:\n346873\n122317\n1348053\n1274643\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "2297:\n3.3\n3.8\n3.8\n3.7\nRMSE: 0.7176\n")	
        
        
# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
