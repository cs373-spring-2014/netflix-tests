#!/usr/bin/env python3

# -------------------------------
# Netflix Project
# Jung Yoon
# CS373 Spring 2014
# Prof. Glenn P. Downing
# -------------------------------

import sys
import io
import unittest

from Netflix import read_input, calculate_rsme, solve, predict_rating

class TestNetflix (unittest.TestCase) :

   # ----------
   # read_input
   # ----------
   def test_read1(self) :   # general test
      r = io.StringIO("3:\n23\n543\n")
      for (movie, user) in read_input(r) :
         self.assertTrue(movie == "1")
         self.assertTrue(user[0] == "23")
         self.assertTrue(user[1] == "543")

   def test_read2(self) :   # skips if not given a movie ID first
      r = io.StringIO("45\n3223\n1:\n23\n543\n")
      for (movie, user) in read_input(r) :
         self.assertTrue(movie == "1")
         self.assertTrue(user[0] == "23")
         self.assertTrue(user[1] == "543")

   def test_read3(self) :   # not a valid movie ID
      r = io.StringIO("17773:\n23\n543\n1:\n23\n543\n")
      for (movie, user) in read_input(r) :
         self.assertTrue(movie == "1")
         self.assertTrue(user[0] == "23")
         self.assertTrue(user[1] == "543")

   # ----------
   # calculate_rsme
   # ----------
   def test_rsme1(self) :   # same list
      a = [1,2,3,4,5,6,7]
      p = [1,2,3,4,5,6,7]
      rmse = calculate_rsme(a,p)
      self.assertTrue(rmse == 0)

   def test_rsme2(self) :   # general test
      a = [7,47,11,20,98]
      p = [9,45,13,22,96]
      rmse = calculate_rsme(a,p)
      self.assertTrue(rmse == 2)   

   def test_rsme3(self) :   # same a and same b
      a = [3]*30
      p = [4]*30
      rmse = calculate_rsme(a,p)
      self.assertTrue(rmse == 1)

   # ----------
   # predict_rating
   # ----------
   def test_predict1(self) :   # negative result
      avg_movie_rating = {'1': 1}
      avg_user_rating = {'23': 1}
      movie = "1"
      user = "23"
      prediction = predict_rating(movie, user, avg_movie_rating, avg_user_rating)
      self.assertTrue(prediction == 1.0)

   def test_predict2(self) :   # over-bound
      avg_movie_rating = {'1': 5}
      avg_user_rating = {'23': 5}
      movie = "1"
      user = "23"
      prediction = predict_rating(movie, user, avg_movie_rating, avg_user_rating)
      self.assertTrue(prediction == 5.0)

   def test_predict3(self) :   # under-bound
      avg_movie_rating = {'1': 1}
      avg_user_rating = {'23': 3}
      movie = "1"
      user = "23"
      prediction = predict_rating(movie, user, avg_movie_rating, avg_user_rating)
      self.assertTrue(prediction == 1.0)

   # ----------
   # solve
   # ----------
   def test_solve1(self) :   # empty input file
      r = io.StringIO("")
      try :
         self.assertTrue(solve(r) == "Did not execute")
      except :
         self.assertTrue(true)

   def test_solve2(self) :   # input file not in correct format
      r = io.StringIO("323\n3432\n9332\n2\n3423\n3423\n3432\n")
      try :
         self.assertTrue(solve(r) == "Did not execute")
      except :
         self.assertTrue(true)

   def test_solve3(self) :   # got to end of program
      r = io.StringIO("buzzbuzzbuzz")
      try :
         self.assertTrue(solve(r) == "Did not execute")
      except :
         self.assertTrue(true)
  
""""""""""""""""""""""""""""""
print("TestNetflix.py")
unittest.main()
print("Done.")
