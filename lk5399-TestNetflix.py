#!/usr/bin/env python3
# -------
# imports
# -------

import io
import unittest
import Netflix
import json
from Netflix import netflix_read, netflix_eval, rmse_sum_gen_zip, netflix_solve

# -----------
# TestCollatz
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read1 (self) :
        r = io.StringIO("2043:\n")
        b = netflix_read(r)
        a= Netflix.movie
        c=Netflix.customer
        self.assertTrue(b == True)
        self.assertTrue(a ==  "2043")


    def test_read2 (self) :
        r = io.StringIO("1059\n")
        b = netflix_read(r)
        a= Netflix.movie
        c=Netflix.customer

        self.assertTrue(b == True)
        self.assertTrue(c ==  "1059")

    def test_read3 (self) :
        r = io.StringIO("")
        b = netflix_read(r)
        a= Netflix.movie
        c=Netflix.customer
        self.assertTrue(b == False)





    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        with open("/u/thunt/cs373-netflix-tests/irvin-user_avg.json", "r") as a:
            user_rating=json.load(a)
        with open("/u/thunt/cs373-netflix-tests/irvin-movie_avg.json", "r") as c:
            movie_rating=json.load(c)
        with open("/u/thunt/cs373-netflix-tests/timsim-probe_ans.json", "r") as f:
            actual_rating = json.load(f)
        Netflix.movie="1"
        Netflix.customer="2446680"
        v=netflix_eval(user_rating,movie_rating)
        self.assertTrue(v == 3.0)

    def test_eval_2 (self) :
        with open("/u/thunt/cs373-netflix-tests/irvin-user_avg.json", "r") as a:
            user_rating=json.load(a)
        with open("/u/thunt/cs373-netflix-tests/irvin-movie_avg.json", "r") as c:
            movie_rating=json.load(c)
        with open("/u/thunt/cs373-netflix-tests/timsim-probe_ans.json", "r") as f:
            actual_rating = json.load(f)
        Netflix.movie="52"
        Netflix.customer="7"
        v=netflix_eval(user_rating,movie_rating)
        self.assertTrue(v == 3.9)

    def test_eval_3 (self) :
        with open("/u/thunt/cs373-netflix-tests/irvin-user_avg.json", "r") as a:
            user_rating=json.load(a)
        with open("/u/thunt/cs373-netflix-tests/irvin-movie_avg.json", "r") as c:
            movie_rating=json.load(c)
        with open("/u/thunt/cs373-netflix-tests/timsim-probe_ans.json", "r") as f:
            actual_rating = json.load(f)
        Netflix.movie="1"
        Netflix.customer="1048619"
        v=netflix_eval(user_rating,movie_rating)
        self.assertTrue(v == 3.5)


    # # -----
    # # rmse_calc
    # # -----
    def test_rsme1(self) :
        a=[2,3,4]
        p=[2,3,4]
        v=rmse_sum_gen_zip(a,p)
        self.assertTrue(v==0.00000)
    def test_rsme2(self) :
        a=[5,6,7]
        p=[5,6,7]
        v=rmse_sum_gen_zip(a,p)
        self.assertTrue(v==0.00000)

    def test_rsme3(self) :
        a=[2,3,4]
        p=[3,4,5]
        v=rmse_sum_gen_zip(a,p)
        self.assertTrue(v==1.00000)



    # # -----
    # # netflix_solve
    # # -----
    def test_solve1 (self) :
        r = io.StringIO("15065:\n2166661")
        w = io.StringIO()
        netflix_solve(r, w)
        

    def test_solve2 (self) :
        r = io.StringIO("15068:\n1370255")
        w = io.StringIO()
        netflix_solve(r, w)


    def test_solve3 (self) :
        r = io.StringIO("4:\n410199")
        w = io.StringIO()
        netflix_solve(r, w)
   
# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
