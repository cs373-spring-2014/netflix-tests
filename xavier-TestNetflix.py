import io
import unittest

from Netflix import loadCache,loadStandardCache,netflix_read,netflix_write,netflix_CalcPerMovie,netflix_solve,netflix_CalcPerPerson,calcRMSE


class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----
    
    def test_netflix_read_1 (self) :
        r = io.StringIO("1:\n30878\n2647871\n1283744\n")
        a = list(netflix_read(r))
        i, users = a[0][0], a[0][1]
        self.assertTrue(i ==  1)
        self.assertTrue(users[0] == 30878)
        self.assertTrue(users[1] == 2647871)
        self.assertTrue(users[2] == 1283744)


    def test_netflix_read_2 (self) :
        r = io.StringIO("5161:\n30878\n")
        a = list(netflix_read(r))
        i, users = a[0][0], a[0][1]
        self.assertTrue(i ==  5161)
        self.assertTrue(users[0] == 30878)

    def test_read_3 (self) :
        r = io.StringIO("1001:\n1050889\n67976\n1025642\n624334\n239718\n549109\n")
        a = list(netflix_read(r))
        i, users = a[0][0], a[0][1]
        self.assertTrue(i ==  1001)
        self.assertTrue(users[0] == 1050889)
        self.assertTrue(users[1] == 67976)
        self.assertTrue(users[2] == 1025642)
        self.assertTrue(users[3] == 624334)
        self.assertTrue(users[4] == 239718)
        self.assertTrue(users[5] == 549109)
    
    # ----
    # eval
    # ----

    def test_netflix_write_1 (self) :
        w = io.StringIO()
        i = 2043
        users = [3.4, 4.1, 1.9]
        netflix_write(w, (i, users))
        self.assertTrue(w.getvalue() == "2043:\n3.4\n4.1\n1.9\n")

    def test_netflix_write_2 (self) :
        w = io.StringIO()
        i = 10851
        users = [4.3, 1.4, 2.8]
        netflix_write(w, (i, users))
        self.assertTrue(w.getvalue() == "10851:\n4.3\n1.4\n2.8\n")

    def test_netflix_write_3 (self) :
        w = io.StringIO()
        i = 5
        users = [4.3]
        netflix_write(w, (i, users))
        self.assertTrue(w.getvalue() == "5:\n4.3\n")

    # -----
    # solve
    # -----

    def test_netflix_solve_1 (self) :
        r = io.StringIO("10014:\n1626179\n1359761\n430376\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10014:\n3.4149656891305753\n4.058889078957015\n3\nRMSE: 0.981124808085621\n")

    def test_netflix_solve_2 (self) :
        r = io.StringIO("10015:\n975179\n829739\n1732761\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10015:\n3.743820039551747\n3.564615931721195\n3.4304054054054056\nRMSE: 0.4860923522267629\n")

    def test_netflix_solve_3 (self) :
        r = io.StringIO("10016:\n1751359\n234929\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10016:\n3.2588101604278075\n2.7345971974648444\nRMSE: 1.5154917448553085\n")

    def test_CalcPerMovie_1 (self) :
        i = 10014
        users = [1626179, 1359761, 430376]
        ratings = "[3.4149656891305753, 4.058889078957015, 3]"
        caches = loadCache()
        result = netflix_CalcPerMovie((i,users), caches)
        self.assertTrue(result[0]==i)
        self.assertTrue(str(result[1])==ratings)

    def test_CalcPerMovie_2 (self) :
        i = 10015
        users = [975179, 829739, 1732761]
        ratings = "[3.743820039551747, 3.564615931721195, 3.4304054054054056]"
        caches = loadCache()
        result = netflix_CalcPerMovie((i,users), caches)
        self.assertTrue(result[0]==i)
        self.assertTrue(str(result[1])==ratings)

    def test_CalcPerMovie_3 (self) :
        i = 10016
        users = [1751359, 234929]
        ratings = "[3.2588101604278075, 2.7345971974648444]"
        caches = loadCache()
        result = netflix_CalcPerMovie((i,users), caches)
        self.assertTrue(result[0]==i)
        self.assertTrue(str(result[1])==ratings)

    def test_netflix_CalcPerPerson_1(self) :
        movie = 2043
        person = 1417435
        rating = "3.683567791980701"
        caches = loadCache()
        result = netflix_CalcPerPerson(movie,person,caches)
        self.assertTrue(str(result)==rating)

    def test_netflix_CalcPerPerson_2(self) :
        movie = 2043
        person = 2312054
        rating = "4.253299453690763"
        caches = loadCache()
        result = netflix_CalcPerPerson(movie,person,caches)
        self.assertTrue(str(result)==rating)

    def test_netflix_CalcPerPerson_3(self) :
        movie = 10851
        person = 1417435
        rating = "3.733450881612091"
        caches = loadCache()
        result = netflix_CalcPerPerson(movie,person,caches)
        self.assertTrue(str(result)==rating)                
    
    def test_calcRMSE_1(self) :
        a = [1,2,3]
        b = [2,3,4]
        r = 1.0
        result = calcRMSE(a,b)
        self.assertTrue(r==result)

    def test_calcRMSE_2(self) :
        a = [5,8,0]
        b = [7,16,44]
        r = "25.84569596664017"
        result = calcRMSE(a,b)
        self.assertTrue(str(r)==str(result))
    
    def test_calcRMSE_3(self) :
        a = [15,15,15]
        b = [15,15,15]
        r = "0.0"
        result = calcRMSE(a,b)
        self.assertTrue(str(r)==str(result))
    
    def test_loadCache_1(self) :
        d = loadCache()
        assert("custAvg" in d)
        assert(len(d["custAvg"])>0)

    def test_loadCache_2(self) :
        d = loadCache()
        assert("movieAvg" in d)
        assert(len(d["movieAvg"])>0)

    def test_loadCache_3(self) :
        d = loadCache()
        assert("probeResults" in d)
        assert(len(d["probeResults"])>0)   

    def test_loadStandardCache1(self) :
        f = open("/v/filer4b/v20q001/thunt/cs373-netflix-tests/ericweb2-movieAverages.txt")
        d = loadStandardCache(f,False)
        f.close()
        assert(len(d)>0)

    def test_loadStandardCache2(self) :
        f = open("/v/filer4b/v20q001/thunt/cs373-netflix-tests/ericweb2-custAverages.txt")
        d = loadStandardCache(f,False)
        f.close()
        assert(len(d)>0)

    def test_loadStandardCache3(self) :
        f = open("/v/filer4b/v20q001/thunt/cs373-netflix-tests/word-probeMapCache.txt")
        d = loadStandardCache(f,True)
        f.close()
        assert(len(d)>0)    
    
# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
