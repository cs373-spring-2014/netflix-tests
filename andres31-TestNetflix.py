#!/usr/bin/env python3



"""
To test the program:
    % python TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestCollatz.py
    % TestNetflix.py > TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, predict_and_populate, parse_data, is_movie_id, rmse

# -----------
# TestCollatz
# -----------

class TestNetflix (unittest.TestCase) :
    
    # ----
    # netflix_read
    # ----

	def test_read_1 (self) :
		inputString = io.StringIO("2043:\n1417435\n2312054\n462685\n10851:\n1417435\n2312054\n462685\n")
		outputString = netflix_read(inputString)
		result_1 = outputString.__next__()
		result_2 = outputString.__next__()
		result_3 = outputString.__next__()
		result_4 = outputString.__next__()
		result_5 = outputString.__next__()
		result_6 = outputString.__next__()
		result_7 = outputString.__next__()
		result_8 = outputString.__next__()
		self.assertTrue(result_1 == "2043:\n")
		self.assertTrue(result_2 == "1417435\n")
		self.assertTrue(result_3 == "2312054\n")
		self.assertTrue(result_4 == "462685\n")
		self.assertTrue(result_5 == "10851:\n")
		self.assertTrue(result_6 == "1417435\n")
		self.assertTrue(result_7 == "2312054\n")
		self.assertTrue(result_8 == "462685\n")


	def test_read_2 (self) :
		inputString = io.StringIO("1:\n14\n")
		outputString = netflix_read(inputString)
		result_1 = outputString.__next__()
		result_2 = outputString.__next__()
		self.assertTrue(result_1 == "1:\n")
		self.assertTrue(result_2 == "14\n")


	def test_read_3 (self) :
		inputString = io.StringIO("Hello\nmy\nname\nis\nAndres\nLugo")
		outputString = netflix_read(inputString)
		result_1 = outputString.__next__()
		result_2 = outputString.__next__()
		result_3 = outputString.__next__()
		result_4 = outputString.__next__()
		result_5 = outputString.__next__()
		result_6 = outputString.__next__()
		self.assertTrue(result_1 == "Hello\n")
		self.assertTrue(result_2 == "my\n")
		self.assertTrue(result_3 == "name\n")
		self.assertTrue(result_4 == "is\n")
		self.assertTrue(result_5 == "Andres\n")
		self.assertTrue(result_6 == "Lugo")
	
    # -----------
    # is_movie_id
    # -----------

	def test_is_movie_id_1(self) :
		inputString_1 = "2043:\n"
		inputString_2 = "141743\n"
		result_1 = is_movie_id(inputString_1)
		result_2 = is_movie_id(inputString_2)
		self.assertTrue(result_1 == True)
		self.assertTrue(result_2 == False)

	def test_is_movie_id_2(self) :
		inputString_1 = 2342
		result_1 = is_movie_id(inputString_1)
		self.assertTrue(result_1 == False)


	def test_is_movie_id_3(self) :
		inputString_1 = "475247592475092347502839475923745092345856203975609234:"
		inputString_2 = "475247592475092347502839475923745092345856203975609234:\n"
		result_1 = is_movie_id(inputString_1)
		result_2 = is_movie_id(inputString_2)
		self.assertTrue(result_1 == False)
		self.assertTrue(result_2 == True)
   		
    # ----------
    # parse_data
    # ----------

	def test_parse_data_1(self) :
		r = io.StringIO("10:\n1952305\n1531863")
		w = io.StringIO()
		parse_data(r, w)
		self.assertTrue(w.getvalue() == "10:\n2.890063550906924\n2.6353281547241596\nRMSE: 0.2693246902681603")


	def test_parse_data_2(self) :
		r = io.StringIO("10:\n1952305\n1531863\n10000:\n200206\n523108")
		w = io.StringIO()
		parse_data(r, w)
		value = w.getvalue()
		self.assertTrue(value == "10:\n2.890063550906924\n2.6353281547241596\n10000:\n3.608575581395349\n3.4624411883317654\nRMSE: 0.6474532858108527")

	def test_parse_data_3(self) :
		r = io.StringIO("10:\n1952305\n1531863\n10000:\n200206\n523108\n10003:\n1515111")
		w = io.StringIO()
		parse_data(r, w)
		value = w.getvalue()
		self.assertTrue(value == "10:\n2.890063550906924\n2.6353281547241596\n10000:\n3.608575581395349\n3.4624411883317654\n10003:\n2.3812897196261678\nRMSE: 0.6920378287841721")


	# --------------------
	# predict_and_populate
	# --------------------

	def test_predict_and_populate_1(self) :
		w = io.StringIO()
		predict_and_populate("30878", 3.6336173508907823, 3.749542961608775, "1", w)
		value = w.getvalue()
		self.assertTrue(value == "3.6831603124995573\n")


	def test_predict_and_populate_2(self) :
		w = io.StringIO()
		predict_and_populate("1952305", 3.6336173508907823, 3.749542961608775, "10", w)
		value = w.getvalue()
		print ("the value is " + value)
		self.assertTrue(value == "3.6831603124995573\n")

	def test_predict_and_populate_3(self) :
		w = io.StringIO()
		predict_and_populate("1450941", 4.508474576271187, 3.4405487804878048, "10002", w)
		value = w.getvalue()
		print ("the value is " + value)
		self.assertTrue(value == "4.2490233567589915\n")



	def test_rmse_1(self) :
		a = [2,3,4]
		p = [2,3,4]
		result = rmse(a,p)
		self.assertTrue(result == 0.0)

	def test_rmse_2(self) :
		a = [2,3,4]
		p = [3,4,5]
		result = rmse(a,p)
		self.assertTrue(result == 1.0)

	def test_rmse_3(self) :
		a = [2,3,4]
		p = [4,3,2]
		result = rmse(a,p)
		self.assertTrue(result == 1.632993161855452)
# ----
# main
# ----

print ("TestNetflix.py")
unittest.main()
print ("Done.")
