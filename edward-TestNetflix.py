#!/usr/bin/env python3

import io
import unittest

from Netflix import netflix_read, netflix_print, netflix_predictor, netflix_solve

class TestNetflix (unittest.TestCase) : 

    # ----
    #test read
    # ----

    def test_read_0(self) :
        r = io.StringIO("")
        a = netflix_read(r)
        i, j = list(a)[0]
        self.assertTrue(i == 0)
        self.assertTrue(j == [])

    def test_read_1(self) :
        r = io.StringIO("2043:\n1417435\n2312054\n462685\n")
        t = (0, [])
        a = netflix_read(r)
        i, j = list(a)[0]
        self.assertTrue(a)
        self.assertTrue(i == 2043)
        self.assertTrue(j == [1417435,2312054,462685])

    def test_read_2(self) :
        r = io.StringIO("2043:\n1417435\n2312054\n462685\n10851:\n1417435\n2312054\n462685")
        a = netflix_read(r)
        k = list(a)
        i, j = k[0]
        self.assertTrue(i == 2043)
        self.assertTrue(j == [1417435,2312054,462685])
        i, j = k[1]
        self.assertTrue(i == 10851)
        self.assertTrue(j == [1417435,2312054,462685])

    # ----
    #test print
    # ----

    def test_print_0(self) :
        w = io.StringIO()
        r = (2043,[3.4,4.1,1.9])
        netflix_print(w, r)
        self.assertTrue(w.getvalue() == "2043:\n3.4\n4.1\n1.9\n")

    # ----
    #test netflix_predictor
    # ----

    def test_predictor_0 (self) :
        m = (10, [1952305, 1531863])
        output = netflix_predictor(m)
        self.assertTrue(len(output) == 2)
        for rating in output:
            self.assertTrue(float(rating) >= 1.0)
            self.assertTrue(float(rating) <= 5.0)

    def test_predictor_1 (self) :
        m = (10000,[200206])
        output = netflix_predictor(m)
        self.assertTrue(len(output) == 1)
        for rating in output:
            self.assertTrue(float(rating) >= 1.0)
            self.assertTrue(float(rating) <= 5.0)

    def test_predictor_2 (self) :
        m = (1, [30878, 2647871, 1283744, 2488120, 317050, 1904905])
        output = netflix_predictor(m)
        self.assertTrue(len(output) == 6)
        for rating in output:
            self.assertTrue(float(rating) >= 1.0)
            self.assertTrue(float(rating) <= 5.0)

    # ----
    #test netflix_solve
    # ----

    def test_solve_0 (self) :
        r = io.StringIO("10:\n1952305\n1531863\n")
        w = io.StringIO()
        output = netflix_solve(r,w)
        self.assertTrue(output < 1.0 )

    def test_solve_1 (self) :
        r = io.StringIO("10:\n1952305\n1531863\n10000:\n200206\n523108\n")
        w = io.StringIO()
        output = netflix_solve(r,w)
        self.assertTrue(output < 1.0 )

    def test_solve_2 (self) :
        r = io.StringIO("10:\n1952305\n1531863\n10000:\n200206\n523108\n10011:\n1624701\n2646826\n")
        w = io.StringIO()
        output = netflix_solve(r,w)
        self.assertTrue(output < 1.0 )

    def test_solve_3 (self) :
        r = io.StringIO("10001:\n262828\n2609496\n1474804\n831991\n267142\n2305771\n220050\n1959883\n27822\n2570808\n90355\n2417258\n264764\n143866\n766895\n714089\n2350428\n")
        w = io.StringIO()
        output = netflix_solve(r,w)
        self.assertTrue(output < 1.0 )

    def test_solve_4 (self) :
        r = io.StringIO("10001:\n262828\n2609496\n1474804\n831991\n267142\n2305771\n220050\n1959883\n27822\n2570808\n90355\n2417258\n264764\n143866\n766895\n714089\n2350428\n1001:\n1050889\n67976\n1025642\n624334\n239718\n549109\n143504\n584301\n437680\n2434971\n400819\n1841130\n1926199\n805709\n545109\n662875\n74408\n529725\n1786012\n2382523\n1358422\n780049\n1894639\n1014799\n2176030\n286045\n1381947\n386254\n224172\n404085\n2005305\n1956315\n2580275\n2110738\n1525405\n2187027\n1995906\n2133433\n2097983\n884805\n2083077\n1642134\n2550380\n2559056\n1080407\n1455000\n498315\n2464251\n1149181\n2117376\n1368543\n1216717\n1727516\n1771413\n2326782\n2423679\n1198786\n1042938\n2640911\n328713\n2591056\n256030\n1363889\n2615076\n52736\n1359594\n1385396\n2414993\n1274754\n715115\n795536\n1991455\n1107351\n1451770\n1433190\n1769644\n875020\n2456714\n906813\n837457\n672807\n1863197\n2585014\n666419\n1849372\n1308814\n2344636\n755775\n768710\n2334860\n468090\n1996056\n1603215\n274970\n1060880\n721144\n1381762\n1589154\n2033841\n1372601\n2339869\n41031\n655047\n916708\n1924771\n1686092\n1552908\n329102\n1181907\n1762077\n1583835\n773994\n25452\n321005\n1369500\n2429351\n396491\n1745676\n2204817\n2624938\n491487\n1298591\n77828\n1047191\n2642897\n2494183\n1337506\n1802655\n126058\n940778\n1988427\n1720476\n1947287\n31062\n")
        w = io.StringIO()
        output = netflix_solve(r,w)
        self.assertTrue(output < 1.0 )

# main
print("TestNetflix.py")
unittest.main()
print("Done.")