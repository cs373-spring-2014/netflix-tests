import unittest
import Netflix
import json
import os


class TestNetflix(unittest.TestCase):
    def setUp(self):
        # setup for rmse
        self.a = [("1", 1.0), ("2", 2.0), ("3", 3.0)]
        self.b = [("1", 2.0), ("2", 3.0), ("3", 4.0)]
        self.c = [("1", 1.0), ("2", 2.0), ("3", 3.0), ("3", 4.0)]
        # setup for get_rating_dict
        self.n = [1, 2, 3]
        self.s = ["a", "b", "c"]
        self.x = 0
        self.y = 0
        # setup for get_rating
        self.user1 = {"id": 1, "movie_ave": 1.0}
        self.user2 = {"id": 2, "movie_ave": 2.0}
        self.user3 = {"id": 3, "movie_ave": 3.0}
        self.user4 = {"id": 4, "movie_ave": 4.0}
        self.user5 = {"id": 5, "movie_ave": 5.0}
        self.movie1 = {"id": 1, "ave": 1.0}
        self.movie2 = {"id": 2, "ave": 2.0}
        self.movie3 = {"id": 3, "ave": 3.0}
        self.movie4 = {"id": 4, "ave": 4.0}
        self.movie5 = {"id": 5, "ave": 5.0}
        self.users = [self.user1, self.user2, self.user3, self.user4, self.user5]
        self.movies = [self.movie1, self.movie2, self.movie3, self.movie4, self.movie5]
        self.overall_average = 3.7
        self.user_averages = {}
        for u in self.users:
            self.user_averages[u["id"]] = u["movie_ave"]

        self.movie_averages = {}
        for m in self.movies:
            self.movie_averages[m["id"]] = m["ave"]


    def test_rmse_1(self):
        self.assertTrue(Netflix.rmse(self.a, self.a) == 0.0)

    def test_rmse_2(self):
        self.assertTrue(Netflix.rmse(self.a, self.b) == 1.0)

    def test_rmse_3(self):
        self.assertRaises(TypeError, Netflix.rmse, 0, self.a)
        self.assertRaises(TypeError, Netflix.rmse, self.a, 0)

    def test_rmse_4(self):
        self.assertRaises(AssertionError, Netflix.rmse, self.a, self.c)

    def test_estimate_rating_1(self):
        self.assertTrue(Netflix.estimate_rating(self.user1["id"], self.movie1["id"], self.overall_average, self.user_averages,
                                                self.movie_averages) < Netflix.estimate_rating(self.user5["id"],
                                                                                               self.movie5["id"],
                                                                                               self.overall_average,
                                                                                               self.user_averages,
                                                                                               self.movie_averages))

    def test_estimate_rating_2(self):
        self.assertTrue(Netflix.estimate_rating(self.user5["id"], self.movie5["id"], self.overall_average, self.user_averages,
                                                self.movie_averages) > Netflix.estimate_rating(self.user1["id"],
                                                                                               self.movie1["id"],
                                                                                               self.overall_average,
                                                                                               self.user_averages,
                                                                                               self.movie_averages))

    def test_estimate_rating_3(self):
        self.assertTrue(Netflix.estimate_rating(self.user3["id"], self.movie3["id"], self.overall_average, self.user_averages,
                                                self.movie_averages) == Netflix.estimate_rating(self.user3["id"],
                                                                                                self.movie3["id"],
                                                                                                self.overall_average,
                                                                                                self.user_averages,
                                                                                                self.movie_averages))

    def test_estimate_rating_4(self):
        self.assertTrue(Netflix.estimate_rating(self.user4["id"], self.movie2["id"], self.overall_average, self.user_averages,
                                                self.movie_averages) == Netflix.estimate_rating(self.user2["id"],
                                                                                                self.movie4["id"],
                                                                                                self.overall_average,
                                                                                                self.user_averages,
                                                                                                self.movie_averages))

    def test_estimate_rating_5(self):
        self.assertTrue(Netflix.estimate_rating(self.user5["id"], self.movie1["id"], self.overall_average, self.user_averages,
                                                self.movie_averages) == Netflix.estimate_rating(self.user1["id"],
                                                                                                self.movie5["id"],
                                                                                                self.overall_average,
                                                                                                self.user_averages,
                                                                                                self.movie_averages))


if __name__ == '__main__':
    unittest.main()
