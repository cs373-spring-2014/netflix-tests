#!/user/bin/evn python3

# -----------------------
# Anthony A. Garza 
# csid: agarza
# Netflix.py
# ----------------------

"""
To test the program:
    % python3 TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestNetflix.out
"""

import io
import unittest

from Netflix import netflix_read_line, netflix_print, netflix_print_RMSE, netflix_eval, netflix_solve, getActualRating

# -------------
# TestNetflix.py
# -------------

class TestNetflix (unittest.TestCase) :
    # read
    def test_read_movie(self) :
        r = io.StringIO("1234:\n")
        a = [-1, -1]
        v = netflix_read_line(r, a)
        self.assertTrue(v == True)
        self.assertTrue(a[0] == 1234)
        self.assertTrue(a[1] == 1)
    
    def test_read_member(self) :
        r = io.StringIO("832\n")
        a = [-1, -1]
        v = netflix_read_line(r, a)
        self.assertTrue(v == True)
        self.assertTrue(a[0] == 832)
        self.assertTrue(a[1] == 0)

    def test_read_end(self) :
        r = io.StringIO("")
        a = [-1, -1]
        v = netflix_read_line(r, a)
        self.assertTrue(v == False)
        self.assertTrue(a[0] == -1)
        self.assertTrue(a[1] == -1)

    # print movie id or member rating
    def test_print_movie(self) :
        w = io.StringIO()
        netflix_print (w, 1234, 1)
        self.assertTrue(w.getvalue() == "1234:\n")
    
    def test_print_movie_2(self) :
        w = io.StringIO()
        netflix_print(w, 7832, 1)
        self.assertTrue(w.getvalue() == "7832:\n")
        
    def test_print_rating(self) :
        w = io.StringIO()
        netflix_print(w, 2.3234, 0)
        self.assertTrue(w.getvalue() == "2.3234\n")
    
    def test_print_rating_2(self) :
        w = io.StringIO()
        netflix_print(w, 5, 0)
        self.assertTrue(w.getvalue() == "5\n")
    
    # print RMSE 
    def test_print_RMSE_1(self) :
        w = io.StringIO()
        netflix_print_RMSE(w, 0.2)
        self.assertTrue(w.getvalue() == "RMSE: 0.2")
        
    def test_print_RMSE_2(self) :
        w = io.StringIO()
        netflix_print_RMSE(w, 1.2324)
        self.assertTrue(w.getvalue() == "RMSE: 1.2324")
        
    def test_print_RMSE_3(self) :
        w = io.StringIO()
        netflix_print_RMSE(w, 0.94212)
        self.assertTrue(w.getvalue() == "RMSE: 0.94212")
        
    # eval tests
    def test_eval_1 (self) :
        v = netflix_eval(10006, 1093333)
        self.assertTrue(str(v) == "3.7387541371011226")
    
    def test_eval_2 (self) :
        v = netflix_eval(10001, 262828)
        self.assertTrue(str(v) == "3.421835643014238")

    def test_eval_3 (self) :
        v = netflix_eval(10011, 1624701)
        self.assertTrue(str(v) == "4.430644527467521")    

    # getActualRating tests
    def test_ar_1 (self) :
        v = getActualRating(1, 30878)
        self.assertTrue(v == 4)
        
    def test_ar_2 (self) :
        v = getActualRating(1000, 2326571)
        self.assertTrue(v == 3)
        
    def test_ar_3 (self) :
        v = getActualRating(10001, 262828)
        self.assertTrue(v == 4)

    # solve tests
    def test_solve_1 (self) :
        r = io.StringIO("2043:\n1417435\n2312054\n462685\n10851:\n2157639\n43429\n1726551\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "2043:\n3.6128524745358206\n4.561179210771877\n3.8964414169201587\n10851:\n3.870434740860335\n3.876164183831157\n4.396995784662757\nRMSE: 1.5679729948287326")
        
    def test_solve_empty (self) :
        r = io.StringIO("")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "")
        
    def test_solve_2 (self) :
        r = io.StringIO("10037:\n253214\n612895\n769764\n396150\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10037:\n3.880194904996164\n3.0195416236056936\n3.1972198617542564\n3.729277278022199\nRMSE: 0.762798883176907")

# -------
# main
# -------

print("TestNextflix.py")
unittest.main()
print("done.")
