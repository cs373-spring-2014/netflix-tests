#!/usr/bin/env python

# -------------------------------
# projects/collatz/TestNetflix.py
# Copyright (C) 2013
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestNetflix.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Netflix import *

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # load_cache
    # ----
    def verify_probe(self, data):
        self.assert_( type(data)               == dict )
        self.assert_( type(data["1"])          == dict )
        self.assert_( data["1"]["30878"]       == 4 )
        self.assert_( data["10003"]["1515111"] == 3)


    def test_load_cache_probe(self):
        self.verify_probe(load_cache(PROBE))
        

    def test_load_cache_nonexistent(self):
        self.assertRaises(IOError, load_cache, "/this/is/fake")

    def test_load_cache_malformed(self):
        self.assertRaises(ValueError, load_cache, "/usr/bin/zip")        


    # ----
    # load_caches
    # ----
    def test_load_caches_empty(self):
        data = load_caches([])
        self.assert_( data == {} )

    def test_load_caches_probe(self):
        data = load_caches([PROBE])
        self.assert_( type(data)                      == dict )
        self.verify_probe(data[PROBE])

    def test_load_caches_multi(self):
        data = load_caches([PROBE, USER_AVERAGE])
        self.assert_( type(data)                      == dict )
        self.verify_probe(data[PROBE])

        self.assert_( data[USER_AVERAGE]["2212128"]   == 3.4523809523809526 )
        self.assert_( data[USER_AVERAGE]["2035903"]   == 3.375)


    def test_load_caches_nonexistent(self):
        self.assertRaises(IOError, load_caches, [PROBE, "/this/is/fake"])

    def test_load_caches_malformed(self):
        self.assertRaises(ValueError, load_caches, [USER_AVERAGE, "/usr/bin/zip"])   


    # ----
    # netflix_caches
    # ----
    def test_netflix_caches_filesexist(self):
        netflix_caches()

    def test_netflix_caches_global_avg(self):
        cache = netflix_caches()
        self.assert_( GLOBAL_USER_AVG in cache )
        self.assert_( type(cache[GLOBAL_USER_AVG]) == float ) 
        self.assert_( cache[GLOBAL_USER_AVG] >= 1.0 and cache[GLOBAL_USER_AVG] <= 5.0)

   
    def test_netflix_caches(self):
        data = netflix_caches()
        self.verify_probe(data[PROBE])
        self.assert_( data[USER_AVERAGE ]["2212128"]   == 3.4523809523809526 )
        self.assert_( data[MOVIE_AVERAGE]["7964"]      == 3.6305970149253732 )

    # ----
    # netflix_guess
    # ----

    FAKECACHE = { PROBE : { '1000' : { '9999' : 3.0 } },  MOVIE_AVERAGE:  {'1000' : 3.0}, USER_AVERAGE:   {'9999' : 4.0}, GLOBAL_USER_AVG: 3.0 }

    def test_netflix_guess(self):
        guess = netflix_guess('1000', '9999', self.FAKECACHE)
        self.assert_(type(guess) == float)
        self.assert_(guess == 4.0)

    def test_netflix_guess_imaginaryperson(self):
        self.assertRaises(AssertionError, netflix_guess, '1000', '-1', self.FAKECACHE)

    def test_netflix_guess_imaginarymovie(self):
        self.assertRaises(AssertionError, netflix_guess, '12345', '9999', self.FAKECACHE)

    
    # -----
    # solve
    # -----
    samplein  = "2683:\n202472\n11057:\n412171\n14147:\n90332\n"
    sampleout = "2683:\n3.69273350109\n11057:\n3.07397944077\n14147:\n3.59267582994\nRMSE: 1.29625792045\n"

    def test_netflix_solve(self):
         r = StringIO.StringIO(self.samplein)
         w = StringIO.StringIO()
         netflix_solve(r, w)
         self.assert_(w.getvalue() == self.sampleout)

    def test_netflix_solve_fakecache(self):
         r = StringIO.StringIO("1000:\n9999\n")
         w = StringIO.StringIO()
         netflix_solve(r, w, self.FAKECACHE)
         self.assert_(w.getvalue() == "1000:\n4.0\nRMSE: 1.0\n")

    def test_netflix_solve_malformed(self):
         r = StringIO.StringIO("1000:\nI am not\nFormed:\ncorrectly\n")
         w = StringIO.StringIO()
         self.assertRaises(AssertionError, netflix_solve, r, w, self.FAKECACHE)


# ----
# main
# ----

print "TestNetflix.py"
unittest.main()
print "Done."
