#!/usr/bin/env python3

# -------------------------------
# projects/netflix/Testnetflix.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestNetflix.py >& TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py >& TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_eval, netflix_print, netflix_solve

# -----------
# Testnetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        r = io.StringIO("2043:\n417435\n2312054\n462685\n")
        l = []
        b = netflix_read(r, l)
        self.assertTrue(b == True)
        self.assertTrue(l)
        self.assertTrue(l[0][0] ==  "2043")
        self.assertTrue(l[0][1] == "417435")

    def test_read2 (self) :
        r = io.StringIO("5000:\n4550\n2312054\n462685\n")
        l = []
        b = netflix_read(r, l)
        self.assertTrue(b == True)
        self.assertTrue(l[0][0] ==  "5000")
        self.assertTrue(l[0][1] == "4550")

    def test_read3 (self) :
        r = io.StringIO("14:\n3215\n2312054\n462685\n")
        l = []
        b = netflix_read(r, l)
        self.assertTrue(b == True)
        self.assertTrue(l[0][0] ==  "14")
        self.assertTrue(l[0][1] == "3215")

    # ----
    # eval
    # ----


    def test_eval_1 (self) :
        v = netflix_eval(["1","30878","2647871","1283744"])
        self.assertTrue(v == [3.683160312499557, 3.283075895740512, 3.590526568166152])

    def test_eval_2 (self) :
        v = netflix_eval(["10","1952305","1531863"])
        self.assertTrue(v == [2.890063550906924, 2.635328154724159])

    def test_eval_3 (self) :
        v = netflix_eval(["1000","2326571","977808","1010534"])
        self.assertTrue(v == [3.181432748538012, 2.872089314194578, 2.584210526315789])

    # -----
    # print
    # -----

    def test_print (self) :
        w = io.StringIO()
        netflix_print(w, 1, [3,3,3])
        self.assertTrue(w.getvalue() == "1:\n3\n3\n3\n")

    def test_print2 (self) :
        w = io.StringIO()
        netflix_print(w, 5, [7,5,4])
        self.assertTrue(w.getvalue() == "5:\n7\n5\n4\n")

    def test_print3 (self) :
        w = io.StringIO()
        netflix_print(w, 5676, [4,2,3,1,5])
        self.assertTrue(w.getvalue() == "5676:\n4\n2\n3\n1\n5\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = io.StringIO("1:\n30878\n2647871\n1283744\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.683160312499557\n3.283075895740512\n3.590526568166152\nRMSE: 0.56659485424\n")

    def test_solve2 (self) :
        r = io.StringIO("10:\n1952305\n1531863\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10:\n2.890063550906924\n2.635328154724159\nRMSE: 0.2693246902681606\n")

    def test_solve3 (self) :
        r = io.StringIO("1000:\n2326571\n977808\n1010534\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "1000:\n3.181432748538012\n2.872089314194578\n2.584210526315789\nRMSE: 0.36082356022643625\n")

# ----
# main
# ----

print("Testnetflix.py")
unittest.main()
print("Done.")
