#!/usr/bin/env python3

# -------
# imports
# -------

import io, unittest, json, math

from Netflix import netflix_read, netflix_write, netflix_actual, netflix_prediction, predict, rmse_sum_gen_zip, netflix_solve

# -----------
# TestNetflix
# -----------

# [(movie#, customer#)]
data1 = [('1', '30878'), ('1', '2647871'), ('1', '1283744'), ('1', '2488120'), ('1', '317050')]
predictions1 = [3, 4, 2, 3, 5]
actual1 = [3, 4, 2, 3, 5]

data2 = [('1', '30878'), ('1', '2647871'), ('1', '1283744'), ('10', '1952305'), ('10', '1531863')]
predictions2 = [4, 4, 2, 4, 4]
actual2 = [3, 5, 1, 5, 3]

data3 = [('1', '30878'), ('1', '2647871'), ('10', '1952305'), ('10', '1531863'), ('1000', '977808')]
predictions3 = [1, 2, 3, 4, 5]
actual3 = [5, 4, 3, 2, 1]

# get qualifying data
with open("/u/thunt/cs373-netflix-tests/timsim-probe_ans.json", "r") as f :
	probe_ratings = json.load(f)

# get user average data
with open("/u/thunt/cs373-netflix-tests/irvin-user_avg.json", "r") as f:
	user_ratings = json.load(f)

#	get movie average rating data
with open("/u/thunt/cs373-netflix-tests/irvin-movie_avg.json", "r") as f:
	movie_ratings = json.load(f)

class TestNetflix (unittest.TestCase) :

	# ----
	# read
	# ----

	def test_read_1 (self) :
		r = io.StringIO("1:\n1\n2\n3\n")
		d = netflix_read(r)
		self.assertTrue(d ==  [('1', '1'), ('1', '2'), ('1', '3')])

	def test_read_2 (self) :
		r = io.StringIO("1:\n1\n2\n3\n2:\n1\n2\n3\n")
		d = netflix_read(r)
		self.assertTrue(d ==  [('1', '1'), ('1', '2'), ('1', '3'), ('2', '1'), ('2', '2'), ('2', '3')])

	def test_read_3 (self) :
		r = io.StringIO("1:\n2:\n3:\n1\n2\n3\n")
		d = netflix_read(r)
		self.assertTrue(d ==  [('3', '1'), ('3', '2'), ('3', '3')])

	# -----
	# write
	# -----

	def test_write_1 (self) :
		global data1, predictions1
		w = io.StringIO()
		p = (v for v in predictions1)
		netflix_write(w, data1, p)
		self.assertTrue(w.getvalue() == "1:\n3\n4\n2\n3\n5\n")

	def test_write_2 (self) :
		global data2, predictions2
		w = io.StringIO()
		p = (v for v in predictions2)
		netflix_write(w, data2, p)
		self.assertTrue(w.getvalue() == "1:\n4\n4\n2\n10:\n4\n4\n")

	def test_write_3 (self) :
		global data3, predictions3
		w = io.StringIO()
		p = (v for v in predictions3)
		netflix_write(w, data3, p)
		self.assertTrue(w.getvalue() == "1:\n1\n2\n10:\n3\n4\n1000:\n5\n")

	# ------
	# actual
	# ------

	def test_actual_1 (self) :
		global data1, probe_ratings
		self.assertTrue(list(netflix_actual(data1, probe_ratings)) == [4,4,3,5,5])

	def test_actual_2 (self) :
		global data2, probe_ratings
		self.assertTrue(list(netflix_actual(data2, probe_ratings)) == [4,4,3,3,3])

	def test_actual_3 (self) :
		global data3, probe_ratings
		self.assertTrue(list(netflix_actual(data3, probe_ratings)) == [4,4,3,3,3])
		
	# ----------
	# prediction
	# ----------

	def test_prediction_1 (self) :
		global data1, movie_ratings, user_ratings
		self.assertTrue(list(netflix_prediction(data1, movie_ratings, user_ratings)) == [3.692, 3.492, 3.645, 4.477, 3.697])

	def test_prediction_2 (self) :
		global data2, movie_ratings, user_ratings
		self.assertTrue(list(netflix_prediction(data2, movie_ratings, user_ratings)) == [3.692, 3.492, 3.645, 3.295, 3.168])

	def test_prediction_3 (self) :
		global data3, movie_ratings, user_ratings
		self.assertTrue(list(netflix_prediction(data3, movie_ratings, user_ratings)) == [3.692, 3.492, 3.295, 3.168, 3.286])

	# -------
	# predict
	# -------

	def test_predict_1 (self) :
		self.assertTrue(predict(3.2, 3.6) == 3.4)

	def test_predict_2 (self) :
		self.assertTrue(predict(4.2, 3) == round(.8*4.2 + .2*3, 3))

	def test_predict_3 (self) :
		self.assertTrue(predict(3.2, 1.5) == round(.2*3.2 + .8*1.5, 3))
		
	# ----------------
	# rmse_sum_gen_zip
	# ----------------
	
	def test_rmse_1 (self):
		global predictions1, actual1
		p = predictions1
		a = actual1
		self.assertTrue(rmse_sum_gen_zip(a, p) == 0)
		self.assertTrue(rmse_sum_gen_zip(p, a) == 0)
		
	def test_rmse_2 (self):
		global predictions2, actual2
		p = predictions2
		a = actual2
		self.assertTrue(rmse_sum_gen_zip(a, p) == 1)
		self.assertTrue(rmse_sum_gen_zip(p, a) == 1)
		
	def test_rmse_3 (self):
		global predictions3, actual3
		p = predictions3
		a = actual3
		self.assertTrue(rmse_sum_gen_zip(a, p) == math.sqrt(40 / 5))
		self.assertTrue(rmse_sum_gen_zip(p, a) == math.sqrt(40 / 5))
	
	
	
	# -----
	# solve
	# -----
	
	def test_solve_1 (self):
		w = io.StringIO()
		r = io.StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n")
		netflix_solve(r, w)
		self.assertTrue(w.getvalue() == "1:\n3.692\n3.492\n3.645\n4.477\n3.697\nRMSE: 0.7403095298589637\n")
	
	def test_solve_2 (self):
		w = io.StringIO()
		r = io.StringIO("1:\n30878\n2647871\n1283744\n10:\n1952305\n1531863\n")
		netflix_solve(r, w)
		self.assertTrue(w.getvalue() == "1:\n3.692\n3.492\n3.645\n10:\n3.295\n3.168\nRMSE: 0.4205239588893836\n")
	
	def test_solve_3 (self):
		w = io.StringIO()
		r = io.StringIO("1:\n30878\n2647871\n10:\n1952305\n1531863\n1000:\n977808\n")
		netflix_solve(r, w)
		self.assertTrue(w.getvalue() == "1:\n3.692\n3.492\n10:\n3.295\n3.168\n1000:\n3.286\nRMSE: 0.331654338129324\n")
	

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
