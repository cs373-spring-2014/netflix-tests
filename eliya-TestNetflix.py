#!/usr/bin/env python3

# -------
# imports
# -------
import io
import unittest

from Netflix import netflix_solve, netflix_read, read_caches, netflix_eval, netflix_print, rmse, customers_per_movie, \
    ratings, movie_order, cust_avgs, movie_avgs, actual_ratings, rmseVal, customer_decade_avg, movie_years
import Netflix

# -----------
# TestCollatz
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1:\n2647871\n30878\n")
        netflix_read(r)
        custms = customers_per_movie[1]
        self.assertTrue(custms[0] ==  2647871)
        self.assertTrue(custms[1] == 30878)
    
    def test_read_2 (self) :
        r = io.StringIO("1:\n2647871\n30878\n")
        Netflix.movie_order = []
        netflix_read(r)
        self.assertTrue(movie_order[0] ==  1)
    
    def test_read_3 (self) :
        r = io.StringIO("1000:\n2326571\n977808\n1010534\n")
        customers_per_movie.clear()
        Netflix.movie_order = []
        netflix_read(r)
        custms = customers_per_movie[1000]
        self.assertTrue(custms[0] ==  2326571)
        self.assertTrue(custms[1] == 977808)
    
    def test_read_4 (self) :
        r = io.StringIO("1:\n2647871\n30878\n1000:\n2326571\n977808\n1010534\n")
        Netflix.movie_order = []
        netflix_read(r)
        self.assertTrue(movie_order[0] ==  1)
        self.assertTrue(movie_order[1] == 1000)
    
    # -----
    # eval
    # -----
    def  test_eval_1 (self) :
        r = io.StringIO("1:\n2647871\n30878\n1000:\n2326571\n977808\n1010534\n")
        w = io.StringIO()
        customers_per_movie.clear()
        movie_order = []
        netflix_read(r)
        Netflix.movie_avgs = {1: 3.2412, 1000: 4.1413}
        Netflix.cust_avgs = {2647871: 3.532, 30878: 4.0, 2326571: 2.4432, 977808: 5.0, 1010534: 2.345}
        Netflix.actual_ratings = {1: [2.0, 3.0], 1000: [3.0, 1.5, 5.0]}
        Netflix.overallAvg = 3.67
        netflix_eval()
        self.assertTrue(str(Netflix.rmseVal) == "2.2042359237776283")

    def  test_eval_2 (self) :
        r = io.StringIO("4:\n343\n242\n6:\n450\n320\n120\n")
        w = io.StringIO()
        customers_per_movie.clear()
        Netflix.movie_order = []
        netflix_read(r)
        Netflix.movie_avgs = {4: 3.4534534, 6: 3.12035}
        Netflix.cust_avgs = {242: 2.45456, 343: 1.4564, 320: 2.4432, 120: 4.464, 450: 3.46464}
        Netflix.actual_ratings = {4: [2.0, 3.0], 6: [3.0, 3.0, 5.0]}
        Netflix.overallAvg = 3.67
        netflix_eval()
        self.assertTrue(str(Netflix.rmseVal) == "0.7972591155739965")

    def  test_eval_3 (self) :
        r = io.StringIO("10:\n33\n12\n13\n735\n25:\n10\n5\n1\n")
        w = io.StringIO()
        customers_per_movie.clear()
        Netflix.movie_order = []
        netflix_read(r)
        Netflix.movie_avgs = {10: 4.56565, 25: 3.894533}
        Netflix.cust_avgs = {1: 3.564, 735: 3.585413, 12: 4.2125, 5: 1.548658, 33: 3.1545, 13: 3.4646, 10: 2.54564}
        Netflix.actual_ratings = {25: [3.0, 1.0, 4.0], 10: [3.0, 4.0, 3.0, 4.0]}
        Netflix.overallAvg = 3.67
        netflix_eval()
        self.assertTrue(str(Netflix.rmseVal) == "0.8343019799577607")

    def  test_eval_4 (self) :
        r = io.StringIO("2:\n2455\n1:\n124\n")
        w = io.StringIO()
        customers_per_movie.clear()
        Netflix.movie_order = []
        netflix_read(r)
        Netflix.movie_avgs = {2: 4.654654, 1: 4.12121}
        Netflix.cust_avgs = {2455: 3.565623, 124: 4.44578}
        Netflix.actual_ratings = {2: [4.0], 1: [5.0]}
        Netflix.overallAvg = 3.67
        netflix_eval()
        self.assertTrue(str(Netflix.rmseVal) == "0.3693420955784545")

    # ----
    # RMSE
    # ----
    def test_rmse_1(self):
        ratings = {1: [3.44, 2.44], 1000: [2.111, 3.53, 4.1]}
        actual_ratings = {1: [3, 3], 1000: [4, 2, 5]}
        Netflix.movie_order = [1, 1000]
        rmse_val = rmse(actual_ratings, ratings)
        self.assertTrue(str(rmse_val) == "1.202199733821298")

    def test_rmse_2(self):
        ratings = {1: [4.11, 2.3], 10001: [4.3532]}
        actual_ratings = {1: [4, 3], 10001: [4]}
        Netflix.movie_order = [1, 10001]
        rmse_val = rmse(actual_ratings, ratings)
        self.assertTrue(str(rmse_val) == "0.45711057743176337")

    def test_rmse_3(self):
        ratings = {10005: [4.11, 2.3], 10: [4.241, 5.0]}
        actual_ratings = {10005: [4, 3], 10: [4, 5]}
        Netflix.movie_order = [10005, 10]
        rmse_val = rmse(actual_ratings, ratings)
        self.assertTrue(str(rmse_val) == "0.3742262016481476")

    def test_rmse_4(self):
        ratings = {10006: [4.11, 2.3], 10001: [2.343, 4.1]}
        actual_ratings = {10006: [5, 1], 10001: [2, 5]}
        Netflix.movie_order = [10006, 10001]
        rmse_val = rmse(actual_ratings, ratings)
        self.assertTrue(str(rmse_val) == "0.9232752839754782")

    def test_rmse_5(self):
        ratings = {6: [1.435, 2.55], 5: [2.421, 3.72]}
        actual_ratings = {6: [2, 3], 5: [4, 2]}
        Netflix.movie_order = [6, 5]
        rmse_val = rmse(actual_ratings, ratings)
        self.assertTrue(str(rmse_val) == "1.2220235267784334")

    def test_rmse_6(self):
        ratings = {503: [4.2, 4.8], 101: [1.3, 3.5]}
        actual_ratings = {503: [4, 3], 101: [1, 2]}
        Netflix.movie_order = [503, 101]
        rmse_val = rmse(actual_ratings, ratings)
        self.assertTrue(str(rmse_val) == "1.1853269591129698")

    # -----
    # print
    # -----
    def test_print_1(self):
        w = io.StringIO()
        Netflix.movie_order = [503, 101]
        Netflix.ratings = {503: [4.2, 4.8], 101: [1.3, 3.5]}
        Netflix.rmseVal = 1.202199733821298
        netflix_print(w)
        self.assertTrue(w.getvalue() == "503:\n4.2\n4.8\n101:\n1.3\n3.5\nRMSE: 1.202199733821298")

    def test_print_2(self):
        w = io.StringIO()
        Netflix.movie_order = [1, 10001]
        Netflix.ratings = {1: [4.11, 2.3], 10001: [4.3532]}
        Netflix.rmseVal = 0.45711057743176337
        netflix_print(w)
        self.assertTrue(w.getvalue() == "1:\n4.11\n2.3\n10001:\n4.3532\nRMSE: 0.45711057743176337")

    def test_print_3(self):
        w = io.StringIO()
        Netflix.movie_order = [10005, 10]
        Netflix.ratings = {10005: [4.11, 2.3], 10: [4.241, 5.0]}
        Netflix.rmseVal = 0.3742262016481476
        netflix_print(w)
        self.assertTrue(w.getvalue() == "10005:\n4.11\n2.3\n10:\n4.241\n5.0\nRMSE: 0.3742262016481476")

    def test_print_4(self):
        w = io.StringIO()
        Netflix.movie_order = [10006, 10001]
        Netflix.ratings = {10006: [4.14231, 2.335231], 10001: [1.343, 3.124121]}
        Netflix.rmseVal = 0.939471394
        netflix_print(w)
        self.assertTrue(w.getvalue() == "10006:\n4.14231\n2.335231\n10001:\n1.343\n3.124121\nRMSE: 0.939471394")

    def test_print_5(self):
        w = io.StringIO()
        Netflix.movie_order = [241, 34]
        Netflix.ratings = {34: [3.1424654731, 2.7351], 241: [2.3432, 4.1246]}
        Netflix.rmseVal = 0.93214
        netflix_print(w)
        self.assertTrue(w.getvalue() == "241:\n2.3432\n4.1246\n34:\n3.1424654731\n2.7351\nRMSE: 0.93214")

# ----
# main
# ----
print("TestNetflix.py")
unittest.main()
print("Done.")
