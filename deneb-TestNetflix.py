#!/usr/bin/env python3

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_eval, netflix_cache_generic, netflix_cache_actual, netflix_eval

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :

  # ----
  # read
  # ----
  
  def test_read_1 (self) :
    l = list(netflix_read(io.StringIO("1234:\n12\n23\n34\n")))
    self.assertTrue(len(l) == 4)
    self.assertTrue(l[0] == "1234:")
    self.assertTrue(l[1] == "12")
    self.assertTrue(l[2] == "23")
    self.assertTrue(l[3] == "34")

  def test_read_2 (self) :
    l = list(netflix_read(io.StringIO("525:\n55\n22\n33\n44\n")))
    self.assertTrue(len(l) == 5)
    self.assertTrue(l[0] == "525:")
    self.assertTrue(l[1] == "55")
    self.assertTrue(l[2] == "22")
    self.assertTrue(l[3] == "33")
    self.assertTrue(l[4] == "44")

  def test_read_3 (self) :
    l = list(netflix_read(io.StringIO("999999:\n1\n2\n")))
    self.assertTrue(len(l) == 3)
    self.assertTrue(l[0] == "999999:")
    self.assertTrue(l[1] == "1")
    self.assertTrue(l[2] == "2")

  # -------------
  # cache_generic
  # -------------

  def test_cache_generic_1 (self) : 
    d = netflix_cache_generic('ericweb2-custAveragesOneLine.txt')  
    self.assertTrue(d[6] == 3.4185303514377)
    self.assertTrue(d[33] == 3.75)
    self.assertTrue(d[3469] == 3.16)

  def test_cache_generic_2 (self) : 
    d = netflix_cache_generic('ericweb2-movieAveragesOneLine.txt')  
    self.assertTrue(d[1] == 3.749542961608775)
    self.assertTrue(d[9992] == 2.43)
    self.assertTrue(d[17770] == 2.816503800217155)

  def test_cache_generic_3 (self) : 
    d = netflix_cache_generic('ericweb2-movieAveragesOneLine.txt')  
    self.assertTrue(d[5000] == 3.1289237668161434)
    self.assertTrue(d[1] == 3.749542961608775)
    self.assertTrue(d[10000] == 2.902325581395349)

  # ------------
  # cache_actual
  # ------------

  def test_cache_actual_1 (self) : 
    d = netflix_cache_actual()
    self.assertTrue(len(d) == 16938) 

  def test_cache_actual_2 (self) : 
    d = netflix_cache_actual()
    self.assertTrue(d["1"]["30878"] == 4) 

  def test_cache_actual_3 (self) : 
    d = netflix_cache_actual()
    self.assertTrue(d["1000"]["977808"] == 3) 

  # ----
  # eval
  # ----

  def test_eval_1 (self) : 
    d = netflix_eval(["1:", "30878", "14756", "548064"])
    self.assertTrue(len(d) == 5)
    self.assertTrue(d[4] == "RMSE: 0.71")

  def test_eval_2 (self) : 
    d = netflix_eval(["10:", "1531863"])
    self.assertTrue(len(d) == 3)
    self.assertTrue(d[2] == "RMSE: 0.12")

  def test_eval_3 (self) : 
    d = netflix_eval(["10011:", "1624701", "2646826"])
    self.assertTrue(len(d) == 4)
    self.assertTrue(d[3] == "RMSE: 0.51")


# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")

