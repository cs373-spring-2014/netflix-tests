# -------
# imports
# -------

import io
import unittest
import math


from Netflix import netflix_read, netflix_rmse, netflix_solve, netflix_parse, netflix_avg

class TestNetflix (unittest.TestCase) :

	# ----
	# read
	# ----

	def test_read_0 (self) :

		# ----
		# read_helper
		# ----
		def read_helper (m) :
			n = str(list(next(m)))
			if str(n[len(n)-3:len(n)-2:1]) == ":" :
				n = n[2:len(n)-3:1]
			else :
				n = n[2:len(n)-2:1]
			return n

		r = io.StringIO("2043:\n235213\n234262\n3045:\n23523\n13234")

		m = netflix_read(r)
		self.assertTrue(read_helper(m) == "2043")

		m = netflix_read(r)
		self.assertTrue(read_helper(m) == "235213")

		m = netflix_read(r)
		self.assertTrue(read_helper(m) == "234262")

		m = netflix_read(r)
		self.assertTrue(read_helper(m) == "3045")

		m = netflix_read(r)
		self.assertTrue(read_helper(m) == "23523")

		m = netflix_read(r)
		self.assertTrue(read_helper(m) == "13234")


	# ----
	# rmse
	# ----

	def test_rmse_0 (self) :
		l1 = [2, 3, 4]
		l2 = [2, 3, 4]
		self.assertTrue(netflix_rmse(l1, l2) == 0)

	def test_rmse_1 (self) :
		l1 = [2, 3, 4]
		l2 = [3, 4, 5]
		self.assertTrue(netflix_rmse(l1, l2) == 1)

	def test_rmse_reverse (self) :
		l1 = [3, 4, 5]
		l2 = [2, 3, 4]
		self.assertTrue(netflix_rmse(l1, l2) == 1)

	def test_rmse_case (self) :
		l1 = [10, 7, 9, 8]
		l2 = [8, 5, 7, 6]
		self.assertTrue(netflix_rmse(l1, l2) == 2)

	def test_rmse_neg (self) :
		l1 = [-5, -5, -5]
		l2 = [-6, -6, -6]
		self.assertTrue(netflix_rmse(l1, l2) == 1)

	#
	# avg
	#

	def test_avg_outlying_customer (self) :
		l1 = [5]
		l2 = [4]
		self.assertTrue(netflix_avg(l1, l2) == [5])

	def test_avg_outlying_movie (self) :
		l1 = [4]
		l2 = [5]
		self.assertTrue(netflix_avg(l1, l2) == [5])

	def test_avg_0 (self) :
		l1 = [3, 2, 4]
		l2 = [2, 3, 2]
		self.assertTrue(netflix_avg(l1, l2) == [2.5, 2.5, 3])

	def test_avg_conflicting_outliers (self) :
		l1 = [5, 4.6, 1.5]
		l2 = [4.5, 4.5, 1]
		self.assertTrue(netflix_avg(l1, l2) == [5, 4.6, 1.5])

	def test_avg_same_numbers (self) :
		l1 = [2, 2, 2]
		l2 = [2, 2, 2]
		self.assertTrue(netflix_avg(l1, l2) == [2, 2, 2])

	#
	# parse
	#

	def test_parse_0 (self) :
		r = io.StringIO("1: 4\n2: 5\n3: 2\n4: 2\n5: 1")
		d = netflix_parse(r)
		self.assertTrue(d["1"] == "4")
		self.assertTrue(d["2"] == "5")
		self.assertTrue(d["3"] == "2")
		self.assertTrue(d["4"] == "2")
		self.assertTrue(d["5"] == "1")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")