import io
import unittest
from Netflix import *

class TestNetflix(unittest.TestCase):
	
	#-------------
	# netflix_read
	#-------------

	def test_netflix_read_1(self):
		r = io.StringIO("2043:\n1417435\n2312054\n462685\n10851:\n1417435\n2312054\n462685\n")
		a = []
		for line in netflix_read(r):
			a.append(line)
		self.assertEqual(len(a), 8)
		self.assertEqual(a[0], "2043:")
		self.assertEqual(a[1], "1417435")
		self.assertEqual(a[7], "462685")

	def test_netflix_read_2(self):
		r = io.StringIO("\n")
		a = []
		for line in netflix_read(r):
			a.append(line)
		self.assertEqual(len(a), 1)
		self.assertEqual(a[0], "")

	def test_netflix_read_3(self):
		r = io.StringIO("")
		a = []
		for line in netflix_read(r):
			a.append(line)
		self.assertEqual(len(a), 0)

	def test_netflix_loadcache_1(self):
		r = io.StringIO("1: 3.749542961608775\n2: 3.5586206896551724\n3: 3.6411530815109345")
		cache = netflix_loadcache(None, r)
		self.assertEqual(cache[1], 3.749542961608775)
		self.assertEqual(cache[2], 3.5586206896551724)
		self.assertEqual(cache[3], 3.6411530815109345)

	def test_netflix_loadcache_2(self):
		r = "/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt"
		cache = netflix_loadcache(r)
		self.assertEqual(cache[17770], 2.816503800217155)
		self.assertEqual(len(cache), 17770)

	def test_netflix_loadcache_3(self):
		r = "/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt"
		cache = netflix_loadcache(r)
		self.assertEqual(len(cache), 480189)

	def test_realcache_1(self):
		# r = "/u/thunt/cs373-netflix-tests/word-probeMapCache.txt"
		r = io.StringIO("1:\n30878-4\n2647871-4\n1283744-3\n2488120-5\n317050-5\n1904905-4\n1989766-4\n14756-4\n1027056-3\n1149588-4\n1394012-5\n1406595-4\n2529547-5\n1682104-4\n2625019-3\n2603381-5\n1774623-4\n470861-5\n712610-4\n1772839-5\n1059319-3\n2380848-5\n548064-5\n")
		cache = netflix_realcache(None, r)
		self.assertEqual(cache[1][30878], 4)
		self.assertEqual(cache[1][2647871], 4)

	def test_realcache_2(self):
		# r = "/u/thunt/cs373-netflix-tests/word-probeMapCache.txt"
		r = io.StringIO("10:\n1952305-3\n1531863-3\n")
		cache = netflix_realcache(None, r)
		self.assertEqual(cache[10][1952305], 3)
		self.assertEqual(cache[10][1531863], 3)

	def test_realcache_3(self):
		# r = "/u/thunt/cs373-netflix-tests/word-probeMapCache.txt"
		r = io.StringIO("10000:\n200206-5\n523108-4\n")
		cache = netflix_realcache(None, r)
		self.assertEqual(cache[10000][200206], 5)
		self.assertEqual(cache[10000][523108], 4)

	def test_ARPCPD_1(self):
		cache = netflix_ARPCPD("/u/thunt/cs373-netflix-tests/jkelle-avgByCustomeridByDecadeOneLine.txt","/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
		self.assertEqual(cache[2431849][1920], 4.625)

	def test_ARPCPD_2(self):
		cache = netflix_ARPCPD("/u/thunt/cs373-netflix-tests/jkelle-avgByCustomeridByDecadeOneLine.txt","/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
		self.assertEqual(cache[2281698][1990], 3.6666666666666665)

	def test_ARPCPD_3(self):
		cache = netflix_ARPCPD("/u/thunt/cs373-netflix-tests/jkelle-avgByCustomeridByDecadeOneLine.txt","/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
		self.assertEqual(cache[1850634][1990], 2.5853658536585367)

	# safe to set up caches now
	M_CACHE = netflix_loadcache("/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt")
	C_CACHE = netflix_loadcache("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
	R_CACHE = netflix_realcache("/u/thunt/cs373-netflix-tests/word-probeMapCache.txt")
	S_CACHE = netflix_loadcache("/u/thunt/cs373-netflix-tests/ericweb2-numRatingsOneLine.txt")
	Y_CACHE = netflix_ARPCPD("/u/thunt/cs373-netflix-tests/jkelle-avgByCustomeridByDecadeOneLine.txt","/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
	A_CACHE = netflix_loadcache("/u/thunt/cs373-netflix-tests/verstarr-movie_year.txt")

	def test_netflix_eval_1(self):
		movieID = 2043
		custs = [1417435,2312054,462685,1428041]
		s = netflix_eval(movieID, custs, self.C_CACHE, self.M_CACHE, self.S_CACHE, self.Y_CACHE, self.A_CACHE)
		self.assertEqual(s[0], 3.9)
		self.assertEqual(s[1], 3.7)
		self.assertEqual(len(s), 4)

	def test_netflix_eval_2(self):
		movieID = 10002
		custs = [1450941,1213181,308502,2581993]
		s = netflix_eval(movieID, custs, self.C_CACHE, self.M_CACHE, self.S_CACHE, self.Y_CACHE, self.A_CACHE)
		self.assertEqual(s, [4.5, 4.0, 4.7, 4.5])
		self.assertEqual(len(s), 4)

	def test_netflix_eval_3(self):
		movieID = 10006
		custs = [1093333,1982605,1534853,1632583]
		s = netflix_eval(movieID, custs, self.C_CACHE, self.M_CACHE, self.S_CACHE, self.Y_CACHE, self.A_CACHE)
		self.assertEqual(s, [4.3, 4.2, 4.5, 3.8])
		self.assertEqual(len(s), 4)

	def test_write_1(self):
		w = io.StringIO()
		netflix_write(w, '1', [2,2,2])
		self.assertEqual(w.getvalue(), "1:\n2\n2\n2\n")

	def test_write_2(self):
		w = io.StringIO()
		netflix_write(w, '345', [5,4])
		self.assertEqual(w.getvalue(), "345:\n5\n4\n")

	def test_write_3(self):
		w = io.StringIO()
		netflix_write(w, '9', [])
		self.assertEqual(w.getvalue(), "9:\n")

	def test_rmse_1(self):
		a = [1,1]
		b = [1,1]
		s = rmse(a,b)
		self.assertEqual(s, 0)

	def test_rmse_2(self):
		a = [1,1]
		b = [2,2]
		s = rmse(a,b)
		self.assertEqual(s, 1)

	def test_rmse_3(self):
		a = [1,2]
		b = [2,1]
		s = rmse(a,b)
		self.assertEqual(s, 1)

	def test_netflix_solve_1(self):
		r = io.StringIO("2043:\n1417435\n2312054\n462685\n10851:\n1050707\n1359601\n2477968\n")
		w = io.StringIO()
		netflix_solve(r,w)
		self.assertEqual(w.getvalue(), "2043:\n3.9\n3.7\n3.9\n10851:\n3.6\n4.2\n3.9\nRMSE: 1.27")

	def test_netflix_solve_2(self):
		r = io.StringIO("10005:\n254775\n1892654\n469365\n793736\n926698\n10006:\n1093333\n1982605\n1534853\n1632583\n")
		w = io.StringIO()
		netflix_solve(r,w)
		self.assertEqual(w.getvalue(), "10005:\n3.7\n4.2\n3.5\n3.4\n3.3\n10006:\n4.3\n4.2\n4.5\n3.8\nRMSE: 1.18")

	def test_netflix_solve_3(self):
		r = io.StringIO("10005:\n254775\n")
		w = io.StringIO()
		netflix_solve(r,w)
		self.assertEqual(w.getvalue(), "10005:\n3.7\nRMSE: 2.7")

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")