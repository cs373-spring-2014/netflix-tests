#!/usr/bin/env python3

"""
To test the program:
    % python TestNetflix.py >& TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py >& TestNetflix.out
"""

# -------
# imports
# -------

import io, unittest

from Netflix import prob_read, prob_eval, get_decade, get_RMSE, fill_testing_list, load_data, load_decade_cache, netflix_solve, next_movie, read_done, my_results, probe_list, customer_ids, current_movie, first_movie, last_movie
 

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # probe_read   
    # ----

    def test_prob_read1(self) :
        r = io.StringIO("30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n10:\n")
        prob_read(r, customer_ids)
        self.assertEqual(len(customer_ids), 23)

    def test_prob_read2(self) :
        r = io.StringIO("2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n10:\n")
        prob_read(r, customer_ids)
        self.assertEqual(len(customer_ids), 31)

    def test_prob_read3(self) :
        r = io.StringIO("30878\n2647871\n1283744\n2488120\n3:\n")
        prob_read(r, customer_ids)
        self.assertEqual(customer_ids[34], '2488120')
       
    # ----
    # load_data
    # ----

    def test_load_data1(self) :
        cache_movie_year = {}
        r = io.StringIO("1: 2003\n2: 2004\n3: 1997\n4: 1994\n5: 2004\n6: 1997\n7: 1992\n")
        load_data(r, cache_movie_year)
        self.assertEqual(cache_movie_year[7], 1992)

    def test_load_data2(self) :
        cache_movie_year = {}
        r = io.StringIO("8: 2004\n9: 1991\n10: 2001\n11: 1999\n12: 1913\n13: 2003\n14: 1982\n15: 1988\n")
        load_data(r, cache_movie_year)
        self.assertEqual(cache_movie_year[8], 2004)

    def test_load_data3(self) :
        cache_movie_year = {}
        r = io.StringIO("16: 1996\n17: 2005\n18: 1994\n19: 2000\n20: 1972\n21: 2002\n")
        load_data(r, cache_movie_year)
        self.assertEqual(cache_movie_year[18], 1994)

    # ----
    # get_decade
    # ----

    def test_get_decade(self) :
        r = io.StringIO("1283744 2488120 317050\n1904905 1989766 14756\n1027056 1149588 987\n")
        cache_decade = {}
        current_movie = 1
        cache_decade[1] = 2003
        get_decade(cache_decade)
        self.assertEqual(cache_decade[1], 2003)

    def test_get_decade1(self) :
        r = io.StringIO("1283744 2488120 317050\n1904905 1989766 14756\n1027056 1149588 987\n")
        cache_decade = {}
        current_movie = 1
        cache_decade[1] = 1950
        get_decade(cache_decade)
        self.assertEqual(cache_decade[1], 1950)

    def test_get_decade2(self) :
        r = io.StringIO("1283744 2488120 317050\n1904905 1989766 14756\n1027056 1149588 987\n")
        cache_decade = {}
        current_movie = 1
        cache_decade[1] = 1999
        get_decade(cache_decade)
        self.assertEqual(cache_decade[1], 1999)

    def test_get_decade3(self) :
        r = io.StringIO("1283744 2488120 317050\n1904905 1989766 14756\n1027056 1149588 987\n")
        cache_decade = {}
        current_movie = 1
        cache_decade[1] = 1991
        get_decade(cache_decade)
        self.assertEqual(cache_decade[1], 1991)

    def test_get_decade4(self) :
        r = io.StringIO("1283744 2488120 317050\n1904905 1989766 14756\n1027056 1149588 987\n")
        cache_decade = {}
        current_movie = 1
        cache_decade[1] = 2002
        get_decade(cache_decade)
        self.assertEqual(cache_decade[1], 2002)

    def test_get_decade5(self) :
        r = io.StringIO("1283744 2488120 317050\n1904905 1989766 14756\n1027056 1149588 987\n")
        cache_decade = {}
        current_movie = 1
        cache_decade[1] = 1997
        get_decade(cache_decade)
        self.assertEqual(cache_decade[1], 1997)

    # ----
    # load decade cache
    # ----

    def test_load_decade_cache1(self) :

        r = io.StringIO("1952305 2000 3.3896103896103895\n")
        cache_decade = {}
        load_decade_cache(r, cache_decade)

        self.assertEqual(cache_decade[1952305][2000], 3.3896103896103895)

    def test_load_decade_cache2(self) :
        r = io.StringIO("236501 1980 4.5\n")
        cache_decade = {}
        load_decade_cache(r, cache_decade)

        self.assertEqual(cache_decade[236501][1980], 4.5)

    def test_load_decade_cache3(self) :
        r = io.StringIO("3003900 1990 5\n")
        cache_decade = {}
        load_decade_cache(r, cache_decade)

        self.assertEqual(cache_decade[3003900][1990], 5)
    
    #
    # fill_testing_list
    #
     
    def test_fill_testing_list(self) :
        s = "10:\n3\n3\n"
        r = io.StringIO(s)
        fill_testing_list(r)
        self.assertEqual(s[0], "1")


    def test_fill_testing_list2 (self) :
        s = "10000:\n5\n4\n"
        r = io.StringIO(s)
        fill_testing_list(r)
        self.assertEqual(s[1], "0")

    def test_fill_testing_list3 (self) :
        s = "10003:\n3\n"
        r = io.StringIO(s)
        fill_testing_list(r)
        self.assertEqual(s[2], "0")

    #
    # get_RMSE
    #

    def test_get_RMSE(self) :
        a = [2.4, 4.1, 1.87, 3.15]
        b = [4.4, 1.1, 3.87, 4.15]
        get_RMSE()
        self.assertEqual(a[2], 1.87)

    def test_get_RMSE2 (self) :
        a = [3.4, 1.1, 5.0, 2.15]
        b = [2.4, 4.1, 2.87, 1.15]
        get_RMSE()
        self.assertEqual(b[3], 1.15)

    def test_get_RMSE3 (self) :
        a = [4.4, 3.1, 2.87, 1.15]
        b = [1.4, 3.1, 4.87, 3.15]
        get_RMSE()
        self.assertEqual(a[1], 3.1)

    def test_get_RMSE4(self) :
        a = [1,5,3,4]
        b = [5,1,4,2]
        get_RMSE()
        self.assertEqual(a[2], 3)

    def test_get_RMSE5 (self) :
        a = [5,1,4,2]
        b = [1,5,3,4]
        get_RMSE()
        self.assertEqual(a[3], 2)

    def test_get_RMSE6 (self) :
        a = [5,1,4,2]
        b = [1,5,3,4]
        get_RMSE()
        self.assertEqual(a[0], 5)


print ("TestNetflix.py")
unittest.main()
print ("Done.")