#!/usr/bin/env python3

from Netflix import read_input, netflix_solve, netflix_eval, get_cust_bias, get_movie_bias
import unittest
from io import StringIO

class TestNetflix (unittest.TestCase) :

    def test_read_input_1(self) :
        r = StringIO("30878")
        w = StringIO()
        ret = read_input(r, w, "10")
        print(w.getvalue())
        self.assertTrue(ret == ("30878", "10"))

    def test_read_input_2(self) :
        r = StringIO("1:\n30878")
        w = StringIO()
        ret = read_input(r, w, "-1")
        self.assertTrue(ret == ("30878", "1"))

    def test_read_input_3(self) :
        r = StringIO("2:\n3:\n500")
        w = StringIO()
        ret = read_input(r, w, "10")
        self.assertTrue(ret == ("500", "3"))

    def test_netflix_solve_1(self) :
        r1 = StringIO("7964:\n317492")
        w = StringIO()
        netflix_solve(r1, w)
        self.assertTrue(w.getvalue() == "7964:\n3.6125481148435012\n")

    def test_netflix_solve_2(self) :
        r1 = StringIO("7964:\n317492\n2043:\n1698418")
        w = StringIO()
        netflix_solve(r1, w)
        self.assertTrue(w.getvalue() == "7964:\n3.6125481148435012\n2043:\n3.7671461079886055\n")

    def test_netflix_solve_3(self) :
        r1 = StringIO("7964:\n2043:\n1698418")
        w = StringIO()
        netflix_solve(r1, w)
        self.assertTrue(w.getvalue() == "7964:\n2043:\n3.7671461079886055\n")
        
    def test_netflix_eval_1(self) :
        self.assertTrue(netflix_eval("2212128", "7964") > 0)
        
    def test_netflix_eval_2(self) :
        self.assertTrue(netflix_eval("2212128", "7964") <= 5)
    
    def test_netflix_eval_3(self) :
        self.assertTrue(str(netflix_eval("2212128", "7964")) == str(3.453754791114117))

    def test_get_cust_bias_1(self) :
        self.assertTrue(get_cust_bias("2212128") > -4)

    def test_get_cust_bias_2(self) :
        self.assertTrue(get_cust_bias("2212128") < 4)

    def test_get_cust_bias_3(self) :
        self.assertTrue(str(get_cust_bias("1698418")) == str(3.619047619047619))

    def test_get_movie_bias_1(self) :
        self.assertTrue(get_movie_bias("7964") > -4)

    def test_get_movie_bias_2(self) :
        self.assertTrue(get_movie_bias("7964") < 4)

    def test_get_movie_bias_3(self) :
        self.assertTrue(str(get_movie_bias("7964")) == str(3.6305970149253732))

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print ("Done.")
