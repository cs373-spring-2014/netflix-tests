#Eric Carrillo
#Eric Wehrmeister

# -------
# imports
# -------

import io
import unittest

from Netflix import *
																	
# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :

    # -----
    # read
    # -----

    def test_read_1 (self) :
        r = io.StringIO("115\n")
        a = [-1, False]
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == 115)
        self.assertTrue(a[1] == False)

    def test_read_2 (self) :
        r = io.StringIO("1:\n")
        a = [-1, False]
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == 1) 
        self.assertTrue(a[1] == True)

    def test_read_3 (self) :
        r = io.StringIO("24112342:\n")
        a = [-1, False]
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == 24112342) 
        self.assertTrue(a[1] == True)

    # ----------
    # print_movie
    # ----------

    def test_netflix_print_movie_1 (self):
        w = io.StringIO()
        netflix_print_movie(w, 2)
        self.assertTrue(w.getvalue() == "2:\n")

    def test_netflix_print_movie_2 (self):
        w = io.StringIO()
        netflix_print_movie(w, 363636)
        self.assertTrue(w.getvalue() == "363636:\n")

    def test_netflix_print_movie_3 (self):
        w = io.StringIO()
        netflix_print_movie(w, 52563)
        self.assertTrue(w.getvalue() == "52563:\n")

    # ------------
    # print_rating
    # ------------

    def test_netflix_print_rating_1 (self):
        w = io.StringIO()
        netflix_print_rating(w, 3.14159265)
        self.assertTrue(w.getvalue() == "3.1\n")

    def test_netflix_print_rating_2 (self):
        w = io.StringIO()
        netflix_print_rating(w, 3.9999999999)
        self.assertTrue(w.getvalue() == "4.0\n")

    def test_netflix_print_rating_3 (self):
        w = io.StringIO()
        netflix_print_rating(w, 5)
        self.assertTrue(w.getvalue() == "5.0\n")

    # ----------
    # print_rmse
    # ----------

    def test_netflix_print_rmse_1 (self):
        w = io.StringIO()
        netflix_print_rmse(w, 0.12)
        self.assertTrue(w.getvalue() == "RMSE: 0.12\n")

    def test_netflix_print_rmse_2 (self):
        w = io.StringIO()
        netflix_print_rmse(w, 0.94)
        self.assertTrue(w.getvalue() == "RMSE: 0.94\n")

    def test_netflix_print_rmse_3 (self):
        w = io.StringIO()
        netflix_print_rmse(w, 1.0)
        self.assertTrue(w.getvalue() == "RMSE: 1.0\n")

    # ----
    # rmse
    # ----

    def test_rmse_1 (self):
        self.assertTrue(str(rmse((2, 3, 4), (2, 3, 4))) == "0.0")

    def test_rmse_2 (self):
        self.assertTrue(str(rmse((2, 3, 4), (3, 4, 5))) == "1.0")

    def test_rmse_3 (self):
        self.assertTrue(str(rmse((2, 3, 4), (4, 3, 2))) == "1.632993161855452")
    
    # ----
    # eval
    # ----
    
    def test_eval_1 (self):
        u = 30878
        m = 1
        r = netflix_eval(u,m)
        self.assertTrue(r <= 5.0 and r >=1.0)
    def test_eval_2 (self):
        u = 1262283
        m = 15084
        r = netflix_eval(u,m)
        self.assertTrue(r <= 5.0 and r >=1.0)
    def test_eval_3 (self):
        u = 229220
        m = 6596
        r = netflix_eval(u,m)
        self.assertTrue(r <= 5.0 and r >=1.0)
    
    
    
        
    # -----
    # solve
    # -----
    
    def test_solve_1 (self):
        r = io.StringIO("1:\n30878\n1149588\n177\n16224:\n473783\n2482003\n1011946")
        w = io.StringIO()
        netflix_solve(r,w)
        s = str(w.getvalue())
        lines = s.split('\n')
        self.assertTrue(lines[0] == "1:")
        self.assertTrue(float(lines[1]) >= 1 and float(lines[1]) <= 5)
        self.assertTrue(float(lines[2]) >= 1 and float(lines[2]) <= 5)
        self.assertTrue(float(lines[3]) >= 1 and float(lines[3]) <= 5)
        self.assertTrue(lines[4] == "16224:")
        self.assertTrue(float(lines[5]) >= 1 and float(lines[5]) <= 5)
        self.assertTrue(float(lines[6]) >= 1 and float(lines[6]) <= 5)
        self.assertTrue(float(lines[7]) >= 1 and float(lines[7]) <= 5)
        self.assertTrue(lines[8].split(" ")[0] == "RMSE:")
        self.assertTrue(float(lines[8].split(" ")[1]) >= 0)
        
    def test_solve_1 (self):
        r = io.StringIO("3285:\n119716\n2152097\n2067395\n5729:\n618503\n823407\n1615992")
        w = io.StringIO()
        netflix_solve(r,w)
        s = str(w.getvalue())
        lines = s.split('\n')
        self.assertTrue(lines[0] == "3285:")
        self.assertTrue(float(lines[1]) >= 1 and float(lines[1]) <= 5)
        self.assertTrue(float(lines[2]) >= 1 and float(lines[2]) <= 5)
        self.assertTrue(float(lines[3]) >= 1 and float(lines[3]) <= 5)
        self.assertTrue(lines[4] == "5729:")
        self.assertTrue(float(lines[5]) >= 1 and float(lines[5]) <= 5)
        self.assertTrue(float(lines[6]) >= 1 and float(lines[6]) <= 5)
        self.assertTrue(float(lines[7]) >= 1 and float(lines[7]) <= 5)
        self.assertTrue(lines[8].split(" ")[0] == "RMSE:")
        self.assertTrue(float(lines[8].split(" ")[1]) >= 0)
    def test_solve_1 (self):
        r = io.StringIO("573:\n1385988\n1805399\n953124\n5370:\n2021399\n2550962\n2011870")
        w = io.StringIO()
        netflix_solve(r,w)
        s = str(w.getvalue())
        lines = s.split('\n')
        self.assertTrue(lines[0] == "573:")
        self.assertTrue(float(lines[1]) >= 1 and float(lines[1]) <= 5)
        self.assertTrue(float(lines[2]) >= 1 and float(lines[2]) <= 5)
        self.assertTrue(float(lines[3]) >= 1 and float(lines[3]) <= 5)
        self.assertTrue(lines[4] == "5370:")
        self.assertTrue(float(lines[5]) >= 1 and float(lines[5]) <= 5)
        self.assertTrue(float(lines[6]) >= 1 and float(lines[6]) <= 5)
        self.assertTrue(float(lines[7]) >= 1 and float(lines[7]) <= 5)
        self.assertTrue(lines[8].split(" ")[0] == "RMSE:")        
        self.assertTrue(float(lines[8].split(" ")[1]) >= 0)

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
