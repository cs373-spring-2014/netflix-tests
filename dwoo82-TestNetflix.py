#!/usr/bin/env python3

# -------------------------------
# projects/netflix/TestNetflix.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestNetflix.py > TestCollatz.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_eval, netflix_print, netflix_solve, rmse, build_cache_one, build_cache_two, avg_to_weight

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :

    # ----
    # read
    # ----

	def test_read (self):
		r = io.StringIO("1:\n48137624\n137245\n575895\n")
		p = netflix_read(r)
		(i, b) = p.__next__()
		self.assertTrue(i == 1)
		self.assertTrue(b)
		(j, b) = p.__next__()
		self.assertTrue(j == 48137624)
		self.assertTrue(not b)

	def test_read2 (self):
		r = io.StringIO("1:\n2:\n8246\n1928\n")
		p = netflix_read(r)

		(i, b) = p.__next__()
		self.assertTrue(i == 1)
		self.assertTrue(b)

		(j, b) = p.__next__()
		self.assertTrue(j == 2)
		self.assertTrue(b)

		(k, b) = p.__next__()
		self.assertTrue(k == 8246)
		self.assertFalse(b)

	def test_read3 (self):
		r = io.StringIO("0:\n3245234\n")
		p = netflix_read(r)
		(i, b) = p.__next__()
		self.assertTrue(i == 0)
		self.assertTrue(b)
		
	# ----
	# eval
	# ---
	
	def test_eval (self):
		m_cache = {"1000": 3.0}
		c_cache = {"1": 1.0}
		caches = [m_cache, c_cache]
		(m, c) = (1000, 1)
		v = netflix_eval((m, c), caches)
		self.assertTrue(v >= 1.0)
		self.assertTrue(v <= 5.0)
		self.assertTrue(v == 1.5)

	def test_eval2 (self):
		m_cache = {"2000": 3.0, "3000": 3.0}
		c_cache = {"1": 1.0, "4": 4.0}
		caches = [m_cache, c_cache]

		(m, c) = (2000, 4)
		v = netflix_eval((m, c), caches)
		self.assertTrue(v == 3.5)
		(m, c) = (2000, 1)
		v = netflix_eval((m, c), caches)
		self.assertTrue(v == 1.5)

		(m, c) = (3000, 4)
		v = netflix_eval((m, c), caches)
		self.assertTrue(v == 3.5)

		(m, c) = (3000, 1)
		v = netflix_eval((m, c), caches)
		self.assertTrue(v == 1.5)

	def test_eval3 (self):
		m_cache = {"9000": 3.0}
		c_cache = {"2": 1.0}
		caches = [m_cache, c_cache]
		(m, c) = (304953, 34872)
		self.assertRaises(KeyError, netflix_eval, (m, c), caches)

	 

	# -----
	# print
	# -----

	def test_print (self):
		w = io.StringIO()
		v = 23452345
		b = True
		netflix_print(w, v, b)
		self.assertTrue(w.getvalue() == "23452345:\n")

	def test_print2 (self):
		w = io.StringIO()
		v = 3.4
		b = False
		netflix_print(w, v, b)
		self.assertTrue(w.getvalue() == "3.4\n")

	def test_print3 (self):
		w = io.StringIO()
		v = 6.0
		b = False
		netflix_print(w, v, b)
		self.assertTrue(w.getvalue() == "6.0\n")

	# ----
	# rmse
	# ----
	
	def test_rmse(self):
		a = 500 * [1.0]
		p = 500 * [5.0]
		r = rmse(a,p)
		self.assertTrue(r == 4.0)

	def test_rmse2(self):
		a = [1, 2, 3, 4, 5]
		p = [1, 2, 3, 4, 5]
		r = rmse(a, p)
		self.assertTrue(r == 0.0)

	def test_rmse_backwards(self):
		a = [5, 4, 3, 2, 1]
		p = [5, 4, 3, 2, 1]
		r = rmse(a, p)
		self.assertTrue(r == 0.0)

	# -------------
	# avg_to_weight
	# -------------

	def test_avg_to_weight1(self):
		self.assertTrue(avg_to_weight(1.0) == .75)

	def test_avg_to_weight2(self):
		self.assertTrue(avg_to_weight(5.0) == .75)

	def test_avg_to_weight3(self):
		self.assertTrue(avg_to_weight(3.0) == .25)

	# ---------------
	# build_cache_one
	# ---------------

	# TODO: fix scoping of variables
	def test_build_cache_movies1(self):
		movies_cache = {}
		self.assertTrue(movies_cache == {})
		path = "test-caches/MovieAvgTest.json" 
		movies_cache = build_cache_one(path)
		self.assertTrue(movies_cache["1000"] == 3.0)

	def test_build_cache_movies2(self):
		movies_cache = {}
		self.assertTrue(movies_cache == {})
		path = "test-caches/MovieAvgTest.json" 
		movies_cache = build_cache_one(path)
		self.assertTrue(movies_cache["2000"] == 3.0)
	
	def test_build_cache_movies3(self):
		movies_cache = {}
		self.assertTrue(movies_cache == {})
		path = "test-caches/MovieAvgTest.json" 
		movies_cache = build_cache_one(path)
		self.assertTrue(movies_cache["8000"] == 3.0)

	def test_build_cache_customers1(self):
		customers_cache = {}
		self.assertTrue(customers_cache == {})
		path = "test-caches/CustAvgTest.json"
		customers_cache = build_cache_one(path)
		self.assertTrue(customers_cache["1"] == 1.0)

	def test_build_cache_customers2(self):
		customers_cache = {}
		self.assertTrue(customers_cache == {})
		path = "test-caches/CustAvgTest.json"
		customers_cache = build_cache_one(path)
		self.assertTrue(customers_cache["2"] == 2.0)

	def test_build_cache_customers3(self):
		customers_cache = {}
		self.assertTrue(customers_cache == {})
		path = "test-caches/CustAvgTest.json"
		customers_cache = build_cache_one(path)
		self.assertTrue(customers_cache["3"] == 3.0)

	def test_build_cache_customers4(self):
		customers_cache = {}
		self.assertTrue(customers_cache == {})
		path = "test-caches/CustAvgTest.json"
		customers_cache = build_cache_one(path)
		self.assertTrue(customers_cache["4"] == 4.0)
		self.assertTrue(customers_cache["5"] == 5.0)

	# ---------------
	# build_cache_two
	# ---------------

	def test_build_cache_probe(self):
		#path = "test-caches/ProbeTest.txt"
		probe_list = build_cache_two()
		self.assertTrue(probe_list[0] == 3.2)
		self.assertTrue(probe_list[1] == 1.4)
		self.assertTrue(probe_list[2] == 4.4)
		self.assertTrue(probe_list[3] == 2.5)
		self.assertTrue(probe_list[4] == 4.8)
		
	# -----
	# solve
	# -----

	def test_solve1(self):
		r = io.StringIO("1000:\n3\n2\n")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue("1000:\n3.0\n2.5\nRMSE: " in w.getvalue())

	def test_solve2(self):	
		r = io.StringIO("3000:\n1\n5\n")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue("3000:\n1.5\n4.5\nRMSE: " in w.getvalue())

	def test_solve3(self):	
		r = io.StringIO("9000:\n4\n2\n")
		w = io.StringIO()
		netflix_solve(r, w)
		self.assertTrue("9000:\n3.5\n2.5\nRMSE: " in w.getvalue())
		


# ----
# main
# ----

print ("TestNetflix.py")
unittest.main()
print ("Done.")
