#!/usr/bin/env python3
# -------------------------------
# Copyright (C) 2014
# Jiarui Hou Sudheesh Katkam
# -------------------------------

"""
To test the program:
    % python TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_movie_ratings, netflix_eval, netflix_rmse, netflix_print, netflix_solve
# -----------
# TestNetflix
# -----------


class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----
    def test_read1 (self):
        r = io.StringIO("48137624\n137245\n575895\n98:\n")
        m = {"customer_ids" : []}
        a = ["1"]
        p = netflix_read(r, m, a)
        self.assertTrue (p)
        self.assertTrue(m == {"movie_id" : "1", "customer_ids" : ["48137624", "137245", "575895"]})
        self.assertTrue(a == ["98"])
        

    def test_read2 (self):
        r = io.StringIO("30878\n2647871\n1283744\n2488120\n")
        m = {"customer_ids" : []}
        a = ["25"]
        p = netflix_read(r,m, a)
        m["movie_id"]= a[0]
        #p == False if on last line
        self.assertTrue(not p)
        self.assertTrue(m == {"movie_id" : "25", "customer_ids" : ["30878", "2647871", "1283744", "2488120"]})
        self.assertTrue(a == ["25"])
        
    def test_read3 (self):
        r = io.StringIO("48137624\n137245\n575895\n90:\n30878\n2647871\n1283744\n2488120\n32:")
        m = {"customer_ids" : []}
        a = ["1"]
        p = netflix_read(r,m, a)
        self.assertTrue(m == {"movie_id" : "1", "customer_ids" : ["48137624", "137245", "575895"]})
        self.assertTrue(a == ["90"])
        self.assertTrue(p)
        m = {"customer_ids" : []}
        p = netflix_read(r,m, a)
        self.assertTrue(m == {"movie_id" : "90", "customer_ids" : ["30878", "2647871", "1283744", "2488120"]})
        self.assertTrue(a == ["32"])
        self.assertTrue(p)

    # ------------
    # netflix_movie_ratings
    # ------------
    def test_movie_ratings1 (self):
        r = io.StringIO("2000:\n48137624,2,2010-10-01\n779760,5,2010-12-01\n")
        d = netflix_movie_ratings (r)
        self.assertTrue(d == {'48137624' : 2 , '779760' : 5})

    def test_movie_ratings2 (self):
        r = io.StringIO("5000:\n48137620,1,2013-10-01\n")
        d = netflix_movie_ratings (r)
        self.assertTrue(d == {'48137620' : 1})

    def test_movie_ratings3 (self):
        r = io.StringIO("1000:\n48137624,4,2010-10-01\n779760,1,2010-12-01\n1296163,3,2004-12-23")
        d = netflix_movie_ratings (r)
        self.assertTrue(d == {'48137624' : 4 ,'779760' : 1 , '1296163' : 3})

    # -------------
    # netflix_print
    # -------------
    def test_print1 (self):
        w = io.StringIO()
        m = {"movie_id" : "1" , "customer_ids" : [1.26, 2.593, 3.92324]}
        netflix_print(w, m)
        self.assertTrue (w.getvalue() == "1:\n1.3\n2.6\n3.9\n")

    def test_print2 (self):
        w = io.StringIO()
        m = {"movie_id" : "5" , "customer_ids" : [2, 3, 5.9]}
        netflix_print(w, m)
        self.assertTrue (w.getvalue() == "5:\n2.0\n3.0\n5.9\n")

    def test_print3 (self):
        w = io.StringIO()
        m = {"movie_id" : "10" , "customer_ids" : [5, 10, 15]}
        netflix_print(w, m)
        self.assertTrue(w.getvalue() == "10:\n5.0\n10.0\n15.0\n")

    # -------------
    # netflix_eval
    # -------------
    def test_eval1 (self):
        m   = {"movie_id" : "90", "customer_ids" : ["30878", "2647871", "1283744", "2488120"]}
        amr = {"90" : 4, "21" : 1}
        acr = {"30878" : 3, "2647871" : 2, "1283744" : 4, "2488120" : 3}
        ar  = {"90" : { "30878" : 1, "2647871" : 1, "1283744" : 3, "2488120" : 2}}
        d   = []
        netflix_eval(m, amr, acr, ar, d)
        self.assertTrue(d == [2.33, 1.33, 1.33, 1.33])

    def test_eval2 (self):
        m   = {"movie_id" : "2043", "customer_ids" : ["716091", "1481271", "1109700", "2098867"]}
        amr = {"2043" : 4}
        acr = {"716091" : 3, "1481271" : 1, "1109700" : 4, "2098867" : 3}
        ar  = {"2043" : {"716091" : 2, "1481271" : 3, "1109700" : 5, "2098867" : 4}}
        d   = []
        netflix_eval(m, amr, acr, ar, d)
        self.assertTrue(d == [1.33, -1.67, -0.6699999999999999, -0.6699999999999999])

    def test_eval3 (self):
        m   = {"movie_id" : "90", "customer_ids" : ["1283744", "2488120"]}
        amr = {"90" : 4, "21" : 1}
        acr = {"30878" : 3, "1283744" : 4, "2488120" : 3}
        ar  = {"90" : { "30878" : 1, "2647871" : 1, "1283744" : 3, "2488120" : 2}}
        d   = []
        netflix_eval(m, amr, acr, ar, d)
        self.assertTrue(d == [1.33, 1.33])

    # -------------
    # netflix_rmse
    # -------------
    def test_rmse1 (self):
        w = io.StringIO()
        d = [1.33, -1.67, -0.6699999999999999, -0.6699999999999999]
        p = netflix_rmse(w, d)
        self.assertTrue(w.getvalue() == "RMSE: 1.17")

    def test_rmse2 (self):
        w = io.StringIO()
        d = [1.33, 1.33]
        p = netflix_rmse(w, d)
        self.assertTrue(w.getvalue() == "RMSE: 1.33")

    def test_rmse3 (self):
        w = io.StringIO()
        d = [2.33, 1.33, 1.33, 1.33]
        p = netflix_rmse(w, d)
        self.assertTrue(w.getvalue() == "RMSE: 1.64")

    # -------------
    # netflix_solve
    # -------------
    def test_solve1 (self) :
        w = io.StringIO()
        r = io.StringIO("10:\n1952305\n1531863\n")
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10:\n2.9\n2.7\nRMSE: 0.24")

    def test_solve2 (self) :
        w = io.StringIO()
        r = io.StringIO("10002:\n1450941\n1213181\n308502\n2581993\n10003:\n1515111\n")
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10002:\n4.3\n3.3\n4.7\n4.0\n10003:\n2.4\nRMSE: 0.69")

    def test_solve3 (self) :
        w = io.StringIO()
        r = io.StringIO("10006:\n1093333\n1982605\n1534853\n1632583\n")
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10006:\n3.7\n3.2\n3.9\n2.7\nRMSE: 1.59")

# ----
# main
# ----
print ("TestNetflix.py")
unittest.main()
print ("Done.")
