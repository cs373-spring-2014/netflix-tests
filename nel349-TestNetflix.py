#!/usr/bin/env python3

# -------------------------------
# projects/Netflix/TestNetflix.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_eval, netflix_solve, list_compare, netflix_prediction_list, netflix_rmse, \
    netflix_prediction



# -----------
# TestCollatz
# -----------

class TestNetflix(unittest.TestCase):



    # ----
    # read
    # ----

    def test_read_1(self):
        r = io.StringIO('1455:\n10\n20\n30\n2454:\n4\n5\n6\n348:\n211\n3\n5\n57')
        a = []
        b = netflix_read(r, a)
        self.assertTrue(b)
        self.assertTrue( list_compare( a[0],(1455, [10, 20, 30])) )
        self.assertTrue( list_compare( a[1],(2454, [4, 5, 6]) ) )
        self.assertTrue( list_compare( a[2],(348, [211, 3, 5, 57])) )
        return 0
    def test_read_2(self):
        r = io.StringIO('10:\n100\n200\n300\n20:\n400\n500\n600\n30:\n1')
        a = []
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue( list_compare( a[0],(10, [100, 200, 300])) )
        self.assertTrue( list_compare( a[1],(20, [400, 500, 600])) )
        self.assertTrue( list_compare( a[2], (30, [1])) )
        return 0
    def test_read_3(self):
        r = io.StringIO('10020:\n1\n2\n3\n2:\n4\n5\n6\n3:\n1\n3\n5\n7')
        a = []
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue( list_compare( a[0], (10020, [1, 2, 3])) )
        self.assertTrue( list_compare( a[1], (2, [4, 5, 6])) )
        self.assertTrue( list_compare( a[2], (3,[1, 3, 5, 7])) )
        return 0


    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = io.StringIO("1000:\n2326571\n977808")
        w = io.StringIO()
        average_mov_rating = {}
        with open("/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt", 'r', encoding='utf-8') as infile:
            for line in infile:
                l = line.split(':')
                average_mov_rating[int(l[0])] = float(l[1])
        avgerage_user_rating = {}
        with open("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt", 'r', encoding='utf-8') as infile:
            for line in infile:
                l = line.split(':')
                avgerage_user_rating[int(l[0])] = float(l[1])
        movieCustList = []
        # movieCustList.append(1)

        netflix_read(r, movieCustList)

        prediction_result = netflix_eval(w, average_mov_rating, avgerage_user_rating, movieCustList)
        self.assertTrue(prediction_result[0] == [1000, [(2326571, 3.1814327485380116), (977808, 2.8720893141945774)]])


    def test_eval_2(self):
        r = io.StringIO("1:\n30878\n2647871")
        w = io.StringIO()
        average_mov_rating = {}
        with open("/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt", 'r', encoding='utf-8') as infile:
            for line in infile:
                l = line.split(':')
                average_mov_rating[int(l[0])] = float(l[1])
        avgerage_user_rating = {}
        with open("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt", 'r', encoding='utf-8') as infile:
            for line in infile:
                l = line.split(':')
                avgerage_user_rating[int(l[0])] = float(l[1])
        movieCustList = []

        netflix_read(r, movieCustList)

        prediction_result = netflix_eval(w, average_mov_rating, avgerage_user_rating, movieCustList)
        self.assertTrue(prediction_result[0] == [1, [(30878, 3.6831603124995573), (2647871, 3.2830758957405117)]])

        return 0
    def test_eval_3(self):
        r = io.StringIO("10001:\n262828\n2609496")
        w = io.StringIO()
        average_mov_rating = {}
        with open("/u/thunt/cs373-netflix-tests/ericweb2-movieAveragesOneLine.txt", 'r', encoding='utf-8') as infile:
            for line in infile:
                l = line.split(':')
                average_mov_rating[int(l[0])] = float(l[1])
        avgerage_user_rating = {}
        with open("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt", 'r', encoding='utf-8') as infile:
            for line in infile:
                l = line.split(':')
                avgerage_user_rating[int(l[0])] = float(l[1])
        movieCustList = []
        netflix_read(r, movieCustList)

        prediction_result = netflix_eval(w, average_mov_rating, avgerage_user_rating, movieCustList)
        self.assertTrue(prediction_result[0] == [10001, [(262828, 3.3954641350210966), (2609496, 4.2924095679985586)]])

        return 0

    # -----
    # solve
    # -----


    def test_solve_1(self):
        r = io.StringIO("1000:\n2326571\n977808")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue("1000:\n3.18\n2.87\nRMSE: " in w.getvalue())

    def test_solve_2(self):
        r = io.StringIO("1:\n30878\n2647871")
        a = io.StringIO()
        netflix_solve(r, a)
        self.assertTrue("1:\n3.68\n3.28\nRMSE: " in a.getvalue())

    def test_solve_3(self):
        r = io.StringIO("10001:\n262828\n2609496")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue("10001:\n3.40\n4.29\nRMSE: " in w.getvalue())

    # -----
    # netflix_rmse
    # -----

    def test_netflix_rmse_1(self):
        a = [1, 2, 3]
        p = [1.4, 2.4, 3.4]
        self.assertTrue(netflix_rmse(a, p) == 0.3999999999999999)

    def test_netflix_rmse_2(self):
        a = [3, 2, 5]
        p = [2.1, 2.9, 4.1]
        self.assertTrue(netflix_rmse(a, p) == 0.9)

    def test_netflix_rmse_3(self):
        a = [1, 2, 3]
        p = [0.1, 1.1, 3.9]
        self.assertTrue(netflix_rmse(a, p) == 0.9)


    # ----
    # print_predictions
    # ----
    def test_netflix_prediction_1(self):
        a = 1
        b = 2
        c = netflix_prediction( a, b)
        self.assertTrue( c != "" )
        return 0

    def test_netflix_prediction_2(self):
        a = [3, 5, 2]
        b = [2, 4, 3]
        c = map(netflix_prediction, a, b)

        self.assertTrue(c != "" )
        return 0

    def test_netflix_prediction_3(self):
        a = [3, 5, 2]
        b = [3, 4, 1]
        c = map(netflix_prediction, a, b)

        self.assertTrue(c != "" )
        return 0

        # ----
        # main
        # ----


print("TestNetflix.py")
unittest.main()
print("Done.")
