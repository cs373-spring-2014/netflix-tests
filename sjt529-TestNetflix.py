#!/usr/bin/env python3

# --------------------------------------------
# workspace/CS373/cs373-netflix/TestNetflix.py
# Copyright (c) 2014
# Sam Tipton
# Trent Kan
# --------------------------------------------

"""
To test the program:
	% python TestNetflix.py > TestNetflix.out
	% chmod ugo+x TestNetflix.py
	% TestNetflix.py > TestNetflix.py
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_solve, netflix_rate, rmse, fill_actual_cache, fill_movie_cache, fill_cust_cache, actual_ratings, first_movie_id, begin_index, movie_averages, customer_averages, predicted_ratings 

# -----------
# TestNetflix
# -----------

class TestNetflix(unittest.TestCase) :
	

	# ------------
	# netflix_rate
	# ------------
	def test_netflix_rate1 (self) :
		import Netflix

		Netflix.movie_averages = {"1" : 4.0}
		Netflix.customer_averages = {"5": 4.5}
		Netflix.average_customer_rating = 4.0
		Netflix.average_movie_rating = 3.5

		self.assertTrue(netflix_rate("1", "5") == 4.5)
		
	

	def test_netflix_rate2 (self) :
		import Netflix

		Netflix.movie_averages = {"2" : 5.0} 
		Netflix.customer_averages = {"9": 4.5}
		Netflix.average_customer_rating = 4.0
		Netflix.average_movie_rating = 3.5
		#shouldn't overflow
		self.assertTrue(netflix_rate("2", "9") == 5.0)
		



	def test_netflix_rate3 (self) :
		import Netflix

		Netflix.movie_averages = {"2" : 1.5} 
		Netflix.customer_averages = {"5": 2.0}
		Netflix.average_customer_rating = 4.0
		Netflix.average_movie_rating = 3.0

		self.assertTrue(netflix_rate("2", "5") == 1.0)
		

	# ----
	# rmse
	# ----

	def test_rmse1 (self) :
		import Netflix

		Netflix.actual_ratings = [[3.0, 4.0, 5.0, 1.0, 2.0]]
		Netflix.predicted_ratings = [[3.0, 4.0, 5.0, 1.0, 2.0]]

		self.assertTrue( rmse() == 0 )


	def test_rmse2 (self) :
		import Netflix

		Netflix.actual_ratings = [[3.0, 4.0, 5.0, 1.0, 2.0]]
		Netflix.predicted_ratings = [[4.0, 5.0, 4.0, 2.0, 1.0]]

		self.assertTrue( rmse() == 1 )

	def test_rmse3 (self) :
		import Netflix

		Netflix.actual_ratings = [[1.0]]
		Netflix.predicted_ratings = [[0.0]]

		self.assertTrue( rmse() == 1 )

	# -----------------
	# fill_actual_cache
	# -----------------

	def test_fill_actual_cache1(self):
		fill_actual_cache()
		self.assertTrue(len(actual_ratings) == 16938)

	def test_fill_actual_cache2(self):
		self.assertTrue(type(actual_ratings[0]) == list)
		self.assertTrue(actual_ratings[0] == [4, 4, 3, 5, 5, 4, 4, 4, 3, 4, 5, 4, 5, 4, 3, 5, 4, 5, 4, 5, 3, 5, 5])

	def test_fill_actual_cache3(self):
		self.assertTrue(actual_ratings[1] == [3, 3])
		self.assertTrue(actual_ratings[3] == [5, 4])

	def test_fill_actual_cache4(self) :
		self.assertTrue(actual_ratings[-1] == [2])
		self.assertTrue(actual_ratings[-2] == [3, 4, 5])



	# ----------------
	# fill_movie_cache
	# ----------------

	def test_fill_movie_cache1(self):
		fill_movie_cache()
		self.assertTrue(movie_averages['17757'] == 3.8)
		self.assertTrue(movie_averages['17635'] == 3.002)
		self.assertTrue(movie_averages['13'] == 4.552)

	def test_fill_movie_cache2(self):
		self.assertTrue('voldemort' not in movie_averages)
		self.assertTrue('0' not in movie_averages)
		self.assertTrue(0 not in movie_averages)
		self.assertTrue('18000' not in movie_averages)


	def test_fill_movie_cache3(self):
		self.assertTrue('2342' in movie_averages)
		self.assertTrue('5555' in movie_averages)
		self.assertTrue('10001' in movie_averages)

	def test_fill_movie_cache4(self) :
		self.assertTrue(list not in movie_averages)
		self.assertTrue(movie_averages['17361'] == 2.9731474969631098)

	# ---------------
	# fill_cust_cache
	# ---------------

	def test_fill_cust_cache1(self):
		fill_cust_cache()
		self.assertTrue(customer_averages['1048600'] == 4.0)
		self.assertTrue(customer_averages['2053057'] == 3.92)
		self.assertTrue(customer_averages['2097129'] == 3.512)
		self.assertTrue(customer_averages['1048526'] == 3.32)
		self.assertTrue(customer_averages['2096970'] == 3.625)

	def test_fill_cust_cache2(self):
		self.assertTrue('20000000' not in customer_averages)
		self.assertTrue('0' not in customer_averages)
		self.assertTrue(0 not in customer_averages)
		self.assertTrue('a' not in customer_averages)

	def test_fill_cust_cache3(self):
		self.assertTrue('25' in customer_averages)
		self.assertTrue('2097269' in customer_averages)
		self.assertTrue('94' in customer_averages)

	def test_fill_cust_cache4(self):
		self.assertTrue('2097148' in customer_averages)
		self.assertTrue(customer_averages['2097148'] == 3.08)
		self.assertTrue(45 not in customer_averages)

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
