#!/usr/bin/env python3

# ---------------------------
# Copyright (C) 2014
# Andrew Solis and Mitch Stephan
# ---------------------------

import io
import unittest
import sys
import json

from Netflix import solve, read, predict_ratings, calculate_rmse

# -----------
# TestNetflix
# -----------
cache_dir = '/u/thunt/cs373-netflix-tests/'
avg_movie_ratings_cache = cache_dir + 'ddg625-MovieCache.json' 
avg_user_ratings_cache = cache_dir + 'irvin-user_avg.json'
user_rating_cache = cache_dir + 'ddg625-ActualCache.json'

class TestNetflix (unittest.TestCase) :   

    def test_read_1(self) :
        r = io.StringIO("10014:\n1626179\n1359761\n430376\n723484")
        w = io.StringIO()
        rating_list = ['','']
        self.assertTrue(read(r, w, rating_list) == True)
        self.assertTrue(rating_list[0] == "10014")
        self.assertTrue(rating_list[1] == "1626179")

    def test_read_2(self) :
        r = io.StringIO("10014\n")
        w = io.StringIO()
        rating_list = ['','']
        self.assertTrue(read(r, w, rating_list) == True)
        self.assertTrue(rating_list[0] == "")
        self.assertTrue(rating_list[1] == "10014")        

    def test_read_3_recursion(self) :
        r = io.StringIO("10014:\n1020:\n123")
        w = io.StringIO()
        rating_list = ['','']
        self.assertTrue(read(r, w, rating_list) == True)        
        self.assertTrue(rating_list[0] == "1020")
        self.assertTrue(rating_list[1] == "123")

    def test_read_4_recursion(self) :
        r = io.StringIO("10014:\n1020:")
        w = io.StringIO()
        rating_list = ['','']
        self.assertTrue(read(r, w, rating_list) == False)        
        self.assertTrue(rating_list[0] == "1020")

    def test_read_5_nothing(self) :
        r = io.StringIO("")
        w = io.StringIO()
        rating_list = ['','']
        self.assertTrue(read(r, w, rating_list) == False)        
   
    def test_predict_ratings_1(self) :    
        w = io.StringIO()
        predicted_ratings_list = []
        avg_movie_cache = {"0": 3}
        avg_user_cache = {"0": 3}
        rating_tuple = ('0', '0') 
        predict_ratings(w, rating_tuple, predicted_ratings_list, avg_movie_cache, avg_user_cache)
        avg_rating = 3.6741013034524053
        result = round(avg_rating + (3 - avg_rating) + (3 - avg_rating),2)
        self.assertTrue(predicted_ratings_list[0] == result)

    def test_predict_ratings_2(self) :    
        w = io.StringIO()
        predicted_ratings_list = []
        avg_movie_cache = {"0": 4}
        avg_user_cache = {"0": 2}
        rating_tuple = ('0', '0') 
        predict_ratings(w, rating_tuple, predicted_ratings_list, avg_movie_cache, avg_user_cache)
        avg_rating = 3.6741013034524053
        result = round(avg_rating + (4 - avg_rating) + (2 - avg_rating),2)
        self.assertTrue(predicted_ratings_list[0] == result)

    def test_predict_ratings_3(self) :    
        w = io.StringIO()
        predicted_ratings_list = []
        avg_movie_cache = {"0": 4, "1": 2, "2": 3}
        avg_user_cache = {"0": 5, "1": 2, "2": 5}
        rating_tuple = ('1', '2') 
        predict_ratings(w, rating_tuple, predicted_ratings_list, avg_movie_cache, avg_user_cache)
        avg_rating = 3.6741013034524053
        result = round(avg_rating + (2 - avg_rating) + (5 - avg_rating),2)
        self.assertTrue(predicted_ratings_list[0] == result)

    def test_calculate_rmse_with_same_list(self) :
        list_one = [1,2,3]
        self.assertTrue(calculate_rmse(list_one, list_one) == 0)

    def test_calculate_rmse_with_list_two_greater_than_list_one(self) :
        list_one = [0,1,2,3,4,5]
        list_two = [1,2,3,4,5,6]
        self.assertTrue(calculate_rmse(list_one, list_two) == 1)

    def test_calculate_rmse_with_list_one_greater_than_list_two(self) :
        list_one = [1,2,3,4,5,6]
        list_two = [0,1,2,3,4,5]
        self.assertTrue(calculate_rmse(list_one, list_two) == 1)

    def test_solve_when_empty(self) :
        r = io.StringIO("")
        w = io.StringIO()
        solve(r, w)
        self.assertTrue(w.getvalue() == "RMSE: 0.0\n")

    def test_solve_with_one_case(self) :
        r = io.StringIO("1:\n30878")
        w = io.StringIO()
        solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.71\nRMSE: 0.29\n")

    def test_solve_with_two_cases(self) :
        r = io.StringIO("1:\n30878\n2647871\n")
        w = io.StringIO()
        solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.71\n3.31\nRMSE: 0.53\n")


print("TestNetflix.py")
unittest.main()
print("Done.")