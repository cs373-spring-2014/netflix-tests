#!/usr/bin/env python3

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_rmse, netflix_eval, netflix_predict, \
netflix_print, netflix_solve

# -----------
# TestNetflix
# -----------

class TestNetflix(unittest.TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        r = io.StringIO("1:\n2647871\n30878\n")
        customers = netflix_read(r)
        self.assertTrue(type(customers['1']) is list)
        self.assertTrue(customers['1'][0] == '2647871')
        self.assertTrue(customers['1'][1] == '30878')

    def test_read_2(self): # Inputs on two customers
        r = io.StringIO("1:\n30878\n2647871\n10:\n1952305\n1531863")
        customers = netflix_read(r)
        self.assertTrue(type(customers['1']) is list)
        self.assertTrue(type(customers['10']) is list)
        self.assertTrue(customers['1'] == ['30878', '2647871'])
        self.assertTrue(customers['10'] == ['1952305','1531863'])

    # ----
    # rmse
    # ----

    def test_rmse_1(self):
        l1 = [0,1,2]
        l2 = [1,2,3]
        rmse = netflix_rmse(l1, l2)
        self.assertTrue(rmse == 1)

    def test_rmse_2(self):
        l1 = [0,1,2]
        l2 = [0,1,2]
        rmse = netflix_rmse(l1, l2)
        self.assertTrue(rmse == 0)

    def test_rmse_3(self):
        l1 = [0.1, 1.2, 2.3]
        l2 = [3.2, 2.1, 1.0]
        rmse = netflix_rmse(l1,l2)
        self.assertTrue(str(rmse) == '2.0091457554559518')

    # ----
    # predict
    # ----

    def test_predict_1(self):
        pred = netflix_predict('10', ['1952305','1531863'])
        self.assertTrue(type(pred[0]) is float)
        self.assertTrue(len(pred) == len(['1952305','1531863']))

    def test_predict_2(self):
        r = io.StringIO("1:\n2647871\n30878\n")
        customers = netflix_read(r)
        pred = netflix_predict('1', customers['1'])
        self.assertTrue(type(pred[0]) is float)
        self.assertTrue(len(pred) == len(customers['1']))

    def test_predict_3(self):
        r = io.StringIO("1:\n30878\n2647871\n10:\n1952305\n1531863")
        customers = netflix_read(r)
        for c in customers:
            pred = netflix_predict(c, customers[c])
            self.assertTrue(type(pred[0]) is float)
            self.assertTrue(len(pred) == len(customers[c]))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        custs = {'1': ['30878', '2647871']}
        pred = netflix_eval(custs)
        self.assertTrue(type(pred['1']) is list)
        self.assertTrue(len(pred['1']) == len(custs['1']))

    def test_predict_2(self):
        r = io.StringIO("1:\n2647871\n30878\n")
        custs = netflix_read(r)
        pred = netflix_eval(custs)
        self.assertTrue(type(pred['1']) is list)
        self.assertTrue(len(pred['1']) == len(custs['1']))

    def test_predict_3(self):
        r = io.StringIO("1:\n30878\n2647871\n10:\n1952305\n1531863")
        custs = netflix_read(r)
        pred = netflix_eval(custs)
        self.assertTrue(type(pred['1']) is list)
        self.assertTrue(len(pred['1']) == len(custs['1']))
        self.assertTrue(type(pred['10']) is list)
        self.assertTrue(len(pred['10']) == len(custs['10']))

    # -----
    # print
    # -----

    def test_print(self):
        w = io.StringIO()
        custs = {'1': [3.0,3.0]}
        actual = {'1': ['3', '3']}
        netflix_print(w, custs, actual)
        self.assertTrue('3.' in w.getvalue() and 'RMSE' in w.getvalue())

    def test_print_2(self):
        w = io.StringIO()
        custs = {'1': ['30878', '2647871']}
        actual = {'1': ['3', '3']}
        pred = netflix_eval(custs)
        netflix_print(w, pred, actual)
        self.assertTrue('3.' in w.getvalue() and 'RMSE' in w.getvalue())

    def test_print_3(self):
        r = io.StringIO("1:\n30878\n2647871\n10:\n1952305\n1531863")
        custs = netflix_read(r)
        actual = {'1': ['3', '3'], '10':['3', '3']}
        w = io.StringIO()
        pred = netflix_eval(custs)
        netflix_print(w, pred, actual)
        self.assertTrue('3.' in w.getvalue() and 'RMSE' in w.getvalue())

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = io.StringIO("1:\n30878\n2647871")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue().find("1:\n3.709531820492699\n3.309447403733653\n") != -1)
# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")

