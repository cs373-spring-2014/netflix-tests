#!/usr/bin/env python3

"""
To test the program:
    % python TestNetflix.py >& TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py >& TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import get_actual_rating, rmse, get_movie_avgs, avg_movie_guess, get_cust_avgs, get_cust_decade, get_movie_decades, get_num_ratings, solve

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # get_actual_rating
    # ----
    def test_get_actual_rating_1 (self) :
        result_dict = get_actual_rating('probe.txt', 0, 23)
        assert(result_dict == {'1': [('30878', 4.0), ('2647871', 4.0), ('1283744', 3.0), ('2488120', 5.0), ('317050', 5.0), ('1904905', 4.0), ('1989766', 4.0), ('14756', 4.0), ('1027056', 3.0), ('1149588', 4.0), ('1394012', 5.0), ('1406595', 4.0), ('2529547', 5.0), ('1682104', 4.0), ('2625019', 3.0), ('2603381', 5.0), ('1774623', 4.0), ('470861', 5.0), ('712610', 4.0), ('1772839', 5.0), ('1059319', 3.0), ('2380848', 5.0), ('548064', 5.0)]})
        
    def test_get_actual_rating_2 (self) :
        result_dict = get_actual_rating('probe.txt', 0, 25)
        result_dict.pop('1')
        assert(result_dict == {'10': [('1952305', 3.0), ('1531863', 3.0)]})

    def test_get_actual_rating_3 (self) :
        result_dict = get_actual_rating('probe.txt', 0, 52)
        result_dict.pop('1')
        result_dict.pop('10')
        assert(result_dict == {'1000': [('2326571', 3.0), ('977808', 3.0), ('1010534', 2.0), ('1861759', 5.0), ('79755', 5.0), ('98259', 5.0), ('1960212', 2.0), ('97460', 5.0), ('2623506', 4.0), ('2409123', 4.0), ('1959111', 5.0), ('809597', 3.0), ('2251189', 3.0), ('537705', 4.0), ('929584', 5.0), ('506737', 3.0), ('708895', 3.0), ('1900790', 4.0), ('2553920', 3.0), ('1196779', 4.0), ('2411446', 1.0), ('1002296', 5.0), ('1580442', 3.0), ('100291', 2.0), ('433455', 4.0), ('2368043', 5.0), ('906984', 4.0)]})

    # ----
    # get_movie_avgs()
    # ----
    def test_get_movie_avgs (self) :
        result_dict = get_movie_avgs()
        assert(result_dict['1'] == 3.749542961608775)
        assert(result_dict['100'] == 2.6538461538461537)
        assert(result_dict['10000'] == 2.902325581395349)

    # ----
    # get_cust_avgs()
    # ----
    def test_get_cust_avgs (self) :
        result_dict = get_cust_avgs()
        assert(result_dict['1147742'] == 3.2857142857142856)
        assert(result_dict['1966655'] == 4.0)
        assert(result_dict['19475'] == 3.872340425531915)

    # ----
    # get_cust_decade()
    # ----
    def test_get_cust_decade (self) :
        result_dict = get_cust_decade()
        assert(result_dict['2229124'] == {1930: 4.0})
        assert(result_dict['282240'] == {2000: 4.5})
        assert(result_dict['1744054'] == {1980: 3.4408602150537635} )

    # ----
    # get_movie_decades()
    # ----
    def test_get_movie_decade (self) :
        result_dict = get_movie_decades()
        assert(result_dict['1299'] == 1972)
        assert(result_dict['8488'] == 2004)
        assert(result_dict['13354'] == 2004 )

    def test_get_num_ratings(self):
        result_dict = get_num_ratings()
        assert (result_dict['10554'] == 5584)
        assert (result_dict['4445'] == 212)
        assert (result_dict['8221'] == 1138)
        
    # ----
    # avg_movie_guess()
    # ----
    def test_avg_movie_guess_1 (self) :
        get_movie_decades()
        get_num_ratings()
        get_movie_avgs()
        get_cust_avgs()
        result_dict = avg_movie_guess('probe.txt', 0, 23)
        assert(result_dict == {'1': [('30878', 4.0), ('2647871', 3.5319566467642964), ('1283744', 4.0), ('2488120', 5.0), ('317050', 4.0), ('1904905', 4.0), ('1989766', 3.631757045965893), ('14756', 4.0), ('1027056', 4.276201490410337), ('1149588', 3.7631856173944644), ('1394012', 3.2101120243208716), ('1406595', 4.0), ('2529547', 4.217342631551478), ('1682104', 4.240452698139807), ('2625019', 3.0), ('2603381', 4.0), ('1774623', 4.0), ('470861', 4.752969167178015), ('712610', 4.3753467895556355), ('1772839', 4.425407839616688), ('1059319', 3.319257045965893), ('2380848', 5.0), ('548064', 3.768892144404454)]})

    def test_avg_movie_guess_2 (self) :
        get_movie_decades()
        get_num_ratings()
        get_movie_avgs()
        get_cust_avgs()
        result_dict = avg_movie_guess('probe.txt', 0, 25)
        result_dict.pop('1')
        assert(result_dict == {'10': [('1952305', 3.0), ('1531863', 3.0)]})
   
    def test_avg_movie_guess_3 (self) :
        get_movie_decades()
        get_num_ratings()
        get_movie_avgs()
        get_cust_avgs()
        result_dict = avg_movie_guess('probe.txt', 0, 52)
        result_dict.pop('1')
        result_dict.pop('10')
        assert(result_dict == {'1000': [('2326571', 3.4302188376626486), ('977808', 3.0), ('1010534', 3.0), ('1861759', 5.0), ('79755', 4.0), ('98259', 3.589753372197183), ('1960212', 3.4579966154404254), ('97460', 4.0), ('2623506', 3.0), ('2409123', 2.6912643319758587), ('1959111', 3.475101878598321), ('809597', 3.7222955084293563), ('2251189', 3.4295800582787965), ('537705', 4.517207141756215), ('929584', 4.0), ('506737', 3.2283454526497284), ('708895', 3.0), ('1900790', 3.2210563169329625), ('2553920', 3.3828749609392097), ('1196779', 4.0), ('2411446', 3.5663299487737596), ('1002296', 4.0), ('1580442', 2.568290733087485), ('100291', 3.2404040228478337), ('433455', 4.246789718888701), ('2368043', 3.2811020520137375), ('906984', 4.0)]})
   
    # ----
    # solve()
    # ----
    def test_solve_1 (self) :
        value = solve('probe.txt', 0, 23)
        assert(str(value) == '0.693566249704')
        
    def test_solve_2 (self) :
        value = solve('probe.txt', 0, 25)
        assert(str(value) == '0.665245376767')
        
    def test_solve_3 (self) :
        value = solve('probe.txt', 0, 52)
        assert(str(value) == '0.860537706439')
      
# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
