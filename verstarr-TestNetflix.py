#!/usr/bin/env python3

# ------------------------------
# projects/cs373-netflix/RunNetflix.py
# Copyright (C) 2014
# Noah Cho and Ver Andrew Starr
# -------------------------------

"""
To test the program:
    % python TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest
import os

from Netflix import netflix, netflix_rsme, netflix_predictionlist, netflix_read, netflix_print

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :

	#----
	# netflix read
	#----
	def test_netflix_read1(self) :
		r = io.StringIO("1:\n")
		result = netflix_read(r)
		self.assertTrue(result)

		r = io.StringIO("1\n")
		result = netflix_read(r)
		self.assertTrue(result)

	def test_netflix_read2(self) :
		r = io.StringIO("1:\n")
		result = netflix_read(r)
		self.assertTrue(result)

		r = io.StringIO("")
		result = netflix_read(r)
		self.assertFalse(result)

	def test_netflix_read3(self) :
		r = io.StringIO("1:\n")
		result = netflix_read(r)
		self.assertTrue(result)

		r = io.StringIO("3214")
		result = netflix_read(r)
		self.assertTrue(result)

		r = io.StringIO("1120")
		result = netflix_read(r)
		self.assertTrue(result)

		r = io.StringIO("9999")
		result = netflix_read(r)
		self.assertTrue(result)

		r = io.StringIO("272:")
		result = netflix_read(r)
		self.assertTrue(result)

	def test_netflix_read4(self) :
		r = io.StringIO("1:\n3\n")
		result = netflix_read(r)
		self.assertTrue(result)

		result = netflix_read(r)
		self.assertTrue(result)

		result = netflix_read(r)
		self.assertFalse(result)

	def test_netflix_print1(self) :
		plist = [1, 2, 3]
		index = 0
		w = io.StringIO()
		r = io.StringIO("1:\n1\n2\n3\n")
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "1:\n")

		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "1:\n1.0\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "1:\n1.0\n2.0\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "1:\n1.0\n2.0\n3.0\n")

	def test_netflix_print2(self) :
		plist = [2.34, 2.11, 3.5]
		index = 0
		w = io.StringIO()
		r = io.StringIO("2:\n2\n3\n4\n")
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n")

		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.3\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.3\n2.1\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.3\n2.1\n3.5\n")

	def test_netflix_print3(self) :
		plist = [2.34, 2.11, 3.5]
		index = 0
		w = io.StringIO()
		r = io.StringIO("2:\n")
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n")

	def test_netflix_print4(self) :
		plist = [2.5, 3.5, 4.5, 4.5, 2.0]
		index = 0
		w = io.StringIO()
		r = io.StringIO("2:\n1\n2\n3\n4\n")
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n")

		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.5\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.5\n3.5\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.5\n3.5\n4.5\n")

		index += 1
		probe = r.readline()
		netflix_print(w, probe, plist, index)
		self.assertTrue(w.getvalue() == "2:\n2.5\n3.5\n4.5\n4.5\n")


	#----
	# RSME
	#----
	def test_netflix_rsme1(self) :
		alist = [3, 3, 3]
		plist = [3, 3, 3]

		result = netflix_rsme(alist, plist)
		assert(result >= 0)
		self.assertTrue(result == 0)

	def test_netflix_rsme2(self) :
		alist = [1, 2, 3]
		plist = [3, 2, 1]

		result = netflix_rsme(alist, plist)
		assert(result >= 0)
		result = round(result, 1)
		self.assertTrue(result == 1.6)

	def test_netflix_rsme3(self) :
		alist = [10, 10, 10]
		plist = [1, 1, 1]

		result = netflix_rsme(alist, plist)
		assert(result >= 0)
		result = round(result, 1)
		self.assertTrue(result == 9)

	def test_netflix_rsme4(self) :
		alist = [-9, -9, -9]
		plist = [0, 0, 0]

		result = netflix_rsme(alist, plist)
		assert(result >= 0)
		result = round(result, 1)
		self.assertTrue(result == 9)

	def test_netflix_rsme5(self) :
		alist = [0, 0, 0]
		plist = [0, 0, 0]

		result = netflix_rsme(alist, plist)
		assert(result >= 0)
		result = round(result, 1)
		self.assertTrue(result == 0)

	def test_netflix_rsme6(self) :
		alist = [-1, -1, -1]
		plist = [-3, -3, -3]

		result = netflix_rsme(alist, plist)
		assert(result >= 0)
		result = round(result, 1)
		self.assertTrue(result == 2)



	#----
	# netflix_predictionlist
	#----
	def test_netflix_predictionlist1(self) :
		plist = []
		custAvgDict = {1: 1, 2: 2, 3: 3}
		movAvgDict = {1: 1, 2: 2, 3: 3}

		r = io.StringIO("1:\n1\n2\n3\n")
		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 0)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 1)
		self.assertTrue(plist[0] == 1)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 2)
		self.assertTrue(plist[0] == 1)
		self.assertTrue(plist[1] == 1)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 3)
		self.assertTrue(plist[0] == 1)
		self.assertTrue(plist[1] == 1)
		self.assertTrue(plist[2] == 0.5)

	def test_netflix_predictionlist2(self) :
		plist = []
		custAvgDict = {1: 5, 2: 5, 3: 5}
		movAvgDict = {1: 5, 2: 1, 3: 4}
		r = io.StringIO("1:\n2:\n3:\n")

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 0)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 0)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 0)

	def test_netflix_predictionlist3(self) :
		plist = []
		custAvgDict = {1: 5, 2: 5, 3: 5}
		movAvgDict = {1: 5, 2: 5, 3: 5}
		r = io.StringIO("1:\n1\n2\n3\n")

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 0)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 1)
		self.assertTrue(plist[0] == 5)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 2)
		self.assertTrue(plist[0] == 5)
		self.assertTrue(plist[1] == 5)

		plist = netflix_predictionlist(r.readline(), plist, custAvgDict, movAvgDict)
		self.assertTrue(len(plist) == 3)
		self.assertTrue(plist[0] == 5)
		self.assertTrue(plist[1] == 5)
		self.assertTrue(plist[2] == 5)

	def test_netflix1(self) : 
		r = io.StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050")
		w = io.StringIO()
		netflix(r, w)
		self.assertTrue(w.getvalue() == "1:\n3.9\n3.5\n3.8\n4.9\n3.9\nRMSE: 0.6512\n")

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")