#!/usr/bin/env python3

import io, json
import unittest
from Netflix import netflix_solve, rmse_sum_gen_zip

#-----------
#TestNetflix
#-----------

# ----
# main
# ----
with open("/u/thunt/cs373-netflix-tests/timsim-probe_ans.json") as f:
  comparison_file = json.load(f)
with open( "/u/thunt/cs373-netflix-tests/irvin-user_avg.json") as f:
  usr_avg = json.load(f)
with open("/u/thunt/cs373-netflix-tests/irvin-movie_avg.json") as f:
  movie_avg = json.load(f)
cache_files = [usr_avg, movie_avg]

class TestNetflix (unittest.TestCase) :
  def test_RMSE1(self):
    c1 = (2, 3, 4)
    c2 = (2, 3, 4)
    assert(str(rmse_sum_gen_zip(c1, c2)) == "0.0")
  
  def test_RMSE2(self):
    c1 = (2, 3, 4)
    c2 = (3, 4, 5)
    assert(str(rmse_sum_gen_zip(c1, c2)) == "1.0")

  def test_RMSE3(self):
    c1 = (2, 3, 4)
    c2 = (4, 3, 2)
    assert(str(rmse_sum_gen_zip(c1, c2)) == "1.632993161855452")

  def test_RMSE4(self):
    c1 = 1000000 * [1]
    c2 = 1000000 * [5]
    assert(str(rmse_sum_gen_zip(c1, c2)) == "4.0")


 
  def test_solve1(self):
    r = io.StringIO("1:\n30878\n2647871\n1283744\n2488120")
    w = io.StringIO()

    netflix_solve(r, w, comparison_file, cache_files)
    w = w.getvalue().split("\n")
    assert(float(w[-1].split()[-1]) < 1.0)
    
  def test_solve2(self):
    r = io.StringIO("10073:\n855860\n770122\n2027058\n166603\n2524705\n1027050\n1842060\n99477")
    w = io.StringIO()

    netflix_solve(r, w, comparison_file, cache_files)
    w = w.getvalue().split("\n")
    assert(float(w[-1].split()[-1]) < 1.0)

  def test_solve3(self):
    r = io.StringIO("10047:\n205729\n827378\n711436\n1636386\n193212\n346924\n1951856\n743660\n10048:\n1590947\n1367805\n449591\n1293456\n1741802\n2350428\n1295702\n10049:\n1430873\n555892\n2082280")
    w = io.StringIO()

    netflix_solve(r, w, comparison_file, cache_files)
    w = w.getvalue().split("\n")
    assert(float(w[-1].split()[-1]) < 1.0)

  def test_solve4(self):
    r = io.StringIO("10083:\n939390\n1128494\n2517616\n1752856\n1417447\n2527611\n464815\n652692\n2161939\n2218833\n813694\n1430552\n1256963\n1886834\n376589\n1139743\n2527168\n458392\n750646")
    w = io.StringIO()

    netflix_solve(r, w, comparison_file, cache_files)
    w = w.getvalue().split("\n")
    assert(float(w[-1].split()[-1]) < 1.0)

print("TestNetflix.py")
unittest.main()
print("Done.")
