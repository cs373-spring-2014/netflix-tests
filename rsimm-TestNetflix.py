# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, cache_lookup, netflix_eval, netflix_solve, rmse

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1:\n")
        a = ["", ""]
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == "1:\n")
        self.assertTrue(a[1] == "")

    def test_read_2 (self) :
        r = io.StringIO("1000\n")
        a = ["", ""]
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == "")
        self.assertTrue(a[1] == "1000\n")


    def test_read_3 (self) :
        #first read
        r = io.StringIO("17:\n")
        a = ["", ""]
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == "17:\n")
        self.assertTrue(a[1] == "")
        #second read
        r = io.StringIO("891\n")
        b = netflix_read(r, a)
        self.assertTrue(b == True)
        self.assertTrue(a[0] == "17:\n")
        self.assertTrue(a[1] == "891\n")


    # ------------
    # cache_lookup
    # ------------

    def test_lookup_1 (self) :
        s = "9719"
        c_id = "MOVIE_AVGS"
        v = cache_lookup(s, c_id)
        self.assertTrue(v == 1.9054054054054055)


    def test_lookup_2 (self) :
        s = "1614560"
        c_id = "CUST_AVGS"
        v = cache_lookup(s, c_id)
        self.assertTrue(v == 3.0)

    def test_lookup_3 (self) :
        s = "2625019"
        c_id = "CUST_AVGS"
        v = cache_lookup(s, c_id)
        self.assertTrue(v == 2.727272727272727)

   
    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        a = ["1","30878"]
        e = netflix_eval(a)
        self.assertTrue(e == 3.6831603124995573)


    def test_eval_2 (self) :
        a = ["10","1952305"]
        e = netflix_eval(a)
        self.assertTrue(e == 2.890063550906924)

    def test_eval_3 (self) :
        a = ["1001","1050889"]
        e = netflix_eval(a)
        self.assertTrue(e == 4.331500414479137)

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = io.StringIO("1:\n30878\n2647871\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "1:\n3.6831603124995573\n3.2830758957405117\nRMSE: 0.40908030851119204\n")


    def test_solve_2 (self) :
        r = io.StringIO("10003:\n1515111\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10003:\n2.3812897196261678\nRMSE: 0.4508391999183904\n")


    def test_solve_3 (self) :
        r = io.StringIO("10006:\n1093333\n10007:\n1204847\n")
        w = io.StringIO()
        netflix_solve(r, w)
        self.assertTrue(w.getvalue() == "10006:\n3.712382629107981\n10007:\n2.046037412537484\nRMSE: 0.5258688376647905\n")


    # ----
    # rmse
    # ----

    def test_rmse_1 (self) :
        a=[1,2,3,4,5]
        b=[1,2,3,4,5]
        c= rmse(a,b)
        self.assertTrue(c==0)


    def test_rmse_2 (self) :
        a=[1,2,3,4,5]
        b=[2,3,4,5,6]
        c= rmse(a,b)
        self.assertTrue(c==1)


    def test_rmse_3 (self) :
        a=[10]
        b=[1]
        c= rmse(a,b)
        self.assertTrue(c==9)


# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")