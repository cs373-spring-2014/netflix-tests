#!/usr/bin/env python3

"""
To test the program
% python TestNetflix.py >& TestNetflix.out
% chmod ugo+x TestNetflix.py
% TestNetflix.py < TestNetflix.in >& TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_rmse, netflix_read, netflix_loop, netflix_real, netflix_predict

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # rmse
    # ----

    def test_rmse_1 (self) :
        a = [1, 2, 3, 4, 5]
        b = [1, 2, 3, 4, 5]
        v = netflix_rmse(a, b)
        self.assertAlmostEqual(v, 0.0, places=5, msg=None, delta=None)

    def test_rmse_2 (self) :
        a = [0, 0, 0, 0, 0]
        b = [1, 1, 1, 1, 1]
        v = netflix_rmse(a, b)
        self.assertAlmostEqual(v, 1.0, places=5, msg=None, delta=None)

    def test_rmse_3 (self) :
        a = [1, 2, 3, 4, 5]
        b = [5, 4, 3, 2, 1]
        v = netflix_rmse(a, b)
        self.assertAlmostEqual(v, 2.82842712475, places=5, msg=None, delta=None)

    def test_rmse_4 (self) :
        a = [5.8, 4.7, 1.0, 0.0, 2.5]
        b = [5.0, 1.8, 3.3, 4.1, 1.1]
        v = netflix_rmse(a, b)
        self.assertAlmostEqual(v, 2.57332469774, places=5, msg=None, delta=None)

    def test_rmse_5 (self) :
        a = [5.0, 4.9, 4.8, 5.0, 0.0]
        b = [5.0, 4.8, 4.9, 4.9, 4.7]
        v = netflix_rmse(a, b)
        self.assertAlmostEqual(v, 2.10333069202, places=5, msg=None, delta=None)

    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1573456:\n11\n")
        a = [0, 0]
        b = netflix_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 1573456)
        self.assertTrue(j == 11)

    def test_read_2 (self) :
        r = io.StringIO("130586:\n574571\n57829\n")
        a = [0, 0]
        b = netflix_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 130586)
        self.assertTrue(j == 574571)
        b = netflix_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 130586)
        self.assertTrue(j == 57829)

    def test_read_3 (self) :
        r = io.StringIO("73191:\n")
        a = [0, 0]
        b = netflix_read(r, a)
        i, j = a
        self.assertTrue(b == False)
        self.assertTrue(i == 73191)
        self.assertTrue(j == 0)

    # ----
    # loop
    # ----

    def test_loop_1 (self) :
        r = io.StringIO("7942:\n1329724\n11456\n144865\n1339652\n")
        w = io.StringIO()
        netflix_loop(r, w)
        self.assertTrue(w.getvalue() == "7942:\n4.22\n4.61\n4.09\n3.91\nRMSE: 0.59\n")

    def test_loop_2 (self) :
        r = io.StringIO("755:\n1452925\n1775137\n124574\n2043027\n")
        w = io.StringIO()
        netflix_loop(r, w)
        self.assertTrue(w.getvalue() == "755:\n3.58\n3.82\n2.75\n3.32\nRMSE: 1.31\n")

    def test_loop_3 (self) :
        r = io.StringIO("6856:\n108976\n1749925\n2002102\n")
        w = io.StringIO()
        netflix_loop(r, w)
        self.assertTrue(w.getvalue() == "6856:\n3.33\n3.3\n3.91\nRMSE: 0.43\n")

    # ----
    # real
    # ----

    def test_real_1 (self) :
        a = [9873, 1813660]
        real = []
        netflix_real(a, real)
        self.assertTrue(real[0] == 3)

    def test_real_2 (self) :
        a = [8699, 536334]
        real = []
        netflix_real(a, real)
        self.assertTrue(real[0] == 4)

    def test_real_3 (self) :
        a = [813, 2531311]
        real = []
        netflix_real(a, real)
        self.assertTrue(real[0] == 4)

    # -------
    # predict
    # -------

    def test_predict_1 (self) :
        a = [7465, 388088]
        b = []
        netflix_predict(a, b)
        self.assertAlmostEqual(b[0], 1.5595651546338967, places=5, msg=None, delta=None)

    def test_predict_2 (self) :
        a = [7343, 797384]
        b = []
        netflix_predict(a, b)
        self.assertAlmostEqual(b[0], 3.711139955324439, places=5, msg=None, delta=None)

    def test_predict_2 (self) :
        a = [728, 1450722]
        b = []
        netflix_predict(a, b)
        self.assertAlmostEqual(b[0], 3.578646252747253, places=5, msg=None, delta=None)

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
