#!/usr/bin/env python3

# --------------------------------------------
# University of Texas - Austin
# CS373: Software Engineering
# PROJECT 2: Netflix
#
# Name: Ian A. Hays
# UTEID: IH2974
# CS username: ianhays
# -------------------------------------------- 

"""
To test the program:
    % python TestNetflix.py > TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py > TestNetflix.out
"""
# ------------------------
# Imports
# ------------------------
import io
import unittest

from Netflix import netflix_read, netflix_eval, probe_actual, user_averages, movie_averages
# ------------------------
# TestCollatz
# ------------------------
class TestCollatz (unittest.TestCase) :

  # ------------------------
  # netflix_read
  # ------------------------
  def test_read_1 (self) :
    r = io.StringIO("10:\n10\n")
    t = netflix_read(r)
    self.assertTrue(t[10][0] == 10)

  def test_read_2 (self) :
    r = io.StringIO("200:\n10\n10\n10\n")
    t = netflix_read(r)
    self.assertTrue(t[200][0] == 10)
    self.assertTrue(t[200][1] == 10)
    self.assertTrue(t[200][2] == 10)

  def test_read_3 (self) :
    r = io.StringIO("544:\n18000\n100:\n18000\n")
    t = netflix_read(r)
    self.assertTrue(t[544][0] == 18000)
    self.assertTrue(t[100][0] == 18000)  
    
  def test_read_4 (self) :
    r = io.StringIO("544:\n18000\n18000\n18000\n100:\n")
    t = netflix_read(r)
    self.assertTrue(t[544][0] == 18000)
    self.assertTrue(t[544][1] == 18000)
    self.assertTrue(t[544][2] == 18000)
    
  # ------------------------
  # netflix_eval
  # ------------------------
  def test_eval_1 (self):
    v = netflix_eval(1, 30878)
    self.assertTrue(v == 3.683160312499557)
 
  def test_eval_2 (self):
    v = netflix_eval(17179, 2268146)
    self.assertTrue(v == 2.9356284431965625)
    
  # ------------------------
  # cache validity
  # ------------------------
  def test_cache_1 (self):
    self.assertTrue(user_averages['585189'] == 3.608695652173913)
    
  def test_cache_2 (self):
    self.assertTrue(movie_averages['7964'] == 3.6305970149253732) 
    
  def test_cache_3 (self):
    self.assertTrue(probe_actual['1']['30878'] == 4)
  
  
# ------------------------
# main
# ------------------------
print("TestNetflix.py")
unittest.main()
print("Done.")
