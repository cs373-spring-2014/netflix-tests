#!/usr/bin/env python3

# -------------------------------
# TestNetflix.py
# Comyar Zaheri
# -------------------------------

"""
To test the program:
    % python3 TestNetflix.py >& TestNetflix.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py >& TestNetflix.out
"""

# -------
# imports
# -------

import io
import unittest

from Netflix import *

# -----------
# TestCollatz
# -----------

class TestNetflix (unittest.TestCase) :

    # ----
    # read
    # ----

    # Load files
    Globals.actual_ratings_cache = json.load(open(Globals.actual_ratings_path))
    Globals.avg_user_rating_cache = json.load(open(Globals.avg_user_rating_cache_path))
    Globals.avg_movie_ratings_cache = json.load(open(Globals.avg_movie_ratings_cache_path))

    def test_read_1 (self) :
        r = io.StringIO("2043:\n1417435\n")
        m = netflix_read(r)
        movie, user = list(next(m))
        self.assertTrue(movie == "2043")
        self.assertTrue(user == "1417435")

    def test_read_2 (self) :
        r = io.StringIO("10851:\n1417435\n")
        m = netflix_read(r)
        movie, user = list(next(m))
        self.assertTrue(movie == "10851")
        self.assertTrue(user == "1417435")

    def test_read_3 (self) :
        r = io.StringIO("2043:\n1417435\n2312054\n")
        m = netflix_read(r)
        movie, user = list(next(m))
        self.assertTrue(movie == "2043")
        self.assertTrue(user == "1417435")
        movie, user = list(next(m))
        self.assertTrue(movie == "2043")
        self.assertTrue(user == "2312054")

    # ----
    # predict
    # ----

    def test_predict_1 (self) :
        prediction = netflix_predict("2043", "1417435")

    def test_predict_2 (self) :
        prediction = netflix_predict("2043", "2312054")

    def test_predict_3 (self) :
        prediction = netflix_predict("10851", "1417435")

    # -----
    # run
    # -----

    def test_run_1 (self) :
        r = io.StringIO("2043:\n1417435\n2312054\n")
        w = io.StringIO()
        netflix_run(r, w)

    def test_run_2 (self) :
        r = io.StringIO("2043:\n1417435\n2312054\n")
        w = io.StringIO()
        netflix_run(r, w)

    def test_run_3 (self) :
        r = io.StringIO("2043:\n1417435\n2312054\n")
        w = io.StringIO()
        netflix_run(r, w)

# ----
# main
# ----

print("TestNetflix.py")
unittest.main()
print("Done.")
