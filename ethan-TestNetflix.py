#!/usr/bin/env python3


# -------
# imports
# -------

import io
import unittest

from Netflix import netflix_read, netflix_print, netflix_solve, \
read_votes, generateAnswerMap, rmse_gen, prep_usr_avg, prep_for_method

# -----------
# TestCollatz
# -----------

class TestNetflix (unittest.TestCase) :

  # ----
  # read
  # ----

  def test_read_simple (self) :
    r = io.StringIO("3103:")
    l = netflix_read(r);
    self.assertTrue(l[0] == "3103:") 

  def test_read_last (self) :
    r = io.StringIO("3103:\n1000\n656565\n123123123")
    l = netflix_read(r);
    
    self.assertTrue(l[3] == "123123123")

  def test_read_multiple (self) :
    r = io.StringIO("3103:\n1000\n656565\n123123123\n747474747")
    l = netflix_read(r);
    self.assertTrue(l[1] == "1000")
    self.assertTrue(l[2] == "656565")
    self.assertTrue(l[3] == "123123123")

  def test_print_simple (self) :
    w = io.StringIO()
    p = [1292,1231,1331]
    netflix_print(w, p)
    self.assertTrue(w.getvalue() == "1292\n1231\n1331\n")

  def test_print_long (self) :
    w = io.StringIO()
    p = [1292,1231,1331,443321,432,1,9]
    netflix_print(w, p)
    self.assertTrue(w.getvalue() == "1292\n1231\n1331\n443321\n432\n1\n9\n")
  
  def test_print_empty (self) :
    w = io.StringIO()
    p = []
    netflix_print(w, p)
    self.assertTrue(w.getvalue() == "")  


  def test_generateAnswerMap_simple (self) :
    d = generateAnswerMap()
    self.assertTrue(d['10015:']['588400'] == '2') 
    

  def test_generateAnswerMap_end (self) :
    d = generateAnswerMap()
    self.assertTrue(d['5643:']['997673'] == '1') 
    

  def test_generateAnswerMap_middle (self) :
    d = generateAnswerMap()
    self.assertTrue(d['778:']['2048576'] == '5')    

  def test_rmse_gen_simple (self) :
    v = [1, 2, 3, 4]
    l = [1, 2, 3, 4]
    self.assertTrue(rmse_gen(v,l) == 0.0)

  def test_rmse_gen_one (self) :
    v = [1, 2, 3, 4]
    l = [2, 3, 4, 5]
    self.assertTrue(rmse_gen(v,l) == 1.0)

  def test_rmse_gen_max (self) :
    v = [1, 2, 3, 4]
    l = [4, 3, 2, 1]
    self.assertTrue(rmse_gen(v,l) == 2.23606797749979)

  def test_prep_user_avg_simple (self) :
    d = prep_usr_avg()
    self.assertTrue(d['688684']["1990"] == '4.2')

  def test_prep_user_avg_middle (self) :
    d = prep_usr_avg()
    self.assertTrue(d['585188']["1940"] == '4.0')

  def test_prep_user_avg_end (self) :
    d = prep_usr_avg() 
    self.assertTrue(d['1565358']["1970"] == "4.5")

  def test_prep_for_method_simple(self) :
    d = prep_for_method("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
    self.assertTrue(d['161406'] == "3.3852459016393444")
  
  def test_prep_for_method_middle(self) :
    d = prep_for_method("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
    self.assertTrue(d['700327'] == '3.2513966480446927')
  
  def test_prep_for_method_end(self) :
    d = prep_for_method("/u/thunt/cs373-netflix-tests/ericweb2-custAveragesOneLine.txt")
    self.assertTrue(d['511016'] == "4.410526315789474")



unittest.main()
print("Done.")

#def netflix_run (r, w) :
#def netflix_read(r) :
#def netflix_print(out, predList) :
#def netflix_solve(idList) :
#def prep_for_method(f) :
#def nexflix_predict(userId, movieId, dataUser, dataMovie, countMovie) :
#def generateAnswerMap() :
#def rmse_gen (voteList, predList) :
#def read_votes(data) :
