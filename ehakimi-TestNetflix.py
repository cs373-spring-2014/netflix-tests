#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2014
# Glenn P. Downing
# ---------------------------

"""
To test the program:
    % python TestNetflix.py >& TestNetflix.py.out
    % chmod ugo+x TestNetflix.py
    % TestNetflix.py >& TestNetflix.py.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Netflix import netflix_read, netflix_print, netflix_solve

# -----------
# TestNetflix
# -----------

class TestNetflix (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("1:\n1\n2\n3\n2:\n4\n5\n6\n3:\n7\n8\n9\n10")
        c = []
        b = netflix_read(r, c)
        self.assert_(b    == True)
        self.assert_(c[0] ==  (1,[1,2,3]))
        self.assert_(c[1] ==  (2,[4,5,6]))
        self.assert_(c[2] ==  (3,[7,8,9,10]))
		
    def test_read_2 (self) :
        r = StringIO.StringIO("")
        c = []
        b = netflix_read(r, c)
        self.assert_(b    == False)
        self.assert_(c ==  [])
		
    def test_read_3 (self) :
        r = StringIO.StringIO("1:\n2:\n0\n")
        c = []
        b = netflix_read(r, c)
        self.assert_(b    == True)
        self.assert_(c[0] ==  (1,[]))
        self.assert_(c[1] ==  (2,[0]))

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = StringIO.StringIO("")
        w = StringIO.StringIO()
        netflix_solve(r, w)
        self.assert_(w.getvalue() == "")

# ----
# main
# ----

print "TestNetflix.py"
unittest.main()
print "Done."
