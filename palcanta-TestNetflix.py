#!/usr/bin/env python3

"""TestNetflix.py - Test harness for unit testing Netflix.py"""
__author__ = "Paulo Alcantara"
__eid__ = "pma454"
__UTCSID__ = "palcanta"
__copyright__ = "Copyright 2014 - Paulo Alcantara"
__credits__ = ["Glenn P Downing", "Paulo Alcantara"]
__license__="UTCS School Project - Do Not Distribute"
__version__="0.0.1"
__email__="paulo.alcantara@utexas.edu"
__status__="Development"


import io
import unittest

from Netflix import calc_rmse, netflix_read, netflix_rate, netflix_print, netflix_repl 

"""
Dummy Cache object used to pass in a test cache for testing methods
Initializes datastructures used in real cache but with empty values (for unit testing)
"""
class DummyCache:
    def __init__(self, avg = 0):
        self.answer_cache = {}
        self.user_avg = {}
        self.movie_avg = {}
        self.overall_average = avg

class NetflixTests(unittest.TestCase):
    def rmse_1_helper(self):
        count = 0
        while count < 500:
            yield 1, 5
            count += 1

    def test_rmse_1(self):
        result = calc_rmse(self.rmse_1_helper())
        self.assertEqual(result, 4)

    def rmse_2_helper(self):
        count = 0
        while count < 3:
            yield count, count+1
            count += 1

    def test_rmse_2(self):
        result = calc_rmse(self.rmse_2_helper())
        self.assertEqual(result, 1)

    def rmse_3_helper(self):
        yield 2, 4
        yield 3, 3
        yield 4, 2

    def test_rmse_3(self):
        # RMSE example test used by prof. Downing in lecture
        # Using yield to create a generator to do on-iteration calculations
        # Reduces the space complexity to O(1)
        result = calc_rmse(self.rmse_3_helper()) 
        self.assertEqual(str(result), "1.632993161855452")

    def test_netflix_read(self):
        test_input = "2043:\n1417435\n"
        r = io.StringIO(test_input)
        w = io.StringIO()
        out = netflix_read(r, w)
        movie_id, customer_id = next(out)
        self.assertTrue(movie_id == 2043)
        self.assertTrue(customer_id == 1417435)
        self.assertTrue(w.getvalue() == "2043:\n")

    def test_netflix_read_1(self):
        test_input = "1337:\n1337\n7331"
        r = io.StringIO(test_input)
        w = io.StringIO()
        out = netflix_read(r, w)
        movie_id, customer_id = next(out)
        self.assertTrue(movie_id == 1337)
        self.assertTrue(customer_id == 1337)
        movie_id, customer_id = next(out)
        self.assertTrue(movie_id == 1337)
        self.assertTrue(customer_id == 7331)
        self.assertTrue(w.getvalue() == "1337:\n")

    def test_netflix_read_2(self):
        test_input = "1337:\n1337\n7331\n7331:\n1337"
        r = io.StringIO(test_input)
        w = io.StringIO()
        out = netflix_read(r, w)
        movie_id, customer_id = next(out)
        self.assertTrue(movie_id == 1337)
        self.assertTrue(customer_id == 1337)
        movie_id, customer_id = next(out)
        self.assertTrue(movie_id == 1337)
        self.assertTrue(customer_id == 7331)
        movie_id, customer_id = next(out)
        self.assertTrue(movie_id == 7331)
        self.assertTrue(customer_id == 1337)
        self.assertTrue(w.getvalue() == "1337:\n7331:\n")

    def test_netflix_rate(self):
        a = ([x, x+1] for x in range(0, 1))
        cache = DummyCache(3) # Initialize a cache with the average movie rating of 3
        # This would generate a negative # based on our rating calculation, make sure our program rounds to 1.0
        cache.movie_avg["0"] = 1
        cache.user_avg["1"] = 2
        out = netflix_rate(a, cache)
        movie_id, customer_id, rating = next(out)
        self.assertTrue(movie_id == 0)
        self.assertTrue(customer_id == 1)
        self.assertTrue(rating == 1.0)

    def test_netflix_rate_1(self):
        a = ([x, x+1] for x in range(0, 1))
        cache = DummyCache(3) # Initialize a cache with the average movie rating of 3
        # Movie / Customer not in cache, will assume default rating
        out = netflix_rate(a, cache)
        movie_id, customer_id, rating = next(out)
        self.assertTrue(movie_id == 0)
        self.assertTrue(customer_id == 1)
        self.assertTrue(rating == 3.0)

    def test_netflix_rate_2(self):
        a = ([x, x] for x in range(0, 3))
        cache = DummyCache(3) # Initialize a cache with the average movie rating of 3
        cache.movie_avg["0"] = 1
        cache.movie_avg["1"] = 5
        cache.movie_avg["2"] = 4
        cache.user_avg["0"] = 2
        cache.user_avg["1"] = 5
        cache.user_avg["2"] = 1
        out = netflix_rate(a, cache)
        movie_id, customer_id, rating = next(out)
        self.assertTrue(movie_id == 0)
        self.assertTrue(customer_id == 0)
        self.assertTrue(rating == 1.0)

        movie_id, customer_id, rating = next(out)
        self.assertTrue(movie_id == 1)
        self.assertTrue(customer_id == 1)
        self.assertTrue(rating == 5.0)

        movie_id, customer_id, rating = next(out)
        self.assertTrue(movie_id == 2)
        self.assertTrue(customer_id == 2)
        self.assertTrue(rating == 2.0)

    def test_netflix_print(self):
        a = ([x, x, 1.0] for x in range(0,1))
        w = io.StringIO()
        cache = DummyCache(3) # Initialize a cache with the average movie rating of 3
        cache.answer_cache["0"] = {"0": 3.0}
        out = netflix_print(w, a, cache)
        rating, actual_rating = next(out)
        self.assertTrue(w.getvalue() == "1.0\n")
        self.assertTrue(rating == 1.0)
        self.assertTrue(actual_rating == 3.0)

    def test_netflix_print_1(self):
        a = ([0, x, x + 1.0] for x in range(0,2))
        w = io.StringIO()
        cache = DummyCache(3) # Initialize a cache with the average movie rating of 3
        cache.answer_cache["0"] = {"0": 3.0, "1" : 4.0}
        out = netflix_print(w, a, cache)
        rating, actual_rating = next(out)
        self.assertTrue(w.getvalue() == "1.0\n")
        self.assertTrue(rating == 1)
        self.assertTrue(actual_rating == 3)

        rating, actual_rating = next(out)
        self.assertTrue(w.getvalue() == "1.0\n2.0\n")
        self.assertTrue(rating == 2.0)
        self.assertTrue(actual_rating == 4.0)

    def test_netflix_print_2(self):
        a = ([x, x, x + 1.0] for x in range(0,2))
        w = io.StringIO()
        cache = DummyCache(3) # Initialize a cache with the average movie rating of 3
        cache.answer_cache["0"] = {"0": 3.0}
        cache.answer_cache["1"] = {"1": 2.0}
        out = netflix_print(w, a, cache)
        rating, actual_rating = next(out)
        self.assertTrue(w.getvalue() == "1.0\n")
        self.assertTrue(rating == 1)
        self.assertTrue(actual_rating == 3)

        rating, actual_rating = next(out)
        self.assertTrue(w.getvalue() == "1.0\n2.0\n")
        self.assertTrue(rating == 2.0)
        self.assertTrue(actual_rating == 2.0)

    def test_netflix_repl(self):
        cache = DummyCache(3.0) # Initialize a cache with the average movie rating of 3
        cache.answer_cache["2043"] = {"1417435": 3}
        cache.movie_avg["2043"] = 5
        cache.user_avg["1417435"] = 2
        test_input = "2043:\n1417435\n"
        r = io.StringIO(test_input)
        w = io.StringIO()
        netflix_repl(r, w, cache)
        
        self.assertTrue(w.getvalue() == "2043:\n4.0\nRMSE: 1.0")

    def test_netflix_repl_1(self):
        cache = DummyCache(3.0) # Initialize a cache with the average movie rating of 3
        cache.answer_cache["2043"] = {"1417435": 3, "1337": 4}
        cache.movie_avg["2043"] = 5
        cache.user_avg["1417435"] = 2
        cache.user_avg["1337"] = 3
        test_input = "2043:\n1417435\n1337\n"
        r = io.StringIO(test_input)
        w = io.StringIO()
        netflix_repl(r, w, cache)
        self.assertTrue(w.getvalue() == "2043:\n4.0\n5.0\nRMSE: 1.0")

    def test_netflix_repl_2(self):
        cache = DummyCache(3.0) # Initialize a cache with the average movie rating of 3
        cache.answer_cache["2043"] = {"1417435": 4, "1337": 3}
        cache.answer_cache["1337"] = {"7331": 2}
        cache.movie_avg["2043"] = 3
        cache.movie_avg["1337"] = 3
        cache.user_avg["1417435"] = 2
        cache.user_avg["1337"] = 3
        cache.user_avg["7331"] = 4
        test_input = "2043:\n1417435\n1337\n1337:\n7331\n"
        r = io.StringIO(test_input)
        w = io.StringIO()
        netflix_repl(r, w, cache)

        self.assertTrue(w.getvalue() == "2043:\n2.0\n3.0\n1337:\n4.0\nRMSE: 1.632993161855452")
# ----
# main
# ----

print ("TestNetflix.py")
unittest.main()
print ("Done.")
